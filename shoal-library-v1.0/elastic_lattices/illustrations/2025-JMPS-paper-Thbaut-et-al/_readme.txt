This folder contains the lattice examples discussed in the paper

"Fixing non-positive energies in higher-order homogenization"
By M. Thbaut, B. Audoly and C. Lestringant
Submitted, 2024

In this paper, we build positive higher-order energies in the bulk, by (i) refraining from integrating the energy by parts and (ii) using a Cholesky procedure to preserve the positivity of the energy. This new approach is reflected in the update (version 1.1) to the library.