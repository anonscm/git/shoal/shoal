<TeXmacs|2.1.1>

<style|<tuple|tmarticle|preview-ref|comment|cite-author-year|schola-font>>

<\body>
  <\hide-preamble>
    <assign|math-bf|<macro|body|<with|color|red|<frame|<arg|body>>>>>

    <assign|structure-coef|<macro|alpha|up|down|\<b-e\><rsup|<arg|up>><rsub|<arg|down>><around*|(|<arg|alpha>|)>>>

    <assign|t:|<syntax|:|\<cdot\>>>

    <assign|t.:|<syntax|<shift|\<therefore\>||.06em>|\<cdot\>>>

    <assign|t::|<syntax|:<around*|\<nobracket\>||\<nobracket\>>:|\<cdot\>>>

    <assign|t.::|<syntax|\<cdot\><around*|\<nobracket\>||\<nobracket\>>\<vdots\><around*|\<nobracket\>||\<nobracket\>>\<cdot\>|\<cdot\>>>

    <assign|t:::|<syntax|:<around*|\<nobracket\>||\<nobracket\>>:<around*|\<nobracket\>||\<nobracket\>>:|\<cdot\>>>

    <assign|Nb|N<rsub|<text|b>>>

    <assign|stp|<macro|x|<below|<math-up|stp>|<arg|x>>>>

    <assign|at-element-center|<macro|f|a|<arg|f><rsub|<arg|a>><rsup|<text|c>>>>

    <assign|citep|<xmacro|keys|(<cite-arg|<arg|keys|0>><map-args|cite-arg-extra|concat|keys|1>)>>

    <assign|citet|<xmacro|keys|<cite-arg|<arg|keys|0>><map-args|cite-arg-extra|concat|keys|1>>>
  </hide-preamble>

  <doc-data|<doc-title|Lattice homogenization in
  <verbatim|shoal>:<next-line>implementation
  notes>|<doc-author|<author-data|<author-name|B.
  Audoly>>>|<doc-date|December 6, 2023>>

  <section|Flexible tree structures>

  <subsection|Motivation>

  Data is stored in a flexible way in the vectors <math|\<b-l\>> (macroscopic
  degrees of freedom), <math|\<b-y\>> (microscopic degrees of freedom),
  <math|\<b-E\>> (microscopic strain) and the rows of the constraint matrix
  <math|\<b-cal-Q\>>. The data they contain is set by the options passed to
  the homogenization library.

  This flexibility serves to accomodate the following situations:

  <\itemize>
    <item>in the presence of pre-strain or pre-stress, one of the entries in
    <math|\<b-l\><around*|(|\<b-X\>|)>> and one of the entries in
    <math|\<b-E\>> represents a constant field with value <math|1>, allowing
    us to write the elastic energy as a quadratic form in <math|\<b-E\>>;

    <item>when inextensible beams are used, the inextensibility constraint is
    enforced by dedicated rows of the constraint matrix <math|\<b-cal-Q\>>;

    <item>with rank-deficient problems, special degrees of freedom
    <math|><math|<wide|l|\<check\>><rsub|i>> are included in
    <strong|<math|l>> to parameterize the amplitude of the null vectors (see
    Appendix<nbsp>E of the Journal of Theoretical, Computational and Applied
    Mechanics paper);

    <item>in future work, additional enrichment variables will be
    incorporated into <math|\<b-l\>> and coupled to the microscopic degrees
    of freedom through a dedicated constraint.
  </itemize>

  The data storage in these structures is described managed by the symbolic
  tree structures. These trees are printed out when the library is run,
  providing a guide on how the homogenized tensors must be interpreted. Each
  leaf in the trees maps out to slots in the corresponding vectors.

  <subsection|<math|\<b-l\>>-vector (macroscopic degrees of freedom)>

  <\equation*>
    <tree|\<b-l\>|<tree|<around*|(|<text|opt>|)><text|rgdMotion>|\<b-u\>|\<b-gamma\>>|<tree|<wide|\<b-varepsilon\>|\<check\>>|\<varepsilon\><rsub|x\<nocomma\>x>|\<varepsilon\><rsub|y\<nocomma\>y>|<sqrt|2>*\<varepsilon\><rsub|x\<nocomma\>y>>|<around*|(|<text|opt>|)>1|<tree|<text|(opt)
    rankDeficiency>|<wide|l|\<check\>><rsub|1>|\<ldots\>>|<tree|<around*|(|<text|opt>|)>
    enrichVar |\<ldots\>>>
  </equation*>

  The tree structure for <math|\<b-l\><around*|(|\<b-X\>|)>> is made up of

  <\itemize>
    <item>an optional rgdMotion branch containing positional degrees of
    freedom <math|\<b-u\>> and rotational degrees of freedom
    <math|\<b-gamma\>> (macroscopic rigid-body motion); these fields are
    optional if all the elements are invariant by rigid-body motions (a case
    where the energy does not depend on <math|\<b-u\>> nor on
    <math|\<b-gamma\>>);

    <\itemize>
      <item>this term might also be useful to handle linear elasticity
      problem obtained by linearizing about a pre-stressed configurations
      (i.e., <em|finite> pre-stress, the paper being limited to infinitesimal
      pre-stress): in this case, the energy contribution
      <math|\<b-N\><rsub|0><t:><around*|(|\<nabla\>\<b-xi\>\<otimes\>\<nabla\>\<b-xi\>|)>>
      depends on <math|\<nabla\>\<b-xi\>=<around*|(|\<nabla\>\<b-xi\>|)><rsup|<text|s>>+<around*|(|\<nabla\>\<b-xi\>|)><rsup|<text|a>>>
      which can be expressed in terms of not only <math|\<b-varepsilon\>>
      (symmetric part <math|<around*|(|\<nabla\>\<b-xi\>|)><rsup|<text|s>>>)
      but also <math|\<b-gamma\>> (antisymmetric part
      <math|<around*|(|\<nabla\>\<b-xi\>|)><rsup|<text|a>>>;
    </itemize>

    <item>the macroscopic strain vector <math|<wide|\<b-varepsilon\>|\<check\>>>
    in Mandel's notation (components in the tree above correspond to
    <math|d=2>);

    <item>an optional constant field equal to <math|1> that is useful, e.g.,
    to account for (infinitesimal) pre-stress

    <item>an optional <em|rank-deficiency> branch;

    <item>an optional branch listing enrichment variables (will be
    demonstrated in future work).
  </itemize>

  <subsection|<math|\<b-y\>>-tree (microscopic degrees of freedom)>

  <\equation*>
    <tree|\<b-y\>|<tree|b=1|<tree|<text|transl.>|\<xi\><rsub|x>|\<xi\><rsub|y>|\<xi\><rsub|z>>|<tree|<text|rot.>|\<theta\><rsub|x>|\<ldots\>>>|\<ldots\>|<tree|b=n<rsub|<text|b>>|<tree|<text|transl.>|\<xi\><rsub|x>|\<xi\><rsub|y>|\<xi\><rsub|z>>>>
  </equation*>

  The tree structure for <math|\<b-y\><around*|(|\<b-X\>|)>> is made up of

  <\itemize>
    <item>a branch for each Bravais sub-lattice containing:

    <\itemize>
      <item>a microscopic displacement <math|\<b-xi\>> (shown for <math|d=3>)
      in the above tree

      <item>an optional microscopic rotation <math|\<b-theta\>> (needed if
      there are adajcent beams with <math|E*I\<neq\>0>);
    </itemize>
  </itemize>

  <\equation*>
    <tree|\<b-E\>|<tree|<text|elmStr>|<tree|<text|elmt
    #1>|\<varepsilon\>|\<kappa\>|\<tau\>>|\<ldots\>|<tree|<text|elmt
    #<math|n<rsub|\<varphi\>>>>|\<varepsilon\>>>|<tree|\<b-xi\><text|-avg.>|<around*|\<langle\>|\<xi\><rsub|1>|\<rangle\>>|\<ldots\>|<around*|\<langle\>|\<xi\><rsub|d>|\<rangle\>>>|<text|(opt)
    >1|<tree|<text|(opt)> enrichVar |\<ldots\>>>
  </equation*>

  \;

  <subsection|<math|\<b-E\>>-tree (microscopic strain)>

  The tree structure for <math|\<b-E\><around*|(|\<b-X\>|)>> is made up of:

  <\itemize>
    <item>an <em|element-strain> branch made up of:

    <\itemize>
      <item>a branch for each element family, containing

      <\itemize>
        <item>the strain relevant to the family type (extensional strain
        <math|\<varepsilon\>>, curvature strain <math|\<kappa\>> and twisting
        strain <math|\<tau\>> for a 2D beam, etc.);
      </itemize>
    </itemize>

    <item>a branch pointing to the Cartesian components
    <math|<around*|\<langle\>|\<xi\><rsub|i>|\<rangle\>>> of the average of
    the microscopic displacement over the various Bravais sub-lattices;

    <item>an optional entry pointing to the (optional) constant field in
    <math|\<b-l\>> with value 1;

    <item>an optional branch for enrichment variables, comprising entries
    pointing to the residual of the equation coupling the enrichment
    variables to the microscopic/macroscopic variables.
  </itemize>

  The <math|\<b-E\><rsub|l>>, <math|\<b-E\><rsub|l><rprime|'>>,
  <math|\<b-E\><rsub|l><rprime|''>>, <math|\<b-E\><rsub|y>>,
  etc.<nbsp>tensors are assembled by visiting this tree.

  <\equation*>
    <tree|\<b-cal-Q\>|<text|<math|<tree|\<b-xi\><text|-avg>|<around*|\<langle\>|\<xi\><rsub|x>|\<rangle\>>|<around*|\<langle\>|\<xi\><rsub|y>|\<rangle\>>|\<ldots\>>>>|<tree|<text|(opt)
    rigidity>|<text|inext. bar #1>|\<ldots\>>|<tree|<text|(opt)
    enrichCstr>|\<ldots\>>>
  </equation*>

  \;

  Finally, the tree structure representing the rows of <math|\<b-cal-Q\>> is
  made up of:

  <\itemize>
    <item>a branch for the constraints on the average microscopic
    displacement <math|<around*|\<langle\>|\<b-xi\><rsub|b>|\<rangle\>><rsub|b>=\<b-0\>>;

    <item>a branch handling any applicable rigidity constraint (such as
    inextensible beams);

    <item>an optional branch handling inextensibility constraints.
  </itemize>

  <subsection|<math|\<b-cal-Q\>>-rows (constraints)>

  The entries in the <math|\<b-cal-Q\>>-tree simply point to the relevant
  entries in <math|\<b-E\>>.
</body>

<\initial>
  <\collection>
    <associate|font-base-size|8>
    <associate|info-flag|detailed>
    <associate|math-font|math-schola>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-3|<tuple|1.2|?>>
    <associate|auto-4|<tuple|1.3|?>>
    <associate|auto-5|<tuple|1.4|?>>
    <associate|auto-6|<tuple|1.5|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|1.<space|2spc>Flexible
      tree structures> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>