'shoal' library

VERSION
1.1

DESCRIPTION
This library implements a generic, higher-order homogenization method for linear, discrete elastic structures. The name 'shoal' stands for 'Second-order HOmogenization Automated in a Library'. 

The underlying mathematical method is described in the following preprint, available on the Hal platform (https://hal.science/hal-04112136): Basile Audoly and Claire Lestringant. An energy approach to asymptotic, higher-order, linear homogenization. 2023. An up-to-date description of the mathematical method can be found in discrete_engine/documentation/*

The library is written in the symbolic calculation language Wolfram Mathematica, and has been developed using Mathematica version 13

AUTHOR
Basile Audoly basile.audoly(special.at]cnrs.fr

LICENCE
CeCILL v2.1
  (French Free Software license, compatible with the GNU GPL)
  (see http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)

HISTORY
2023.05.30 first public release (version 1.0)
2024.07.08 do not integrate by parts the l'' term by default (version 1.1)

ACKNOWLEDGEMENTS
This library has been implemented based on a method designed jointly with Claire Lestringant (Institut d'Alembert, Sorbonne University)

MANIFEST
discrete_engine/*     -> abstract homogenization engine (JTCAM 2023 paper by Audoly, Lestringant)
elastic_lattices/*    -> application to elastic lattices (JMPS 2024 paper by Ye, Audoly, Lestringant)
