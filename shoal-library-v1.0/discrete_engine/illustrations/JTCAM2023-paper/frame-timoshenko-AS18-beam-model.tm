<TeXmacs|2.1.1>

<style|<tuple|tmarticle|schola-font>>

<\body>
  <doc-data|<doc-title|Analysis of the beam model of Abdoul-Anziz and
  Seppecher>>

  Basile Audoly, August 2023

  \;

  In the \<#2018\>square periodic beam\<#2019\> example, the links have
  initial length 1, and are subsequently scaled to length
  <math|\<varepsilon\>>.

  Eq.<nbsp>[6] defines stretching strain <math|\<rho\>=<frac|\<Delta\>\<b-xi\>|\<varepsilon\>>\<cdot\>\<b-tau\>>
  where <math|\<b-tau\>> is the unit tangent, which is consistent with our
  strain <math|\<varepsilon\>\<equiv\>\<rho\>>.

  In<nbsp>[7], strain energy is defined as
  <math|E<rsup|\<varepsilon\>>=<big|sum><rsub|<text|el.<nbsp>families>><frac|a|2>*\<rho\><rsup|2>>.
  They use <math|a=1>, so they effectively set the coefficient of
  <math|\<varepsilon\><rsub|\<varphi\>><rsup|2>/2> in the energy
  <math|w<rsub|\<varphi\>>> as <math|<around*|(|E*A*a|)><rsub|<text|ours>>\<equiv\>1>.

  In [7'], <math|\<alpha\>=\<b-tau\>\<times\><frac|\<Delta\>\<b-xi\>|l<rsub|0>>>
  becomes (in 2D) <math|\<b-alpha\>=\<b-e\><rsub|3>*<around*|(|<frac|\<b-n\>\<cdot\>\<Delta\>\<b-xi\>|l>|)>>.
  Our own strain measures correspond to

  <\equation*>
    <tabular|<tformat|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<table|<row|<cell|\<kappa\>>|<cell|=>|<cell|\<theta\><rsub|3\<nocomma\>+>-\<theta\><rsub|3\<nocomma\>->>|<cell|=>|<cell|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|+>-<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|->>>|<row|<cell|\<tau\>>|<cell|=>|<cell|<frac|\<theta\><rsub|3\<nocomma\>->+\<theta\><rsub|3\<nocomma\>+>|2>-\<alpha\><rsub|3>>|<cell|=>|<cell|<frac|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|->+<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|->|2>.>>>>>
  </equation*>

  \;

  In<nbsp>[8], bending energy is written as

  <\equation*>
    F<rsup|\<varepsilon\>>=<frac|1|2>*<big|sum><rsub|<text|el.<nbsp>families>><matrix|<tformat|<table|<row|<cell|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|->>>|<row|<cell|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|+>>>>>>\<cdot\><matrix|<tformat|<table|<row|<cell|b>|<cell|c>>|<row|<cell|c<rsup|T>>|<cell|d>>>>>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|->>>|<row|<cell|<around*|(|\<theta\><rsub|3>-\<alpha\><rsub|3>|)><rsub|+>>>>>>.
  </equation*>

  In [example 1], the <math|\<b-tau\>\<otimes\>\<b-tau\>> term captures
  twisting modulus and is ignored in 2D, so we have

  <\equation*>
    b=d=a*f,<separating-space|2em>c=<frac|a|2>*f.
  </equation*>

  This yields

  <\equation*>
    <tabular|<tformat|<table|<row|<cell|F<rsup|\<varepsilon\>>>|<cell|=>|<cell|<frac|a*f|2>*<big|sum><rsub|<text|ef>><matrix|<tformat|<table|<row|<cell|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->>>|<row|<cell|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>>>>>>\<cdot\><matrix|<tformat|<table|<row|<cell|1>|<cell|1/2>>|<row|<cell|1/2>|<cell|1>>>>>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->>>|<row|<cell|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>>>>>>>>|<row|<cell|>|<cell|=>|<cell|<frac|a*f|2>*<big|sum><rsub|<text|ef>><around*|(|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|-><rsup|2>+<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+><rsup|2>+<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->*<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|a*f|2>*<big|sum><rsub|<text|ef>><around*|(|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|-><rsup|2>+<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+><rsup|2>+<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->*<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|a*f|2>*<big|sum><rsub|<text|ef>><around*|(|3*<around*|(|<frac|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->+<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>|2>|)><rsup|2>+<frac|1|4>*<around*|(|<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|+>-*<around*|(|\<theta\>-\<alpha\><rsub|3>|)><rsub|->|)><rsup|2>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|a*f|2>*<big|sum><rsub|<text|ef>><around*|(|<frac|1|4>*\<kappa\><rsup|2>+3*\<tau\><rsup|2>|)>>>>>>
  </equation*>

  \;

  Since they use <math|a=1> and <math|f=1> in their examples:

  <\itemize>
    <item>their shearing-to-bending-modulus ratio
    <math|<frac|3|1/4>=<frac|\<mathd\><rsup|2>F<rsup|\<varepsilon\>>/\<mathd\>\<kappa\><rsup|2>|\<mathd\><rsup|2>E<rsup|\<varepsilon\>>/\<mathd\>\<tau\><rsup|2>>=12>
    is correct, and consistent with ours;

    <item>they effectively set the coefficient of
    <math|\<kappa\><rsub|\<varphi\>><rsup|2>/2> in the energy
    <math|w<rsub|\<varphi\>>> as <math|<around*|(|<frac|E*I|a>|)><rsub|<text|ours>>\<equiv\><frac|1|4>>.
  </itemize>

  \;
</body>

<\initial>
  <\collection>
    <associate|font-base-size|8>
    <associate|math-font|math-schola>
  </collection>
</initial>