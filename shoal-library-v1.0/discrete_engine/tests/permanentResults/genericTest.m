<|"microscopicDoF" -> <|"Y0" -> SparseArray[Automatic, {1, 1}, 0, 
     {1, {{0, 1}, {{1}}}, {-(Subscript[el0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]])}}], "Y0p" -> SparseArray[Automatic, {1, 1, 2}, 0, 
     {1, {{0, 2}, {{1, 1}, {1, 2}}}, 
      {(-(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2, 
       (-(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2}}], 
   "Y1" -> SparseArray[Automatic, {1, 1, 2, 2}, 0, 
     {1, {{0, 4}, {{1, 1, 1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}}}, 
      {(Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3}}]|>, 
 "microscopicStrain" -> <|"F0" -> SparseArray[Automatic, {3, 1}, 0, 
     {1, {{0, 0, 1, 2}, {{1}, {1}}}, 
      {Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]}}], 
   "F0p" -> SparseArray[Automatic, {3, 1, 2}, 0, 
     {1, {{0, 0, 2, 4}, {{1, 1}, {1, 2}, {1, 1}, {1, 2}}}, 
      {Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
        (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
        (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
        (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
        (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
        (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]}}], 
   "F0s" -> SparseArray[Automatic, {3, 1, 2, 2}, 0, 
     {1, {{0, 4, 8, 12}, {{1, 1, 1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}, {1, 1, 
       1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}, {1, 1, 1}, {1, 1, 2}, {1, 2, 1}, 
       {1, 2, 2}}}, {(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2)/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2)/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
        Subscript[el2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
        Subscript[el2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]], 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
        Subscript[el2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
        Subscript[el2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
        Subscript[el2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
        (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]}}], 
   "F1" -> SparseArray[Automatic, {3, 1, 2, 2}, 0, 
     {1, {{0, 0, 4, 8}, {{1, 1, 1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}, {1, 1, 
       1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}}}, 
      {((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3}}], 
   "F1p" -> SparseArray[Automatic, {3, 1, 2, 2, 2}, 0, 
     {1, {{0, 8, 16, 24}, {{1, 1, 1, 1}, {1, 1, 1, 2}, {1, 2, 1, 1}, {1, 2, 
       1, 2}, {1, 1, 2, 1}, {1, 1, 2, 2}, {1, 2, 2, 1}, {1, 2, 2, 2}, {1, 1, 
       1, 1}, {1, 1, 1, 2}, {1, 2, 1, 1}, {1, 2, 1, 2}, {1, 1, 2, 1}, {1, 1, 
       2, 2}, {1, 2, 2, 1}, {1, 2, 2, 2}, {1, 1, 1, 1}, {1, 1, 1, 2}, {1, 2, 
       1, 1}, {1, 2, 1, 2}, {1, 1, 2, 1}, {1, 1, 2, 2}, {1, 2, 2, 1}, {1, 2, 
       2, 2}}}, {(-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
            Subscript[m, 2]]^2*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
            Subscript[m, 2]]^2*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el1, 1, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el1, 1, 1, 2]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 2, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el1, 1, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el1, 1, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el1, 1, 1, 2]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 2, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el1, 1, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el1, 1, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el1, 1, 1, 2]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][Subscript[
                ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 2, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, (-3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el1, 1, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3, 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el1, 1, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       -((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el1, 1, 1, 2]][Subscript[m, 1], 
              Subscript[m, 2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][Subscript[
                ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]))/
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 2, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^3}}], "F2" -> SparseArray[Automatic, {3, 1, 2, 2, 
     2, 2}, 0, {1, {{0, 16, 32, 48}, {{1, 1, 1, 1, 1}, {1, 1, 1, 1, 2}, {1, 
       1, 2, 1, 1}, {1, 1, 2, 1, 2}, {1, 1, 1, 2, 1}, {1, 1, 1, 2, 2}, {1, 1, 
       2, 2, 1}, {1, 1, 2, 2, 2}, {1, 2, 1, 1, 1}, {1, 2, 1, 1, 2}, {1, 2, 2, 
       1, 1}, {1, 2, 2, 1, 2}, {1, 2, 1, 2, 1}, {1, 2, 1, 2, 2}, {1, 2, 2, 2, 
       1}, {1, 2, 2, 2, 2}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 2}, {1, 1, 2, 1, 
       1}, {1, 1, 2, 1, 2}, {1, 1, 1, 2, 1}, {1, 1, 1, 2, 2}, {1, 1, 2, 2, 
       1}, {1, 1, 2, 2, 2}, {1, 2, 1, 1, 1}, {1, 2, 1, 1, 2}, {1, 2, 2, 1, 
       1}, {1, 2, 2, 1, 2}, {1, 2, 1, 2, 1}, {1, 2, 1, 2, 2}, {1, 2, 2, 2, 
       1}, {1, 2, 2, 2, 2}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 2}, {1, 1, 2, 1, 
       1}, {1, 1, 2, 1, 2}, {1, 1, 1, 2, 1}, {1, 1, 1, 2, 2}, {1, 1, 2, 2, 
       1}, {1, 1, 2, 2, 2}, {1, 2, 1, 1, 1}, {1, 2, 1, 1, 2}, {1, 2, 2, 1, 
       1}, {1, 2, 2, 1, 2}, {1, 2, 1, 2, 1}, {1, 2, 1, 2, 2}, {1, 2, 2, 2, 
       1}, {1, 2, 2, 2, 2}}}, 
      {(3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]^2 + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]^2 + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (3*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 3*Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 3*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 3*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (3*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 3*Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
              Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]^2 + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*(3*Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]]^2 + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (6*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
            Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (6*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
            Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
           Subscript[m, 1], Subscript[m, 2]] - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
           2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), (3*Subscript[el0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]])*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]^2 - 
         2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
          Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
           Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*
            (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]])))/(2*Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4), 
       -((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
              Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                 Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                 Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*(
                3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                  Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                   Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                   2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]^4)}}], "F2tilde" -> SparseArray[Automatic, {3, 1, 
     2, 2, 2}, 0, {1, {{0, 8, 16, 24}, {{1, 1, 1, 1}, {1, 1, 1, 2}, {1, 1, 2, 
       1}, {1, 1, 2, 2}, {1, 2, 1, 1}, {1, 2, 1, 2}, {1, 2, 2, 1}, {1, 2, 2, 
       2}, {1, 1, 1, 1}, {1, 1, 1, 2}, {1, 1, 2, 1}, {1, 1, 2, 2}, {1, 2, 1, 
       1}, {1, 2, 1, 2}, {1, 2, 2, 1}, {1, 2, 2, 2}, {1, 1, 1, 1}, {1, 1, 1, 
       2}, {1, 1, 2, 1}, {1, 1, 2, 2}, {1, 2, 1, 1}, {1, 2, 1, 2}, {1, 2, 2, 
       1}, {1, 2, 2, 2}}}, 
      {((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2 - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2 - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3, 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3), 
       ((Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3}}]|>, 
 "LagrangeMultipliers" -> <|"G0" -> SparseArray[Automatic, {1, 1}, 0, 
     {1, {{0, 1}, {{1}}}, 
      {(-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
         Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
           2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2}}], 
   "G0p" -> SparseArray[Automatic, {1, 1, 2}, 0, 
     {1, {{0, 2}, {{1, 1}, {1, 2}}}, 
      {(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2, 
       (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
          Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
          (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2}}], 
   "G1" -> SparseArray[Automatic, {1, 1, 2, 2}, 0, 
     {1, {{0, 4}, {{1, 1, 1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}}}, 
      {(-(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[k, 2, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[k, 3, 3][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*(Subscript[k, 2, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 3, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 
                       1]][Subscript[m, 1], Subscript[m, 2]]))))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 3, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[k, 2, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[k, 2, 3]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 3, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 2, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey1, 3, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[k, 1, 2][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[k, 2, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 2]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[k, 2, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[k, 3, 3][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*(Subscript[k, 2, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 3, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                  (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 
                       2]][Subscript[m, 1], Subscript[m, 2]]))))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 3, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[el0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[k, 2, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[k, 2, 3]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 3, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey1, 2, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey1, 3, 1, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 2, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
              Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[k, 1, 2][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[k, 2, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 2]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 2]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[ey1, 2, 1, 2]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 3, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[k, 2, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[k, 3, 3][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*(Subscript[k, 2, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 3, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 
                       1]][Subscript[m, 1], Subscript[m, 2]]))))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 3, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[k, 2, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[k, 2, 3]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 3, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey1, 2, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey1, 3, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[k, 1, 2][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[k, 2, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 2]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]])*
           Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]]) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[k, 2, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[k, 3, 3][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*(Subscript[k, 2, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 3, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                  (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 
                       2]][Subscript[m, 1], Subscript[m, 2]]))))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
          (Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[k, 3, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[el0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[k, 2, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[k, 2, 3]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 3, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey1, 2, 1, 2]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey1, 3, 1, 2]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 2]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 1, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 2][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 2, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 2, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 2, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 3, 1]][
             Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[k, 2, 3][
             Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
             Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
              Subscript[ey0, 3, 1]][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 3, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[k, 1, 2][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[k, 2, 2]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 2]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 2]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[k, 1, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 3, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[k, 2, 3]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 2, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[k, 3, 3]][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[k, 2, 3][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 2, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 2, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey1, 2, 1, 2]][Subscript[m, 
                  1], Subscript[m, 2]]) + Subscript[el0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 3, 1, 2]][
               Subscript[m, 1], Subscript[m, 2]])))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4}}]|>, 
 "elasticityLeadingOrder" -> 
  <|"K0" -> SparseArray[Automatic, {1, 1}, 0, {1, {{0, 1}, {{1}}}, 
      {(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
        (Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])}}], 
   "K1" -> SparseArray[Automatic, {1, 1, 2, 2}, 0, 
     {1, {{0, 4}, {{1, 1, 1}, {1, 2, 1}, {1, 1, 2}, {1, 2, 2}}}, 
      {(-2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                   2]])) + Subscript[ey0, 3, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*(Subscript[k, 2, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]))))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]] - Subscript[el0, 1, 1][
            Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
             Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                   2]])) + Subscript[ey0, 3, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*(Subscript[k, 2, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]))))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]] - Subscript[el0, 1, 1][
            Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
             Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 
                   2]])) + Subscript[ey0, 3, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*(Subscript[k, 2, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]]))))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]] - Subscript[el0, 1, 1][
            Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
             Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4, 
       (-2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (-(Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
          Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             (2*Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*(
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 
                   2]])) + Subscript[ey0, 3, 1][Subscript[m, 1], 
              Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]*(Subscript[k, 2, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]] + Subscript[k, 3, 3][Subscript[m, 1], 
                  Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]]))))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]] - Subscript[el0, 1, 1][
            Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
             Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4}}], 
   "K2" -> SparseArray[Automatic, {1, 1, 2, 2, 2, 2}, 0, 
     {1, {{0, 16}, {{1, 1, 1, 1, 1}, {1, 1, 1, 1, 2}, {1, 1, 1, 2, 1}, {1, 1, 
       1, 2, 2}, {1, 2, 1, 1, 1}, {1, 2, 1, 1, 2}, {1, 2, 1, 2, 1}, {1, 2, 1, 
       2, 2}, {1, 1, 2, 1, 1}, {1, 1, 2, 1, 2}, {1, 1, 2, 2, 1}, {1, 1, 2, 2, 
       2}, {1, 2, 2, 1, 1}, {1, 2, 2, 1, 2}, {1, 2, 2, 2, 1}, {1, 2, 2, 2, 
       2}}}, {((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 - 
         2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]]^2 + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(3*Derivative[1, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) - (Subscript[el0, 2, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) + Subscript[el0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) - Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]))*(6*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]^2 - 
             2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
              Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + 2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[
                      m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + 2*Subscript[ey2, 2, 
                  1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + 2*Subscript[ey2, 3, 
                  1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) - (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + 3*Derivative[0, 1][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) - (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 2][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  (3*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]] + 3*Derivative[0, 1][
                      Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 
                      1], Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 
                       1]][Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 - 
         2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]]^2 + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) - (Subscript[el0, 2, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) + Subscript[el0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) - Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]))*(6*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]^2 - 
             2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
              Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + 2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[
                      m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) - (Subscript[el0, 2, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) + Subscript[el0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) - Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]))*(6*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]^2 - 
             2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
              Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*(Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 
                     1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + 2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[
                      m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 - 
         2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                (2*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[2, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]]^2 + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(3*Derivative[1, 0][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[2, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[2, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[2, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[1, 0][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[2, 0][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 2, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 3, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) - (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 2][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  (3*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]] + 3*Derivative[0, 1][
                      Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 
                      1], Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 
                       1]][Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + 2*Subscript[ey2, 2, 
                  1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (4*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + 2*Subscript[ey2, 3, 
                  1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (4*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 
                    1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]))) - (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[
                m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] - 2*Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 1, 
                2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                  1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + 3*Derivative[0, 1][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (4*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]))) + Subscript[ey0, 1, 1][Subscript[m, 1], 
           Subscript[m, 2]]*(Subscript[el0, 2, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (6*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]] - 
           2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
            Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[ey1, 1, 1, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) + 
             2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Derivative[0, 1][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (4*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]] + Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]])) + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 3*
                Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + 2*Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6, 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 2, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - 2*Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + 2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])) - Subscript[ey0, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*(4*Subscript[ey2, 3, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 2]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                (3*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) - (Subscript[el0, 2, 1][Subscript[m, 
                1], Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) + Subscript[el0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]) - Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Subscript[k, 3, 3][Subscript[m, 1], 
                 Subscript[m, 2]]))*(6*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                Subscript[m, 1], Subscript[m, 2]]^2 - 
             2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3*
              Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*(Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 
                     1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + 2*Subscript[ey2, 1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]*(4*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]) + Subscript[ey1, 1, 1, 1][Subscript[m, 
                  1], Subscript[m, 2]]*(Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + 2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                      2]] + Subscript[el0, 1, 1][Subscript[m, 1], Subscript[
                      m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
              Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
              Subscript[m, 1], Subscript[m, 2]])^2 - 
         2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^3*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey0, 1, 1][
                Subscript[m, 1], Subscript[m, 2]]^2*(Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*(Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]) + 
               Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
                (2*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 2][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 
                    2]])) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[
                m, 2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1], Subscript[m, 2]]^2 + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(3*Derivative[0, 1][Subscript[el0, 1, 
                     1]][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 2][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]]))) + Subscript[ey0, 1, 1][
             Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 + Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*Derivative[0, 2][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] - Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Derivative[0, 1][
                    Subscript[el0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]])) + 
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              (-3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                  Subscript[m, 2]]^2 - Subscript[ey0, 1, 1][Subscript[m, 1], 
                  Subscript[m, 2]]^2*(Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                    2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 2][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]]) + Subscript[ey0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][Subscript[m, 
                    1], Subscript[m, 2]]*(3*Derivative[0, 1][Subscript[el0, 
                       1, 1]][Subscript[m, 1], Subscript[m, 2]]*
                    Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                     Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                     Subscript[m, 2]]*Derivative[0, 2][Subscript[ey0, 1, 1]][
                     Subscript[m, 1], Subscript[m, 2]]))))))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^6}}], 
   "K2tilde" -> SparseArray[Automatic, {1, 1, 2, 2, 2}, 0, 
     {1, {{0, 8}, {{1, 1, 1, 1}, {1, 1, 1, 2}, {1, 2, 1, 1}, {1, 2, 1, 2}, 
       {1, 1, 2, 1}, {1, 1, 2, 2}, {1, 2, 2, 1}, {1, 2, 2, 2}}}, 
      {(2*((-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       ((2*(-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       (2*((-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       ((2*(-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       ((2*(-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       (2*((-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2 - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       ((2*(-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5, 
       (2*((-(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]])) - 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2 - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
           (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]]))*
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
           Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
            Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
            Subscript[m, 1], Subscript[m, 2]]))/
        Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5}}]|>, 
 "elasticityCoupling" -> <|"A0" -> SparseArray[Automatic, {1, 1, 2}, 0, 
     {1, {{0, 2}, {{1, 1}, {1, 2}}}, 
      {-(((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]))/
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       -(((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
          Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^3) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         (Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])}}], 
   "A1" -> SparseArray[Automatic, {1, 1, 2, 2, 2}, 0, 
     {1, {{0, 8}, {{1, 1, 1, 1}, {1, 1, 1, 2}, {1, 1, 2, 1}, {1, 1, 2, 2}, 
       {1, 2, 1, 1}, {1, 2, 1, 2}, {1, 2, 2, 1}, {1, 2, 2, 2}}}, 
      {((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 1][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 1, 
                  1][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 1]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(2*Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1], 
                   Subscript[m, 2]])))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[1, 0][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]])))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 2, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[1, 0][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[1, 0][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[1, 0][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[1, 0][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 2, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[1, 0][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[1, 0][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) - 
         2*(Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]*Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Derivative[0, 1][Subscript[ey1, 1, 1, 2]][
                 Subscript[m, 1], Subscript[m, 2]])) - 
           (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
             Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 2*
                Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
                Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
            (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 1, 2][Subscript[m, 
                  1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                 Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 
                  2]]) - Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 
                2]]*(2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*(2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
                   Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
                   Subscript[m, 1], Subscript[m, 2]] + Subscript[el1, 1, 1, 
                    2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                  Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                   Subscript[m, 2]])))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])*
          (Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])))*
          (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
             Subscript[m, 1], Subscript[m, 2]]) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + 2*Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 1, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*(Subscript[el1, 1, 1, 2][
                 Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 2, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 2, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 2, 1, 2, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))) - 2*Subscript[ey0, 1, 1][
           Subscript[m, 1], Subscript[m, 2]]*
          (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
          (3*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Derivative[0, 1][Subscript[ey0, 1, 1]][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*(2*Subscript[ey2, 3, 1, 2, 2][Subscript[m, 
                1], Subscript[m, 2]]*Derivative[0, 1][Subscript[el0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[ey1, 3, 1, 2][
               Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                Subscript[el1, 1, 1, 2]][Subscript[m, 1], Subscript[m, 2]]) - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (2*Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Derivative[0, 1][Subscript[el0, 1, 1]][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]*Derivative[0, 1][Subscript[ey0, 1, 1]][
               Subscript[m, 1], Subscript[m, 2]] + Subscript[el0, 1, 1][
               Subscript[m, 1], Subscript[m, 2]]*(2*Subscript[ey2, 3, 1, 2, 
                  2][Subscript[m, 1], Subscript[m, 2]]*Derivative[0, 1][
                  Subscript[ey0, 1, 1]][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
                Derivative[0, 1][Subscript[ey1, 1, 1, 2]][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^5)}}]|>, 
 "elasticityGradient" -> <|"B0" -> SparseArray[Automatic, {1, 2, 1, 2}, 0, 
     {1, {{0, 4}, {{1, 1, 1}, {1, 1, 2}, {2, 1, 1}, {2, 1, 2}}}, 
      {((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]))*(Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^4, 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]])))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]])) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]))*(Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]])))*(Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]])) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]))*(Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]))))/
        (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^4), 
       ((Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
            Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
           Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))*
          (Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])) + 
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
              Subscript[el1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              (Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
               Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
                Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 
                  2]]))) + (Subscript[ey0, 1, 1][Subscript[m, 1], 
              Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
             Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
             Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]]))*(Subscript[k, 2, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 2, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 2, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]])) + Subscript[k, 3, 3][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 1, 1][Subscript[m, 1], 
                Subscript[m, 2]]^2*Subscript[el1, 3, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey0, 3, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[ey1, 1, 1, 2][Subscript[m, 1], 
               Subscript[m, 2]] - Subscript[ey0, 1, 1][Subscript[m, 1], 
               Subscript[m, 2]]*(Subscript[ey0, 3, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[el1, 1, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]] + Subscript[el0, 1, 1][Subscript[m, 1], 
                 Subscript[m, 2]]*Subscript[ey1, 3, 1, 2][Subscript[m, 1], 
                 Subscript[m, 2]]))))/Subscript[ey0, 1, 1][Subscript[m, 1], 
          Subscript[m, 2]]^4}}], "C0" -> SparseArray[Automatic, {1, 1, 2, 2}, 
     0, {1, {{0, 4}, {{1, 1, 1}, {1, 1, 2}, {1, 2, 1}, {1, 2, 2}}}, 
      {-(((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[el2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1], Subscript[m, 
                 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^4) + (Subscript[el0, 2, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
            Subscript[m, 2]] - (Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
          Subscript[el2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
          Subscript[el2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]), 
       -1/2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[el2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^4 + (Subscript[el0, 2, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
            Subscript[m, 2]] - (Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
          Subscript[el2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
          Subscript[el2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])), 
       -1/2*((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[el2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey2, 1, 1, 1, 2][Subscript[m, 1], Subscript[m, 
                 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^4 + (Subscript[el0, 2, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
            Subscript[m, 2]] - (Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
          Subscript[el2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]]))/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2) + 
          Subscript[el2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 1][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el1, 1, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            2*Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           (2*Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])), 
       -(((Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]) + 
            Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]) - 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 2, 2][Subscript[m, 1], Subscript[m, 2]] + 
              2*Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]^2*
               Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))*
           (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]^2 + 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2*
             Subscript[el2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
            Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
              Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
               Subscript[ey2, 1, 1, 2, 2][Subscript[m, 1], Subscript[m, 
                 2]])))/Subscript[ey0, 1, 1][Subscript[m, 1], 
            Subscript[m, 2]]^4) + (Subscript[el0, 2, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], 
            Subscript[m, 2]] + Subscript[el0, 3, 1][Subscript[m, 1], 
            Subscript[m, 2]]*Subscript[k, 2, 3][Subscript[m, 1], 
            Subscript[m, 2]] - (Subscript[el0, 1, 1][Subscript[m, 1], 
             Subscript[m, 2]]*(Subscript[ey0, 2, 1][Subscript[m, 1], 
               Subscript[m, 2]]*Subscript[k, 2, 2][Subscript[m, 1], Subscript[
                m, 2]] + Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 
                2]]*Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
          Subscript[el2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 2, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 2, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]) + 
        (Subscript[el0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
          Subscript[el0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
           Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 2, 3][Subscript[m, 1], Subscript[m, 2]] + 
             Subscript[ey0, 3, 1][Subscript[m, 1], Subscript[m, 2]]*
              Subscript[k, 3, 3][Subscript[m, 1], Subscript[m, 2]]))/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])*
         ((Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
            Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]]^2 + 
          Subscript[el2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]] - 
          (Subscript[el1, 1, 1, 2][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey1, 3, 1, 2][Subscript[m, 1], Subscript[m, 2]] + 
            Subscript[el0, 1, 1][Subscript[m, 1], Subscript[m, 2]]*
             Subscript[ey2, 3, 1, 2, 2][Subscript[m, 1], Subscript[m, 2]])/
           Subscript[ey0, 1, 1][Subscript[m, 1], Subscript[m, 2]])}}]|>, 
 "boundaryTerms" -> <|"a0" -> zeroDimensionTensor, 
   "k1" -> zeroDimensionTensor|>, "solvabilityConditions" -> <||>|>
