<|"microscopicDoF" -> <|"Y0" -> SparseArray[Automatic, {1, 1}, 0, 
     {1, {{0, 1}, {{1}}}, {-((Subscript[el0, 1, 1][Subscript[m, 1]]*
           (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
              Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
             Subscript[k, 1, 2][Subscript[m, 1]]) + 
          Subscript[el0, 2, 1][Subscript[m, 1]]*
           (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
             Subscript[k, 2, 2][Subscript[m, 1]]))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]]))}}], 
   "Y0p" -> SparseArray[Automatic, {1, 1, 1}, 0, {1, {{0, 1}, {{1, 1}}}, 
      {(-(Subscript[ey0, 1, 1][Subscript[m, 1]]*
           (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
              Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
             Subscript[el1, 2, 1, 1][Subscript[m, 1]])) - 
         Subscript[ey0, 2, 1][Subscript[m, 1]]*
          (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
             Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
            Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
         Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
           Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
         Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
           Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
         Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
           Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
         Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
           Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
           Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
          Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
           Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
          Subscript[k, 2, 2][Subscript[m, 1]])}}], 
   "Y1" -> SparseArray[Automatic, {1, 1, 1, 1}, 0, {1, {{0, 1}, {{1, 1, 1}}}, 
      {(Subscript[ey0, 2, 1][Subscript[m, 1]]*
          (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
              Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*(
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                    1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
              (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]])) - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
             Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
            Subscript[k, 1, 2][Subscript[m, 1]]*
            (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 1, 1]][Subscript[m, 1]] + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 1, 2]][Subscript[m, 1]] + 
             Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                 2, 1, 1]][Subscript[m, 1]]) + Subscript[k, 1, 1][
             Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
              (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[el0, 1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 
                  1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
              (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                 Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
              (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                 Subscript[m, 1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
          (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
              Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*(
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]])) + 
             Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                    1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
              (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[el0, 1, 1]][Subscript[m, 1]] + 2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                   Subscript[m, 1]])))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}]|>, 
 "microscopicStrain" -> <|"F0" -> SparseArray[Automatic, {2, 1}, 0, 
     {1, {{0, 1, 2}, {{1}, {1}}}, 
      {-(((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
             Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]])*
          (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
            Subscript[k, 2, 2][Subscript[m, 1]]))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])), 
       ((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
            Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]])*
         (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
           Subscript[k, 1, 2][Subscript[m, 1]]))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
           Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
          Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
           Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
          Subscript[k, 2, 2][Subscript[m, 1]])}}], 
   "F0p" -> SparseArray[Automatic, {2, 1, 1}, 0, 
     {1, {{0, 1, 2}, {{1, 1}, {1, 1}}}, 
      {(-(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 2][
            Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
         Subscript[ey0, 2, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
           (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
               Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
           (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]])*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1]]))/(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          Subscript[k, 1, 1][Subscript[m, 1]] + 
         2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
           Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
         Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
           Subscript[m, 1]]), (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
           Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
          (-(Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]]) + 
           (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1]]) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
               Subscript[m, 1]] - Subscript[k, 1, 2][Subscript[m, 1]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
           (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]])*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1]]))/(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          Subscript[k, 1, 1][Subscript[m, 1]] + 
         2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
           Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
         Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
           Subscript[m, 1]])}}], "F0s" -> SparseArray[Automatic, {2, 1, 1, 
     1}, 0, {1, {{0, 1, 2}, {{1, 1, 1}, {1, 1, 1}}}, 
      {(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
          (-(Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
              Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
               Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]]) + Subscript[el2, 1, 1, 1, 1][
         Subscript[m, 1]] - ((Subscript[el0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]) + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]))*
          Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]])/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]]), 
       (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
          (-(Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
              Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
               Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]]) + Subscript[el2, 2, 1, 1, 1][
         Subscript[m, 1]] - ((Subscript[el0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]) + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]))*
          Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]])/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])}}], 
   "F1" -> SparseArray[Automatic, {2, 1, 1, 1}, 0, 
     {1, {{0, 1, 2}, {{1, 1, 1}, {1, 1, 1}}}, 
      {(Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
           Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
            Subscript[k, 2, 2][Subscript[m, 1]]*
            (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 
                 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*
            (2*Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
             Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[
                m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
             Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[
                m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                1]]) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
             Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                 Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
           Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
            Subscript[k, 2, 2][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[
                m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 
                2, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
              (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 2*
                Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) - Subscript[k, 1, 2][
               Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[ey0, 2, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 2, 2][Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[
                m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
              Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[
                m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
              (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] - 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]]) - Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                 Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]])) - 
             Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                    1]])))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
          (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
              Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey1, 2, 1, 1]][Subscript[m, 1]])) + 
           Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[
                m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[
                m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
              (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                 Subscript[m, 1]]))) - Subscript[ey0, 1, 1][Subscript[m, 1]]^
           2*(Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
            Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
           Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
            Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
            Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] - 
             Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]])) - 
             Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2, 
       (-(Subscript[ey0, 1, 1][Subscript[m, 1]]^3*Subscript[ey1, 2, 1, 1][
            Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]^2*
             Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
            Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
              Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
             (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
          (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
              Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
             (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]])*Derivative[1][Subscript[ey0, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]^2*
              (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
               Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
             Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
           Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
            Subscript[k, 1, 1][Subscript[m, 1]]*
            (-(Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] - Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
           Subscript[el0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
               2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]])*Derivative[1][Subscript[ey0, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
              (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-(Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
              (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                 Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]])) + 
             Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))) + 
         Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
               Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] - Subscript[k, 1, 2][Subscript[m, 1]]*
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]])) - 
             Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*(-2*Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                    1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                   Subscript[m, 1]])))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}], 
   "F1p" -> SparseArray[Automatic, {2, 1, 1, 1, 1}, 0, 
     {1, {{0, 1, 2}, {{1, 1, 1, 1}, {1, 1, 1, 1}}}, 
      {(-2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[el0, 1, 1]][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 1, 1][
               Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) - Subscript[ey0, 2, 1][
               Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 1, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]]))) - Subscript[ey0, 1, 1][
             Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (2*Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                  Subscript[el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]]) + Subscript[k, 1, 
                  1][Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]]) + Subscript[k, 1, 
                  2][Subscript[m, 1]]*(3*Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
               Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   k, 1, 1]][Subscript[m, 1]] - Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(3*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
         Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
          ((Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                 Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[el1, 2, 1, 1][Subscript[m, 1]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[k, 1, 1]][Subscript[m, 1]] + 
             2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]])) + 
           (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
               Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
              Subscript[k, 2, 2][Subscript[m, 1]])*
            (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 
                2, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[el0, 2, 1]][Subscript[m, 1]] + 
             Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[
                m, 1]] - (Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]])*
              Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
             (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[el1, 2, 1, 1][Subscript[m, 1]])*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 1, 2]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 2, 2]][Subscript[m, 1]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[el1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                   el1, 1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[el1, 2, 1, 1]][
                 Subscript[m, 1]]) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[el1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[el1, 2, 1, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[el1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                 ey1, 1, 1, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
               Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
              Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
               Subscript[m, 1]])) + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                      1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2, 
       (-2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[el0, 1, 1]][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 1, 1][
               Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) - Subscript[ey0, 2, 1][
               Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 1, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]]))) - Subscript[ey0, 1, 1][
             Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (2*Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                  Subscript[el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]]) + Subscript[k, 1, 
                  1][Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]]) + Subscript[k, 1, 
                  2][Subscript[m, 1]]*(3*Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
               Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 2, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   k, 1, 1]][Subscript[m, 1]] - Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(3*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
         Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
          ((Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                 Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[el1, 2, 1, 1][Subscript[m, 1]]) - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[k, 1, 1]][Subscript[m, 1]] + 
             2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]])) + 
           (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
               Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
              Subscript[k, 2, 2][Subscript[m, 1]])*
            (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 
                2, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
               Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[el0, 2, 1]][Subscript[m, 1]] + 
             Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[
                m, 1]] - (Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]])*
              Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
             (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[el1, 2, 1, 1][Subscript[m, 1]])*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 1, 2]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[k, 2, 2]][Subscript[m, 1]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[el1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                   el1, 1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[el1, 2, 1, 1]][
                 Subscript[m, 1]]) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[el1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[el1, 2, 1, 
                  1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[el1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
               Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                 ey1, 1, 1, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
               Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
              Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
               Subscript[m, 1]])) + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                      1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}], 
   "F2" -> SparseArray[Automatic, {2, 1, 1, 1, 1, 1}, 0, 
     {1, {{0, 1, 2}, {{1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}}}, 
      {-((Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*
           (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                2*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]))*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]])) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^
                2*Subscript[k, 2, 2][Subscript[m, 1]])*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]]^2 + 
                4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + 
                4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + 
                4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] + 
                2*Subscript[k, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]^2 + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                2*Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + 2*Subscript[k, 1, 2][Subscript[m, 1]]*
                 (2*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[2][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]]) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(2*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]]*
                 (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                 (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[2][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[2][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
          Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
           (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                2*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]))*
             (Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                         1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                     (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                     Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 
                      1]][Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 
                     1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(Subscript[k, 1, 1][
                   Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                   1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                        1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                   (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                         m, 1]]))))) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^
                2*Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                 Subscript[m, 1]]^2*Subscript[k, 2, 2][Subscript[m, 1]])*
             (2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                 Subscript[ey0, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
              2*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
               Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))*(
                Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]]) - 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))*(
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                Subscript[m, 1]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*(
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 
                        1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                      Subscript[m, 1]]) - Subscript[el0, 1, 1][Subscript[m, 
                     1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                     1]])) - Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  el0, 2, 1]][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey1, 2, 1, 1]][Subscript[m, 1]]) - 2*Subscript[el0, 2, 1][
                Subscript[m, 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 1, 1]][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey1, 2, 1, 1]][Subscript[m, 1]]) - Subscript[el0, 2, 1][
                Subscript[m, 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
               Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*(
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                     1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                     1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 1, 1]][Subscript[m, 1]]*(
                -(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                     1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]]*(
                -(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                     1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]^2*(-(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^2*
                 (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*(Derivative[1][Subscript[el0, 1, 1]][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] - Derivative[1][Subscript[el0, 2, 1]][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                      Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                       1]]*Derivative[2][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (3*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                       m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*
                 (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]])) - Subscript[el0, 2, 1][Subscript[m, 
                 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 
                 2][Subscript[m, 1]]*(Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + 2*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]^
                2*Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    2, 1, 1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + 2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]^2 + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2 - 
                Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] - 2*Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 1, 2]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  3*Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  3*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    5*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (5*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    3*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(3*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[el0, 2, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (-(Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
             Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            Subscript[k, 2, 2][Subscript[m, 1]])^3), 
       -((Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
           (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                2*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]))*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]])) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^
                2*Subscript[k, 2, 2][Subscript[m, 1]])*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]]^2 + 
                4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + 
                4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + 
                4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] + 
                2*Subscript[k, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]^2 + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                2*Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + 2*Subscript[k, 1, 2][Subscript[m, 1]]*
                 (2*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[2][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]]) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(2*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]]*
                 (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                 (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[2][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[2][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
          Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
           (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                2*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]))*
             (Subscript[ey0, 2, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                         1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                     (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                     Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 
                      1]][Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 
                     1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(Subscript[k, 1, 1][
                   Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                   1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                        1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                   (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                         m, 1]]))))) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^
                2*Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                 Subscript[m, 1]]^2*Subscript[k, 2, 2][Subscript[m, 1]])*
             (2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                 Subscript[ey0, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
              2*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
               Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))*(
                Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]]) - 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))*(
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                Subscript[m, 1]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*(
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 
                        1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                      Subscript[m, 1]]) - Subscript[el0, 1, 1][Subscript[m, 
                     1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                     1]])) - Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  el0, 2, 1]][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey1, 2, 1, 1]][Subscript[m, 1]]) - 2*Subscript[el0, 2, 1][
                Subscript[m, 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 1, 1]][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey1, 2, 1, 1]][Subscript[m, 1]]) - Subscript[el0, 2, 1][
                Subscript[m, 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
               Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*(
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                     1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                 Subscript[k, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                     1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 1, 1]][Subscript[m, 1]]*(
                -(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                     1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]]*(
                -(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                     1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                 Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    1, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]^2*(-(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^2*
                 (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*(Derivative[1][Subscript[el0, 1, 1]][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] - Derivative[1][Subscript[el0, 2, 1]][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                      Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                       1]]*Derivative[2][Subscript[k, 1, 2]][Subscript[m, 
                       1]]) + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (3*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                       m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*
                 (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]])) - Subscript[el0, 2, 1][Subscript[m, 
                 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 
                 2][Subscript[m, 1]]*(Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + 2*Derivative[1][Subscript[k, 1, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                   Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]^
                2*Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    el0, 1, 1]][Subscript[m, 1]]*Derivative[1][Subscript[ey1, 
                    2, 1, 1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]] + 2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                  Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
               Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]^2 + Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2 - 
                Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                  Subscript[m, 1]] - 2*Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 1, 2]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  3*Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  3*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    5*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (5*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    3*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(3*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[el0, 2, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   (-(Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))/
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
             Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            Subscript[k, 2, 2][Subscript[m, 1]])^3)}}], 
   "F2tilde" -> SparseArray[Automatic, {2, 1, 1, 1, 1}, 0, 
     {1, {{0, 1, 2}, {{1, 1, 1, 1}, {1, 1, 1, 1}}}, 
      {(Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*
          ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
              (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[k, 1, 1]][Subscript[m, 1]] + 
             2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]])) - 
           (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
               Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
              Subscript[k, 2, 2][Subscript[m, 1]])*
            ((Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                Subscript[el0, 1, 1]][Subscript[m, 1]] + 
             (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                Subscript[el0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1]] + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
         Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                      1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2, 
       (Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
          ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
              (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]))*
            (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                Subscript[k, 1, 1]][Subscript[m, 1]] + 
             2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]]) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]])) - 
           (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
               Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
              Subscript[k, 2, 2][Subscript[m, 1]])*
            ((Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                Subscript[el0, 1, 1]][Subscript[m, 1]] + 
             (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                Subscript[el0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 1]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1]] + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
         Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                      1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
               Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                 Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey1, 2, 1, 1]][Subscript[m, 1]]) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}]|>, 
 "LagrangeMultipliers" -> <|"G0" -> zeroDimensionTensor, 
   "G0p" -> zeroDimensionTensor, "G1" -> zeroDimensionTensor|>, 
 "elasticityLeadingOrder" -> 
  <|"K0" -> SparseArray[Automatic, {1, 1}, 0, {1, {{0, 1}, {{1}}}, 
      {((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
             Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]])^2*
         (-Subscript[k, 1, 2][Subscript[m, 1]]^2 + 
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
            Subscript[m, 1]]))/(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
          Subscript[k, 1, 1][Subscript[m, 1]] + 
         2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
           Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
         Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
           Subscript[m, 1]])}}], "K1" -> SparseArray[Automatic, {1, 1, 1, 1}, 
     0, {1, {{0, 1}, {{1, 1, 1}}}, 
      {(2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
            Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]])*
         (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
            Subscript[m, 1]])*(Subscript[ey0, 2, 1][Subscript[m, 1]]*
           Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
          Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
            Subscript[m, 1]])*((Subscript[el0, 1, 1][Subscript[m, 1]]*
             (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]) + 
            Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]))*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
             Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
            2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[k, 1, 2]][Subscript[m, 1]]) + 
            Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[k, 2, 2]][Subscript[m, 1]])) - 
          (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
              Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
             Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
              Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
             Subscript[k, 2, 2][Subscript[m, 1]])*
           ((Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][Subscript[
                el0, 1, 1]][Subscript[m, 1]] + 
            (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][Subscript[
                el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
              Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
              Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                  ey0, 2, 1]][Subscript[m, 1]] + Subscript[ey0, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
            Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                Subscript[m, 1]]))))/(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
           Subscript[k, 1, 1][Subscript[m, 1]] + 
          2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
            Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
          Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
            Subscript[m, 1]])^3}}], "K2" -> SparseArray[Automatic, {1, 1, 1, 
     1, 1, 1}, 0, {1, {{0, 1}, {{1, 1, 1, 1, 1}}}, 
      {((Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
             Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] - Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]) - 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]]) + Subscript[k, 1, 
                  2][Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
              Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 1, 1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]]) - Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                  Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                   2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                      1]])))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey1, 2, 1, 1]][Subscript[m, 1]])) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
               Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] - Subscript[k, 1, 2][
                 Subscript[m, 1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) - 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                      1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                  Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (-((Subscript[k, 1, 2][Subscript[m, 1]]^2 - Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*(
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                        1]][Subscript[m, 1]] - Subscript[ey1, 1, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                      Subscript[m, 1]])))) + Subscript[ey0, 2, 1][Subscript[
                m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) - Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]]) - Subscript[el0, 1, 
                  1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]^3*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
             Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[
                m, 1]]*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
             Subscript[k, 1, 1][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]]*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (2*Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*(-(Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] - 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^3*(2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                         1]][Subscript[m, 1]]))))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + 2*Subscript[k, 1, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]^2*((Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                    Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 
                      1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                       1]][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[
                      m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                     Subscript[m, 1]])) - Subscript[k, 1, 1][Subscript[m, 1]]*
                (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[ey1, 2, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                         1]][Subscript[m, 1]])))))) - 
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^3*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]^2*
              Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(
                Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
              Subscript[k, 1, 1][Subscript[m, 1]]*(-(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
                 2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]])*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                    Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[k, 2, 2][
                  Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (2*Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) - Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*(-2*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                      1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            ((Subscript[k, 1, 2][Subscript[m, 1]]^2 - Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*Derivative[1][
                Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-2*Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^2*
                (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) - 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(-(Subscript[el0, 1, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                      1]])))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (-(Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]) - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*(-(Subscript[k, 2, 2][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]) + Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey1, 2, 1, 1]][Subscript[m, 1]])) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey1, 2, 1, 1]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*((Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]])*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                (-(Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                      1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]^3*(2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         1]][Subscript[m, 1]] + 2*Subscript[ey1, 2, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                       Subscript[m, 1]])) + Subscript[k, 1, 1][Subscript[m, 
                    1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 
                      1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                       2]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                      1]]*(2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]]) - Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(-(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] - Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                      1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + 2*Subscript[ey1, 2, 1, 1][Subscript[
                      m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^3*
                (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                      1]])) - Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  ((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]] + Subscript[el0, 1, 1][
                       Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]])*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) - 
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
             Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]])*
          (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
           Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]])*(-(Subscript[ey0, 2, 1][Subscript[m, 1]]*
             (Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*(
                2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                       1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]))*
                 ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 
                         1][Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 
                         1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 2][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 
                         1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                         1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]]))*
                   (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                     (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 
                         2][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                          1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                        Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                         1]])) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                     Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 
                       1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[
                       m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 
                       2][Subscript[m, 1]])*((Subscript[ey0, 1, 1][Subscript[
                         m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 
                         2][Subscript[m, 1]])*Derivative[1][Subscript[el0, 1, 
                        1]][Subscript[m, 1]] + (Subscript[ey0, 1, 1][
                        Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                         1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                        Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                         1]] + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                        Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                         1]] + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                    Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 
                     1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                     Subscript[m, 1]]^2*Subscript[k, 2, 2][Subscript[m, 1]])*
                 ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 
                         1][Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 
                         1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 2][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 
                         1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                         1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]]))*
                   (2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]^2 + 
                    4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    2*Subscript[k, 1, 1][Subscript[m, 1]]*(Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]]^2 + 
                      Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                    2*Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 
                       2][Subscript[m, 1]]*Derivative[2][Subscript[ey0, 2, 
                        1]][Subscript[m, 1]] + 2*Subscript[k, 1, 2][Subscript[
                       m, 1]]*(2*Derivative[1][Subscript[ey0, 1, 1]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                        Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                         1]]*Derivative[2][Subscript[ey0, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                       Derivative[2][Subscript[ey0, 2, 1]][Subscript[m, 
                         1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                     Derivative[2][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 
                       1][Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                        1]]^2*Derivative[2][Subscript[k, 2, 2]][Subscript[m, 
                       1]]) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                     Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 
                       1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[
                       m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 
                       2][Subscript[m, 1]])*(2*Derivative[1][Subscript[el0, 
                        1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[
                         m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[
                         m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                    (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                        Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                         1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
                     Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                        Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                         1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
                     Derivative[2][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(
                2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                       1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]))*
                 (Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 
                          2][Subscript[m, 1]] + Subscript[el0, 2, 1][
                         Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                          1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                        Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                        (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                     (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                          Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                          Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                          m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                       (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 
                          2, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          el0, 1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]]) - Subscript[el0, 1, 1][
                          Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                          Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                          Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                     Subscript[m, 1]]^2*(Subscript[k, 1, 1][Subscript[m, 1]]^
                      2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                        Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                          Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                       (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
                   (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 
                          1][Subscript[m, 1]] + Subscript[el0, 2, 1][
                         Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                          1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                        Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                        (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                     (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 
                          2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                          Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                          Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[
                         m, 1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                         Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                         (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                         (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                       (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                          1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                          1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                       (2*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          el0, 1, 1]][Subscript[m, 1]] - Subscript[el0, 2, 1][
                          Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                          1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                          Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[
                          m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]]))))) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                   Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                     1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 
                     1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                    Subscript[m, 1]])*(2*Subscript[ey0, 1, 1][Subscript[m, 
                     1]]*Subscript[k, 1, 1][Subscript[m, 1]]^2*
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                  (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                      Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 
                       1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
                   Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                        1]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[
                       m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                        Subscript[m, 1]])) - (Subscript[el0, 1, 1][Subscript[
                       m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]])*Derivative[1][Subscript[ey0, 2, 1]][
                    Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                        1]][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[
                       m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
                  2*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 
                     1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                        Subscript[m, 1]]))*(Subscript[k, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) - 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                        Subscript[m, 1]]))*(Subscript[k, 1, 2][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                     Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[ey0, 2, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                   (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                        Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                         1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                     (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]]) - Subscript[el0, 1, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                         1]])) - Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                  2*Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 
                     1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
                     Subscript[m, 1]]^2*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                     (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 
                       1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                         1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                     (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]])) + Subscript[ey0, 2, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                      Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                         Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                       1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          k, 1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                     (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                        Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          k, 1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                          2]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                          Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                    Subscript[m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                      Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                         Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                       1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          k, 1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]]) + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                     (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                        Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          k, 1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                          2]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                          Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                     Subscript[m, 1]]^2*Subscript[k, 1, 1][Subscript[m, 1]]^2*
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[2][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                      Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                      Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                      Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                   (-(Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]]^2) + Subscript[k, 1, 1][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^
                      2*(Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[2][Subscript[el0, 2, 1]][Subscript[m, 
                         1]]) - Subscript[k, 1, 2][Subscript[m, 1]]*
                     (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] - 
                        Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                        Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(3*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                          Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                        Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) - Subscript[k, 1, 2][Subscript[m, 1]]^2*
                     (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                         1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Derivative[2][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                         1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                     (2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      2*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                          1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                          1]] + Derivative[1][Subscript[el0, 2, 1]][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 
                          1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                         Derivative[2][Subscript[el0, 2, 1]][Subscript[m, 
                          1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[k, 1, 1]][Subscript[m, 
                          1]]) + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                          1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + 2*Derivative[1][Subscript[el0, 2, 1]][
                          Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                          Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 
                          1]]*Derivative[2][Subscript[el0, 1, 1]][Subscript[
                          m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
                     Subscript[m, 1]]^2*Subscript[k, 1, 2][Subscript[m, 1]]*
                   (Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + 2*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                       m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[2][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                    Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + 2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     (2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                      Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 
                     1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 
                        1]][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                        2]][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[
                       m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2 + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         2]][Subscript[m, 1]]^2 - Subscript[k, 1, 1][
                      Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                        1]][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                        2]][Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[
                       m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                    2*Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                      Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                       1]]*Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 
                       1, 1, 1][Subscript[m, 1]]*Derivative[2][Subscript[k, 
                        1, 2]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                      Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                          1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                          1]][Subscript[m, 1]] + 3*Subscript[el0, 2, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                          1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                        Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]] + 3*Subscript[el0, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*(3*Derivative[1][Subscript[el0, 1, 
                          1]][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          1]][Subscript[m, 1]] + 5*Derivative[1][Subscript[
                          el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]] + 
                        2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                        Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 1]][Subscript[m, 1]] + 
                        Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       (5*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                          1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + 3*Derivative[1][Subscript[el0, 2, 1]][
                          Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                          Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[
                          m, 1]]*Derivative[2][Subscript[el0, 2, 1]][
                          Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                          m, 1]]*Derivative[2][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                     (3*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                         1]] + 3*Derivative[1][Subscript[el0, 1, 1]][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]] + 2*Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[2][Subscript[el0, 1, 1]][
                        Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*Derivative[2][Subscript[el0, 2, 1]][
                        Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 
                         1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][Subscript[
                         m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Derivative[2][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                         1]]) - Subscript[k, 2, 2][Subscript[m, 1]]*
                     (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      2*Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                        Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       (-(Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                         Derivative[2][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 
                         1][Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 
                          1, 1]][Subscript[m, 1]])))))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
              (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]]))*
                ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                       Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                       Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 
                      1]]*(Subscript[ey0, 1, 1][Subscript[m, 1]]*
                      Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 
                        1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                        1]]))*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                    (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 
                        2][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + 2*Subscript[k, 2, 2][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                        1]])) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                    Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                      1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 
                      1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                     Subscript[m, 1]])*((Subscript[ey0, 1, 1][Subscript[m, 
                        1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                       Subscript[m, 1]])*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + (Subscript[ey0, 1, 1][Subscript[m, 
                        1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                       Subscript[m, 1]])*Derivative[1][Subscript[el0, 2, 1]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                       Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
               (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                   Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                  Subscript[k, 2, 2][Subscript[m, 1]])*
                ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                       Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                       Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 
                      1]]*(Subscript[ey0, 1, 1][Subscript[m, 1]]*
                      Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 
                        1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                        1]]))*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]^2 + 
                   4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   4*Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   4*Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   2*Subscript[k, 1, 1][Subscript[m, 1]]*(Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]]^2 + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                   2*Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*Derivative[2][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]] + 2*Subscript[k, 1, 2][Subscript[m, 1]]*
                    (2*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[2][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 
                      1][Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^
                     2*Derivative[2][Subscript[k, 2, 2]][Subscript[m, 1]]) - 
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                     Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 
                      1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                     Subscript[m, 1]])*(2*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                    (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                       Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                        1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
                    Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                       Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 
                        1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
                    Derivative[2][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     2*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 2, 2]][Subscript[m, 1]])))) + 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]]))*
                (Subscript[ey0, 2, 1][Subscript[m, 1]]*
                  (-((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 
                         2][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[
                         m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
                     (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 
                          1, 1]][Subscript[m, 1]] + Subscript[k, 2, 2][
                        Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                         1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                       (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                    (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                         Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                         Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                          1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                      (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]]) - Subscript[el0, 1, 1][
                         Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                      Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                       Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                          1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                          m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                  (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                        1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                    (Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                         Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[
                        m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                   Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 
                         2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[
                         m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                       Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 
                          2, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                        Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                          Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                          1]][Subscript[m, 1]]))) + Subscript[ey0, 2, 1][
                     Subscript[m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 
                         1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                       (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                      (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 
                          2, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                         Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                        (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                      (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                          1]] + 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                          1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                      (2*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          el0, 1, 1]][Subscript[m, 1]] - Subscript[el0, 2, 1][
                         Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                          1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                        (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                          Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                          Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                          1]]))))) - (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                  Subscript[k, 1, 1][Subscript[m, 1]] + 2*Subscript[ey0, 1, 
                    1][Subscript[m, 1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                    Subscript[m, 1]]^2*Subscript[k, 2, 2][Subscript[m, 1]])*
                (2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                 (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                  (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]])) - (Subscript[el0, 1, 1][Subscript[
                      m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]])*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
                 2*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))*(Subscript[k, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]]) - 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))*(Subscript[k, 1, 2][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                    Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] + 2*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                  (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                       Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                        1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                      (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                       Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                          1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                          m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                          1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                      (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                      Subscript[m, 1]]^2*Derivative[1][Subscript[ey1, 1, 1, 
                       1]][Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 
                      1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) - 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                 2*Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
                   Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
                    Subscript[m, 1]]^2*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                   Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                       Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 
                      1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]]*(Subscript[k, 1, 2][
                     Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                        1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                       Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 
                      1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                  (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                       Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                        1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                      (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                       1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                        1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                          1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                         Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                         Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[
                        m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         1, 1]][Subscript[m, 1]] + 2*Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                       Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[
                        m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                    (2*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         1, 1]][Subscript[m, 1]] - Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                        1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                      (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 2, 2]][Subscript[m, 1]] + 
                       Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                          Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                    Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]*Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 
                    1]] - Subscript[ey0, 1, 1][Subscript[m, 1]]*
                  (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                    (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                    (Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                  (-(Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         2]][Subscript[m, 1]]^2) + Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^2*
                    (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[2][Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*(Derivative[1][Subscript[el0, 1, 
                          1]][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] - Derivative[1][Subscript[el0, 
                          2, 1]][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          2, 2]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                         Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                         Subscript[m, 1]]) + Subscript[el0, 1, 1][Subscript[
                        m, 1]]*(3*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                         Subscript[m, 1]] + Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[2][Subscript[k, 2, 2]][
                         Subscript[m, 1]])) - Subscript[k, 1, 2][Subscript[m, 
                       1]]^2*(Derivative[1][Subscript[el0, 1, 1]][Subscript[
                        m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                       Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                        1]]*Derivative[2][Subscript[ey1, 1, 1, 1]][Subscript[
                        m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     2*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                       Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      (2*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                       Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                       Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]])) - Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   2*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[2][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[2][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                   Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   2*Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    (2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                    (Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     2*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*Derivative[2][Subscript[ey1, 2, 1, 1]][
                     Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 
                    1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                  (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]^2 + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]]^2 - Subscript[k, 1, 1][Subscript[m, 
                      1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] - 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                     Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]] - 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                     Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                   2*Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                     Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[2][Subscript[k, 1, 2]][
                     Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[2][Subscript[k, 2, 2]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    (2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     3*Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                     3*Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      (3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       5*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       2*Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      (5*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       3*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                       2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[2][
                          Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 2]][Subscript[m, 1]] + 
                       Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]^2*(3*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     3*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]] + 2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[2][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[2][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]] + 2*Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[
                        m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                       Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                        1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                        m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      (Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                        Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[2][
                          Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      (-(Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                        Derivative[2][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                       Subscript[m, 1]]*Derivative[2][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                       Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                      Derivative[2][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                        1]])))))))/(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
           Subscript[k, 1, 1][Subscript[m, 1]] + 
          2*Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
            Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]] + 
          Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
            Subscript[m, 1]])^4}}], "K2tilde" -> SparseArray[Automatic, {1, 
     1, 1, 1, 1}, 0, {1, {{0, 1}, {{1, 1, 1, 1}}}, 
      {(2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
            Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]])*
         (Subscript[ey0, 2, 1][Subscript[m, 1]]*
           (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
            Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
              Subscript[m, 1]])*(Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]])) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
            Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 
                      1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                      1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                     1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                     (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[
                     m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(
                Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                        1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                   (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                         m, 1]])))))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
           (-Subscript[k, 1, 2][Subscript[m, 1]]^2 + 
            Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
              Subscript[m, 1]])*(Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
             ((Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]))*(
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]] + 
                2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]])) - 
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                  Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                 Subscript[k, 2, 2][Subscript[m, 1]])*(
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]])*Derivative[1][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
            Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 
                      1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                      1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
                Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                     1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                     (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[
                     m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(
                Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                  Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                    Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                     1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                       1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) + 
              Subscript[ey0, 1, 1][Subscript[m, 1]]*(
                -((Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                       Subscript[m, 1]]))) + Subscript[ey0, 2, 1][Subscript[
                   m, 1]]*(-(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                       Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                        m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                        1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                   (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                       1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                    2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                          2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                        Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                         1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                         m, 1]]))))))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^3}}]|>, 
 "elasticityCoupling" -> <|"A0" -> SparseArray[Automatic, {1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1}}}, 
      {((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
            Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]])*
         (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
            Subscript[m, 1]])*(-(Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
             Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           (Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
              Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
            (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 2, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 1][
              Subscript[m, 1]]) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
           (Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
              2*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                Subscript[m, 1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 1][
                Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
             Subscript[ey1, 2, 1, 1][Subscript[m, 1]]) + 
          Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
            Subscript[m, 1]]*(Subscript[ey0, 2, 1][Subscript[m, 1]]*
             (2*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
                Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
               Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
            Subscript[el0, 1, 1][Subscript[m, 1]]*
             (-(Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1]]) + 
            Subscript[el0, 2, 1][Subscript[m, 1]]*
             (-(Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}], 
   "A1" -> SparseArray[Automatic, {1, 1, 1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1, 1, 1}}}, 
      {(-((Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
            Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
              Subscript[m, 1]])*(Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
             Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
            Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[ey0, 1, 1][
                Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
              Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
             Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
              Subscript[m, 1]])*(Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
             Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
             (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 2, 1]][Subscript[m, 1]]) + Subscript[el0, 1, 1][
                Subscript[m, 1]]*(2*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                   Subscript[ey0, 2, 1]][Subscript[m, 1]]) - 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                  Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]))) + 
            Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
              Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                 (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
              Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 2, 1]][Subscript[m, 1]]) - 
                Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                        1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                      Subscript[m, 1]]))) + Subscript[ey0, 2, 1][
                Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]^2*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                       1]] - 2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                    Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                      Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                       1]])))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
             (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey1, 2, 1, 1]][Subscript[m, 1]])) + Subscript[k, 1, 1][
                Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))) - 
            Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(Subscript[el0, 1, 1][
                Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 2][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][
                Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                 Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 2]][Subscript[m, 1]] - Subscript[k, 1, 2][
                  Subscript[m, 1]]*(2*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]])) - 
                Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                       m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                  Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                        1]][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[
                       m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                      Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[
                       m, 1]])))))) + (Subscript[k, 1, 2][Subscript[m, 1]]^
            2 - Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]])*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
           Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[el1, 1, 1, 1][
               Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*
          (Subscript[ey0, 1, 1][Subscript[m, 1]]^3*Subscript[ey1, 2, 1, 1][
             Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]^2*
              Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                   el0, 2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]])) - Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(
                Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                    ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                  Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                  Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]])) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                  Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
              Subscript[k, 1, 1][Subscript[m, 1]]*(-(Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
                 2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]])*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                    Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[k, 2, 2][
                  Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                  Subscript[m, 1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                (-(Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                    Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey0, 1, 1]][Subscript[m, 1]] + Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (2*Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) - Subscript[k, 1, 2][Subscript[m, 1]]^2*
                (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                   Subscript[m, 1]]*(-2*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                      1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))) - 
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 2][
             Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-(Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]]) + 
             (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1]]) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                 Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
               Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                 Subscript[m, 1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            (-((Subscript[k, 1, 2][Subscript[m, 1]]^2 - Subscript[k, 1, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*(
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                 Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] - 
                Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 
                        1]][Subscript[m, 1]] - Subscript[ey1, 1, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                      Subscript[m, 1]])))) + Subscript[ey0, 2, 1][Subscript[
                m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]^2*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     el0, 2, 1]][Subscript[m, 1]] - Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]]) - Subscript[k, 1, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]]) - Subscript[el0, 1, 
                  1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]^3*
                Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 2, 2]][Subscript[m, 1]] + Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
             Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[
                m, 1]]*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                  Subscript[k, 1, 2]][Subscript[m, 1]] + Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
             Subscript[k, 1, 1][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                    1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]]*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (2*Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*(-(Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] - 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                    Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 
                    1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                    1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^3*(2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 
                         1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                         1]][Subscript[m, 1]]))))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + 2*Subscript[k, 1, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*Derivative[1][
                Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                  Subscript[m, 1]]^2*((Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 1, 2][Subscript[m, 1]]*(-(Subscript[el0, 2, 1][
                    Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]^2*(2*Subscript[ey1, 2, 
                      1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                       1]][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[
                      m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                     Subscript[m, 1]])) - Subscript[k, 1, 1][Subscript[m, 1]]*
                (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                     Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(2*Subscript[ey1, 2, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                         2, 1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                         1]][Subscript[m, 1]])))))) + 
         (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
             Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
           Subscript[ey0, 2, 1][Subscript[m, 1]]*
            (-(Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]]) + 
             (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 1, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 
                1][Subscript[m, 1]]) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                 Subscript[m, 1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
                Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
              Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*
          (Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            ((Subscript[k, 1, 2][Subscript[m, 1]]^2 - Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))*Derivative[1][
                Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
             Subscript[ey0, 2, 1][Subscript[m, 1]]*(-2*Subscript[el0, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                 Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]^2*
                (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]])) - 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(-(Subscript[el0, 1, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                      1]])))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            (-(Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
               Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]]) - 
             Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
               Subscript[m, 1]]*(-(Subscript[k, 2, 2][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                   Subscript[k, 1, 1]][Subscript[m, 1]]) + Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey1, 2, 1, 1]][Subscript[m, 1]])) + 
             Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] - 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                 Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][Subscript[
                     ey1, 2, 1, 1]][Subscript[m, 1]]))) - 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
             Subscript[m, 1]]*((Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
               Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]])*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                 Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                (-(Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                    Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                      1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 2][Subscript[m, 1]]^3*(2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]) - 
               Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(2*Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 
                         1]][Subscript[m, 1]] + 2*Subscript[ey1, 2, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                       Subscript[m, 1]])) + Subscript[k, 1, 1][Subscript[m, 
                    1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 
                      1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                       2]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                      1]]*(2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]]))) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]]) - Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) + 
               Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                   Subscript[m, 1]]*(Subscript[el0, 2, 1][Subscript[m, 1]]*
                    Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]))))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
            ((-Subscript[k, 1, 2][Subscript[m, 1]]^2 + Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                 Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*(-(Subscript[k, 2, 2][
                    Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] - Subscript[ey1, 1, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                      1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
                 Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
               Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                  Subscript[m, 1]]^2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] - 
               Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[k, 2, 2][
                 Subscript[m, 1]]*(Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + 2*Subscript[ey1, 2, 1, 1][Subscript[
                      m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^3*
                (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
               Subscript[k, 1, 2][Subscript[m, 1]]^2*(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] - Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                      1]])) - Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  ((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                        1, 1][Subscript[m, 1]] + Subscript[el0, 1, 1][
                       Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]])*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) - 
                 Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                     Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                    (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])))))) + 
         2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
             Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]])*
          (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
           Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
             Subscript[m, 1]])*(-(Subscript[ey0, 2, 1][Subscript[m, 1]]*
             (2*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]*(
                Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
                 (Subscript[k, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                   (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]])) - 
                Subscript[ey0, 2, 1][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                    Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]^2*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] - Subscript[k, 1, 1][Subscript[m, 1]]*
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                      Subscript[m, 1]]) - Subscript[ey0, 2, 1][Subscript[m, 
                     1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                     (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                        Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                        Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                         1]]))) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*
                 (2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                    Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                      Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     (Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 1]][Subscript[m, 1]] - 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                        Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                        Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                         1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                     (3*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
                Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(Subscript[el0, 1, 1][
                    Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                   (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]^2*Derivative[1][Subscript[ey0, 2, 1]][
                      Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                     (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 2, 1]][
                      Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                       1]]*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 1]][Subscript[m, 1]] - 
                    Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                     (3*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[el0, 2, 1]][Subscript[m, 1]] - 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]])))) - 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(
                (Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                      Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 
                       1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                     Subscript[el1, 2, 1, 1][Subscript[m, 1]]) - 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                     1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                     1]]*Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 
                     2, 1, 1][Subscript[m, 1]] - Subscript[el0, 2, 1][
                    Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
                 (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                     Subscript[k, 1, 1]][Subscript[m, 1]] + 
                  2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                    Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                      Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 
                       1]]*Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                       1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[k, 2, 2]][Subscript[m, 1]])) + 
                (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                    Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 
                     1]]*Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[ey0, 2, 1][
                     Subscript[m, 1]]^2*Subscript[k, 2, 2][Subscript[m, 1]])*
                 (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                  Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                    Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[el0, 2, 1]][Subscript[m, 1]] - 
                  (Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 1, 1, 
                       1][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 
                       1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]])*
                   Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                  (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 1, 1, 
                       1][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                       1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]])*
                   Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 1, 2]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                     Subscript[k, 2, 2]][Subscript[m, 1]] - 
                  Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[el1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[el1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                     Derivative[1][Subscript[el1, 1, 1, 1]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[el1, 2, 1, 1]][Subscript[m, 
                       1]]) - Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   (Subscript[el1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 1, 2]][Subscript[m, 1]] + 
                    Subscript[el1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[k, 2, 2]][Subscript[m, 1]] + 
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[el1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                       Subscript[el1, 2, 1, 1]][Subscript[m, 1]]) + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                  Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                    Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                    Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                     Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) - 
              Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                  Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                      Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 
                        1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                        1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                         1]][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[
                        m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                        Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                         1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                     (Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                          Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]]) - Subscript[el0, 1, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) - Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Subscript[k, 1, 2][Subscript[m, 1]]^2*Derivative[1][
                       Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                         1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                 (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 
                     1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                    Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                      Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                       1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                       1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                     Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                       1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                   (Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 
                          1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                        Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 
                       1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                         1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                          1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                        Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                          1]][Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                  Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                      Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 
                        1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                        1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                         1]][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[
                        m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 
                          1]] + Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                        Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 
                          1]]))) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
                   (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 
                        2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                         Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[
                          m, 1]])) + Subscript[k, 1, 2][Subscript[m, 1]]*
                     (2*Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 
                         1, 1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 
                          2, 1]][Subscript[m, 1]] + Subscript[el0, 1, 1][
                        Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[
                          m, 1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[
                          m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                          1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                       (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                        Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                         Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                          1]])) + Subscript[k, 1, 2][Subscript[m, 1]]^2*
                     (2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                      2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                    Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                        Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                         1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                       Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                      Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 
                          1, 1, 1][Subscript[m, 1]]*Derivative[1][Subscript[
                          k, 1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 
                          1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 
                          2]][Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[
                          m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                          Subscript[m, 1]]))))))) + 
           Subscript[ey0, 1, 1][Subscript[m, 1]]*
            (2*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]*
              (Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
                (Subscript[k, 1, 1][Subscript[m, 1]]^2*Derivative[1][
                    Subscript[el0, 1, 1]][Subscript[m, 1]] - 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]])) - 
               Subscript[ey0, 2, 1][Subscript[m, 1]]^2*(Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                  (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]^2*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] - Subscript[k, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]]) - Subscript[ey0, 2, 1][Subscript[m, 
                    1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                    (Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[k, 2, 2][
                       Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                       Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                        1]]))) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
                Subscript[ey0, 2, 1][Subscript[m, 1]]*
                (2*(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                   Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                     Subscript[m, 1]]*(Subscript[k, 1, 2][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    (Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 1]][Subscript[m, 1]] - 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                       Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                       Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                        1]]) + Subscript[k, 1, 2][Subscript[m, 1]]*
                    (3*Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]))) - 
               Subscript[ey0, 1, 1][Subscript[m, 1]]^2*(Subscript[el0, 1, 1][
                   Subscript[m, 1]]*Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[ey0, 2, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]^2*Derivative[1][Subscript[ey0, 2, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                    (Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 1, 1]][Subscript[m, 1]] - 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey0, 2, 1]][Subscript[m, 1]])) - 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                      Subscript[m, 1]]^2*Derivative[1][Subscript[el0, 2, 1]][
                     Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                    Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] - 
                   Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 1][Subscript[m, 1]]*
                    (3*Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] - 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]])))) - 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
              ((Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 
                      1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Subscript[el1, 2, 1, 1][Subscript[m, 1]]) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                     Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 
                      1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                    Subscript[el1, 2, 1, 1][Subscript[m, 1]]) - 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]])*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[k, 1, 1][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                     Subscript[m, 1]]*Derivative[1][Subscript[ey0, 1, 1]][
                     Subscript[m, 1]] + 2*Subscript[k, 2, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey0, 2, 1]][Subscript[m, 1]] + 
                   Subscript[ey0, 2, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 2, 2]][Subscript[m, 1]])) + 
               (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
                   Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
                  Subscript[k, 2, 2][Subscript[m, 1]])*(Subscript[k, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                 Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                   Subscript[m, 1]] - (Subscript[k, 1, 1][Subscript[m, 1]]*
                    Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[ey0, 1, 
                     1]][Subscript[m, 1]] - (Subscript[k, 1, 2][Subscript[m, 
                      1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 
                      1][Subscript[m, 1]])*Derivative[1][Subscript[ey0, 2, 
                     1]][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                    1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                  Derivative[1][Subscript[k, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                   Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                    Subscript[k, 1, 2]][Subscript[m, 1]] + 
                 Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                   Subscript[m, 1]] - Subscript[ey0, 1, 1][Subscript[m, 1]]*
                  (Subscript[el1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 1]][Subscript[m, 1]] + 
                   Subscript[el1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[k, 1, 2]][Subscript[m, 1]] + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Derivative[1][
                      Subscript[el1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[el1, 2, 1, 1]][Subscript[m, 1]]) - 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[el1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                     Subscript[m, 1]] + Subscript[el1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 2, 2]][Subscript[m, 
                      1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[el1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                      Subscript[el1, 2, 1, 1]][Subscript[m, 1]]) + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 1][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 1, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 2][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                 Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                   Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                   Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                    Subscript[ey1, 2, 1, 1]][Subscript[m, 1]])) - 
             Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 
                       1][Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 
                       1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                        1]][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[
                       m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 2, 2][
                     Subscript[m, 1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                      Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 1]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 2]][
                         Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                      1]]*(Subscript[k, 2, 2][Subscript[m, 1]]*
                      (Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                       Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[el0, 2, 1]][Subscript[m, 1]]) - 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]])) - Subscript[el0, 1, 1][Subscript[
                      m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]]^2*
                    Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                     Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                        1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[ey1, 1, 1, 1]][Subscript[m, 
                        1]]))) + Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
                (Subscript[k, 1, 1][Subscript[m, 1]]^2*Subscript[ey1, 1, 1, 
                    1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 1]][
                   Subscript[m, 1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 1, 1, 
                      1][Subscript[m, 1]]*Derivative[1][Subscript[k, 1, 1]][
                     Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 
                      1]]*Derivative[1][Subscript[k, 1, 2]][Subscript[m, 
                      1]] + Subscript[k, 1, 2][Subscript[m, 1]]*
                    Derivative[1][Subscript[ey1, 2, 1, 1]][Subscript[m, 
                      1]]) + Subscript[k, 1, 1][Subscript[m, 1]]*
                  (Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 1, 
                         1]][Subscript[m, 1]] + Subscript[ey1, 1, 1, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 1]][
                       Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 
                      1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]] + 
                     Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[k, 2, 2]][Subscript[m, 1]]) + 
                   Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[ey1, 2, 1, 
                        1][Subscript[m, 1]]*Derivative[1][Subscript[el0, 2, 
                         1]][Subscript[m, 1]] + Subscript[el0, 2, 1][
                       Subscript[m, 1]]*Derivative[1][Subscript[ey1, 2, 1, 
                         1]][Subscript[m, 1]]))) + Subscript[ey0, 1, 1][
                 Subscript[m, 1]]*(-((Subscript[el0, 1, 1][Subscript[m, 1]]*
                     Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 
                       1][Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 
                       1]])*(Subscript[k, 1, 1][Subscript[m, 1]]*
                     Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                       Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                    Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 
                       1][Subscript[m, 1]]*Derivative[1][Subscript[ey0, 2, 
                        1]][Subscript[m, 1]] + Subscript[k, 1, 2][Subscript[
                       m, 1]]*(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[ey0, 1, 1]][Subscript[m, 1]] + 
                      Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                         Subscript[ey0, 2, 1]][Subscript[m, 1]]))) + 
                 Subscript[ey0, 2, 1][Subscript[m, 1]]*
                  (-(Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
                      Subscript[m, 1]]*(Subscript[ey1, 1, 1, 1][Subscript[m, 
                         1]]*Derivative[1][Subscript[k, 1, 1]][Subscript[m, 
                         1]] + Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                       Derivative[1][Subscript[k, 1, 2]][Subscript[m, 1]])) + 
                   Subscript[k, 1, 2][Subscript[m, 1]]*(2*Subscript[k, 2, 2][
                       Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 2, 1]][Subscript[m, 
                        1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
                      (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 1]][Subscript[m, 1]] + 
                       Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*Derivative[1][
                          Subscript[k, 1, 2]][Subscript[m, 1]]) + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]])) + Subscript[k, 1, 2][Subscript[m, 
                       1]]^2*(2*Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*
                      Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 1]] + 
                     2*Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[el0, 2, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 2, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] + 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 2, 1, 1]][Subscript[m, 1]]) + 
                   Subscript[k, 1, 1][Subscript[m, 1]]*(2*Subscript[k, 1, 2][
                       Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                        1]]*Derivative[1][Subscript[el0, 1, 1]][Subscript[m, 
                        1]] - Subscript[el0, 2, 1][Subscript[m, 1]]*
                      Subscript[k, 2, 2][Subscript[m, 1]]*Derivative[1][
                        Subscript[ey1, 1, 1, 1]][Subscript[m, 1]] - 
                     Subscript[el0, 1, 1][Subscript[m, 1]]*(Subscript[ey1, 1, 
                          1, 1][Subscript[m, 1]]*Derivative[1][Subscript[k, 
                          1, 2]][Subscript[m, 1]] + Subscript[ey1, 2, 1, 1][
                         Subscript[m, 1]]*Derivative[1][Subscript[k, 2, 2]][
                         Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 
                          1]]*Derivative[1][Subscript[ey1, 2, 1, 1]][
                         Subscript[m, 1]]))))))))/
        (2*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
             Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
            Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
             Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
            Subscript[k, 2, 2][Subscript[m, 1]])^3)}}]|>, 
 "elasticityGradient" -> <|"B0" -> SparseArray[Automatic, {1, 1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1, 1}}}, 
      {((Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
            Subscript[m, 1]])*(-((Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
              Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
             Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey0, 2, 1][
               Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 1]] - 
             Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] + 
               Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                 Subscript[m, 1]]))*(Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
              Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
               Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]*
              (-(Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 
                   1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
                Subscript[ey1, 1, 1, 1][Subscript[m, 1]]) - 
             Subscript[ey0, 1, 1][Subscript[m, 1]]*(Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                   Subscript[m, 1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
                  Subscript[k, 1, 1][Subscript[m, 1]] + Subscript[el0, 2, 1][
                   Subscript[m, 1]]*Subscript[k, 1, 2][Subscript[m, 1]])*
                Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))) + 
          (Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[el1, 1, 1, 1][
              Subscript[m, 1]] - Subscript[ey0, 2, 1][Subscript[m, 1]]*
             (Subscript[ey0, 1, 1][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[ey1, 1, 1, 1][Subscript[m, 1]]) + 
            Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
              Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]])*
           (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 2][
              Subscript[m, 1]]*Subscript[el1, 2, 1, 1][Subscript[m, 1]] + 
            Subscript[ey0, 2, 1][Subscript[m, 1]]*(-(Subscript[ey0, 2, 1][
                 Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]]*
                Subscript[el1, 1, 1, 1][Subscript[m, 1]]) + 
              (Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
                  Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
                 Subscript[k, 2, 2][Subscript[m, 1]])*Subscript[ey1, 1, 1, 1][
                Subscript[m, 1]]) - Subscript[ey0, 1, 1][Subscript[m, 1]]*
             (Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                  Subscript[m, 1]]*Subscript[el1, 1, 1, 1][Subscript[m, 1]] - 
                Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[el1, 2, 1, 1][
                  Subscript[m, 1]]) + (Subscript[el0, 1, 1][Subscript[m, 1]]*
                 Subscript[k, 1, 2][Subscript[m, 1]] + Subscript[el0, 2, 1][
                  Subscript[m, 1]]*Subscript[k, 2, 2][Subscript[m, 1]])*
               Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}], 
   "C0" -> SparseArray[Automatic, {1, 1, 1, 1}, 0, {1, {{0, 1}, {{1, 1, 1}}}, 
      {((Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey0, 1, 1][
            Subscript[m, 1]] - Subscript[el0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]])*
         (Subscript[k, 1, 2][Subscript[m, 1]]^2 - 
          Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[k, 2, 2][
            Subscript[m, 1]])*(-(Subscript[ey0, 1, 1][Subscript[m, 1]]^3*
            Subscript[k, 1, 1][Subscript[m, 1]]*Subscript[el2, 2, 1, 1, 1][
             Subscript[m, 1]]) + Subscript[ey0, 2, 1][Subscript[m, 1]]*
           (Subscript[ey1, 1, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                  Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*(
                Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                  Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]])) + 
            Subscript[ey0, 2, 1][Subscript[m, 1]]^2*Subscript[k, 2, 2][
              Subscript[m, 1]]*Subscript[el2, 1, 1, 1, 1][Subscript[m, 1]] - 
            Subscript[ey0, 2, 1][Subscript[m, 1]]*(Subscript[k, 1, 2][
                Subscript[m, 1]]*(Subscript[el1, 1, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                Subscript[el0, 1, 1][Subscript[m, 1]]*Subscript[ey2, 1, 1, 1, 
                   1][Subscript[m, 1]]) + Subscript[k, 2, 2][Subscript[m, 1]]*
               (Subscript[el1, 2, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 1, 
                   1][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 
                   1]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]]))) + 
          Subscript[ey0, 1, 1][Subscript[m, 1]]^2*
           (Subscript[k, 1, 1][Subscript[m, 1]]*(Subscript[el1, 1, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
              Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[el2, 1, 1, 1, 
                 1][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 1]]*
               Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]) + 
            Subscript[k, 1, 2][Subscript[m, 1]]*(Subscript[el1, 2, 1, 1][
                Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] - 
              2*Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[el2, 2, 1, 1, 
                 1][Subscript[m, 1]] + Subscript[el0, 2, 1][Subscript[m, 1]]*
               Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]])) + 
          Subscript[ey0, 1, 1][Subscript[m, 1]]*
           (-(Subscript[ey1, 2, 1, 1][Subscript[m, 1]]*(Subscript[el0, 1, 1][
                 Subscript[m, 1]]*(Subscript[k, 1, 1][Subscript[m, 1]]*
                  Subscript[ey1, 1, 1, 1][Subscript[m, 1]] + 
                 Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][
                   Subscript[m, 1]]) + Subscript[el0, 2, 1][Subscript[m, 1]]*
                (Subscript[k, 1, 2][Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][
                   Subscript[m, 1]] + Subscript[k, 2, 2][Subscript[m, 1]]*
                  Subscript[ey1, 2, 1, 1][Subscript[m, 1]]))) + 
            Subscript[ey0, 2, 1][Subscript[m, 1]]^2*(2*Subscript[k, 1, 2][
                Subscript[m, 1]]*Subscript[el2, 1, 1, 1, 1][Subscript[m, 
                 1]] - Subscript[k, 2, 2][Subscript[m, 1]]*Subscript[el2, 2, 
                 1, 1, 1][Subscript[m, 1]]) + Subscript[ey0, 2, 1][
              Subscript[m, 1]]*(-(Subscript[k, 1, 1][Subscript[m, 1]]*
                (Subscript[el1, 1, 1, 1][Subscript[m, 1]]*Subscript[ey1, 1, 
                    1, 1][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[
                    m, 1]]*Subscript[ey2, 1, 1, 1, 1][Subscript[m, 1]])) + 
              Subscript[k, 1, 2][Subscript[m, 1]]*(-(Subscript[el1, 2, 1, 1][
                   Subscript[m, 1]]*Subscript[ey1, 1, 1, 1][Subscript[m, 
                    1]]) + Subscript[el1, 1, 1, 1][Subscript[m, 1]]*
                 Subscript[ey1, 2, 1, 1][Subscript[m, 1]] - 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey2, 1, 1, 1, 
                   1][Subscript[m, 1]] + Subscript[el0, 1, 1][Subscript[m, 
                   1]]*Subscript[ey2, 2, 1, 1, 1][Subscript[m, 1]]) + 
              Subscript[k, 2, 2][Subscript[m, 1]]*(Subscript[el1, 2, 1, 1][
                  Subscript[m, 1]]*Subscript[ey1, 2, 1, 1][Subscript[m, 1]] + 
                Subscript[el0, 2, 1][Subscript[m, 1]]*Subscript[ey2, 2, 1, 1, 
                   1][Subscript[m, 1]])))))/
        (Subscript[ey0, 1, 1][Subscript[m, 1]]^2*Subscript[k, 1, 1][
            Subscript[m, 1]] + 2*Subscript[ey0, 1, 1][Subscript[m, 1]]*
           Subscript[ey0, 2, 1][Subscript[m, 1]]*Subscript[k, 1, 2][
            Subscript[m, 1]] + Subscript[ey0, 2, 1][Subscript[m, 1]]^2*
           Subscript[k, 2, 2][Subscript[m, 1]])^2}}]|>, 
 "boundaryTerms" -> <|"a0" -> zeroDimensionTensor, 
   "k1" -> zeroDimensionTensor|>, "solvabilityConditions" -> <||>|>
