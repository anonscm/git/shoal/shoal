<|"microscopicDoF" -> <|"Y0" -> SparseArray[Automatic, {1, 1}, 0, 
     {1, {{0, 1}, {{1}}}, {-(Subscript[el0, 1, 1][]/Subscript[ey0, 1, 1][
          ])}}], "Y0p" -> SparseArray[Automatic, {1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1}}}, 
      {(-(Subscript[ey0, 1, 1][]*Subscript[el1, 1, 1, 1][]) + 
         Subscript[el0, 1, 1][]*Subscript[ey1, 1, 1, 1][])/
        Subscript[ey0, 1, 1][]^2}}], "Y1" -> zeroDimensionTensor|>, 
 "microscopicStrain" -> <|"F0" -> SparseArray[Automatic, {3, 1}, 0, 
     {1, {{0, 0, 1, 2}, {{1}, {1}}}, {Subscript[el0, 2, 1][] - 
        (Subscript[el0, 1, 1][]*Subscript[ey0, 2, 1][])/Subscript[ey0, 1, 1][
          ], Subscript[el0, 3, 1][] - (Subscript[el0, 1, 1][]*
          Subscript[ey0, 3, 1][])/Subscript[ey0, 1, 1][]}}], 
   "F0p" -> SparseArray[Automatic, {3, 1, 1}, 0, 
     {1, {{0, 0, 1, 2}, {{1, 1}, {1, 1}}}, 
      {Subscript[el1, 2, 1, 1][] + (Subscript[el0, 1, 1][]*
          Subscript[ey0, 2, 1][]*Subscript[ey1, 1, 1, 1][])/
         Subscript[ey0, 1, 1][]^2 - (Subscript[ey0, 2, 1][]*
           Subscript[el1, 1, 1, 1][] + Subscript[el0, 1, 1][]*
           Subscript[ey1, 2, 1, 1][])/Subscript[ey0, 1, 1][], 
       Subscript[el1, 3, 1, 1][] + (Subscript[el0, 1, 1][]*
          Subscript[ey0, 3, 1][]*Subscript[ey1, 1, 1, 1][])/
         Subscript[ey0, 1, 1][]^2 - (Subscript[ey0, 3, 1][]*
           Subscript[el1, 1, 1, 1][] + Subscript[el0, 1, 1][]*
           Subscript[ey1, 3, 1, 1][])/Subscript[ey0, 1, 1][]}}], 
   "F0s" -> SparseArray[Automatic, {3, 1, 1, 1}, 0, 
     {1, {{0, 1, 2, 3}, {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}}, 
      {(Subscript[el0, 1, 1][]*Subscript[ey1, 1, 1, 1][]^2)/
         Subscript[ey0, 1, 1][]^2 + Subscript[el2, 1, 1, 1, 1][] - 
        (Subscript[el1, 1, 1, 1][]*Subscript[ey1, 1, 1, 1][] + 
          Subscript[el0, 1, 1][]*Subscript[ey2, 1, 1, 1, 1][])/
         Subscript[ey0, 1, 1][], (Subscript[el0, 1, 1][]*
          Subscript[ey1, 1, 1, 1][]*Subscript[ey1, 2, 1, 1][])/
         Subscript[ey0, 1, 1][]^2 + Subscript[el2, 2, 1, 1, 1][] - 
        (Subscript[el1, 1, 1, 1][]*Subscript[ey1, 2, 1, 1][] + 
          Subscript[el0, 1, 1][]*Subscript[ey2, 2, 1, 1, 1][])/
         Subscript[ey0, 1, 1][], (Subscript[el0, 1, 1][]*
          Subscript[ey1, 1, 1, 1][]*Subscript[ey1, 3, 1, 1][])/
         Subscript[ey0, 1, 1][]^2 + Subscript[el2, 3, 1, 1, 1][] - 
        (Subscript[el1, 1, 1, 1][]*Subscript[ey1, 3, 1, 1][] + 
          Subscript[el0, 1, 1][]*Subscript[ey2, 3, 1, 1, 1][])/
         Subscript[ey0, 1, 1][]}}], "F1" -> zeroDimensionTensor, 
   "F1p" -> zeroDimensionTensor, "F2" -> zeroDimensionTensor, 
   "F2tilde" -> zeroDimensionTensor|>, "LagrangeMultipliers" -> 
  <|"G0" -> SparseArray[Automatic, {1, 1}, 0, {1, {{0, 1}, {{1}}}, 
      {(-(Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][]*
           (Subscript[ey0, 1, 1][]*Subscript[k, 1, 2][] + 
            Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
            Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][])) - 
         Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][]*
          (Subscript[ey0, 1, 1][]*Subscript[k, 1, 3][] + 
           Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
           Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]) + 
         Subscript[el0, 1, 1][]*(Subscript[ey0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 1, 2][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 1, 3][]) + 
           Subscript[ey0, 2, 1][]^2*Subscript[k, 2, 2][] + 
           2*Subscript[ey0, 2, 1][]*Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][
             ] + Subscript[ey0, 3, 1][]^2*Subscript[k, 3, 3][]))/
        Subscript[ey0, 1, 1][]^2}}], "G0p" -> SparseArray[Automatic, {1, 1, 
     1}, 0, {1, {{0, 1}, {{1, 1}}}, 
      {(Subscript[ey0, 2, 1][]^2*Subscript[k, 2, 2][]*Subscript[el1, 1, 1, 1][
           ] - Subscript[ey0, 1, 1][]^2*(Subscript[k, 1, 2][]*
            Subscript[el1, 2, 1, 1][] + Subscript[k, 1, 3][]*
            Subscript[el1, 3, 1, 1][]) + Subscript[ey0, 2, 1][]*
          (2*Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][]*
            Subscript[el1, 1, 1, 1][] - (Subscript[el0, 1, 1][]*
              Subscript[k, 1, 2][] + Subscript[el0, 2, 1][]*Subscript[k, 2, 
                2][] + Subscript[el0, 3, 1][]*Subscript[k, 2, 3][])*
            Subscript[ey1, 1, 1, 1][]) + Subscript[ey0, 3, 1][]*
          (Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]*
            Subscript[el1, 1, 1, 1][] - (Subscript[el0, 1, 1][]*
              Subscript[k, 1, 3][] + Subscript[el0, 2, 1][]*Subscript[k, 2, 
                3][] + Subscript[el0, 3, 1][]*Subscript[k, 3, 3][])*
            Subscript[ey1, 1, 1, 1][]) + Subscript[ey0, 1, 1][]*
          (Subscript[ey0, 2, 1][]*(Subscript[k, 1, 2][]*Subscript[el1, 1, 1, 
                1][] - Subscript[k, 2, 2][]*Subscript[el1, 2, 1, 1][] - 
             Subscript[k, 2, 3][]*Subscript[el1, 3, 1, 1][]) + 
           Subscript[ey0, 3, 1][]*(Subscript[k, 1, 3][]*Subscript[el1, 1, 1, 
                1][] - Subscript[k, 2, 3][]*Subscript[el1, 2, 1, 1][] - 
             Subscript[k, 3, 3][]*Subscript[el1, 3, 1, 1][]) + 
           Subscript[el0, 1, 1][]*Subscript[k, 1, 2][]*
            Subscript[ey1, 2, 1, 1][] + Subscript[el0, 2, 1][]*
            Subscript[k, 2, 2][]*Subscript[ey1, 2, 1, 1][] + 
           Subscript[el0, 3, 1][]*Subscript[k, 2, 3][]*
            Subscript[ey1, 2, 1, 1][] + Subscript[el0, 1, 1][]*
            Subscript[k, 1, 3][]*Subscript[ey1, 3, 1, 1][] + 
           Subscript[el0, 2, 1][]*Subscript[k, 2, 3][]*
            Subscript[ey1, 3, 1, 1][] + Subscript[el0, 3, 1][]*
            Subscript[k, 3, 3][]*Subscript[ey1, 3, 1, 1][]))/
        Subscript[ey0, 1, 1][]^2}}], "G1" -> zeroDimensionTensor|>, 
 "elasticityLeadingOrder" -> 
  <|"K0" -> SparseArray[Automatic, {1, 1}, 0, {1, {{0, 1}, {{1}}}, 
      {((Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][] - 
           Subscript[el0, 1, 1][]*Subscript[ey0, 2, 1][])*
          (Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][]*Subscript[k, 2, 2][
             ] + Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][]*
            Subscript[k, 2, 3][] - Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][])) + 
         (Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][] - 
           Subscript[el0, 1, 1][]*Subscript[ey0, 3, 1][])*
          (Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][]*Subscript[k, 2, 3][
             ] + Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][]*
            Subscript[k, 3, 3][] - Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][])))/
        Subscript[ey0, 1, 1][]^2}}], "K1" -> zeroDimensionTensor, 
   "K2" -> zeroDimensionTensor, "K2tilde" -> zeroDimensionTensor|>, 
 "elasticityCoupling" -> <|"A0" -> SparseArray[Automatic, {1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1}}}, 
      {-(((Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][]*
             (Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
              Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][]) + 
            Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][]*
             (Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
              Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]) - 
            Subscript[el0, 1, 1][]*(Subscript[ey0, 2, 1][]^2*Subscript[k, 2, 
                 2][] + 2*Subscript[ey0, 2, 1][]*Subscript[ey0, 3, 1][]*
               Subscript[k, 2, 3][] + Subscript[ey0, 3, 1][]^2*Subscript[k, 
                 3, 3][]))*(Subscript[ey0, 1, 1][]*Subscript[el1, 1, 1, 1][
              ] - Subscript[el0, 1, 1][]*Subscript[ey1, 1, 1, 1][]))/
          Subscript[ey0, 1, 1][]^3) + 
        (Subscript[el0, 2, 1][]*Subscript[k, 2, 2][] + Subscript[el0, 3, 1][]*
           Subscript[k, 2, 3][] - (Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][]))/
           Subscript[ey0, 1, 1][])*(Subscript[el1, 2, 1, 1][] - 
          (Subscript[el0, 1, 1][]*Subscript[ey1, 2, 1, 1][])/
           Subscript[ey0, 1, 1][]) + 
        (Subscript[el0, 2, 1][]*Subscript[k, 2, 3][] + Subscript[el0, 3, 1][]*
           Subscript[k, 3, 3][] - (Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]))/
           Subscript[ey0, 1, 1][])*(Subscript[el1, 3, 1, 1][] - 
          (Subscript[el0, 1, 1][]*Subscript[ey1, 3, 1, 1][])/
           Subscript[ey0, 1, 1][])}}], "A1" -> zeroDimensionTensor|>, 
 "elasticityGradient" -> <|"B0" -> SparseArray[Automatic, {1, 1, 1, 1}, 0, 
     {1, {{0, 1}, {{1, 1, 1}}}, 
      {((Subscript[ey0, 1, 1][]^2*Subscript[el1, 2, 1, 1][] + 
           Subscript[el0, 1, 1][]*Subscript[ey0, 2, 1][]*
            Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[el1, 1, 1, 1][] + 
             Subscript[el0, 1, 1][]*Subscript[ey1, 2, 1, 1][]))*
          (Subscript[k, 2, 2][]*(Subscript[ey0, 1, 1][]^2*Subscript[el1, 2, 
                1, 1][] + Subscript[el0, 1, 1][]*Subscript[ey0, 2, 1][]*
              Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
              (Subscript[ey0, 2, 1][]*Subscript[el1, 1, 1, 1][] + 
               Subscript[el0, 1, 1][]*Subscript[ey1, 2, 1, 1][])) + 
           Subscript[k, 2, 3][]*(Subscript[ey0, 1, 1][]^2*Subscript[el1, 3, 
                1, 1][] + Subscript[el0, 1, 1][]*Subscript[ey0, 3, 1][]*
              Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
              (Subscript[ey0, 3, 1][]*Subscript[el1, 1, 1, 1][] + 
               Subscript[el0, 1, 1][]*Subscript[ey1, 3, 1, 1][]))) + 
         (Subscript[ey0, 1, 1][]^2*Subscript[el1, 3, 1, 1][] + 
           Subscript[el0, 1, 1][]*Subscript[ey0, 3, 1][]*
            Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
            (Subscript[ey0, 3, 1][]*Subscript[el1, 1, 1, 1][] + 
             Subscript[el0, 1, 1][]*Subscript[ey1, 3, 1, 1][]))*
          (Subscript[k, 2, 3][]*(Subscript[ey0, 1, 1][]^2*Subscript[el1, 2, 
                1, 1][] + Subscript[el0, 1, 1][]*Subscript[ey0, 2, 1][]*
              Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
              (Subscript[ey0, 2, 1][]*Subscript[el1, 1, 1, 1][] + 
               Subscript[el0, 1, 1][]*Subscript[ey1, 2, 1, 1][])) + 
           Subscript[k, 3, 3][]*(Subscript[ey0, 1, 1][]^2*Subscript[el1, 3, 
                1, 1][] + Subscript[el0, 1, 1][]*Subscript[ey0, 3, 1][]*
              Subscript[ey1, 1, 1, 1][] - Subscript[ey0, 1, 1][]*
              (Subscript[ey0, 3, 1][]*Subscript[el1, 1, 1, 1][] + 
               Subscript[el0, 1, 1][]*Subscript[ey1, 3, 1, 1][]))))/
        Subscript[ey0, 1, 1][]^4}}], "C0" -> SparseArray[Automatic, {1, 1, 1, 
     1}, 0, {1, {{0, 1}, {{1, 1, 1}}}, 
      {-(((Subscript[el0, 2, 1][]*Subscript[ey0, 1, 1][]*
             (Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
              Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][]) + 
            Subscript[el0, 3, 1][]*Subscript[ey0, 1, 1][]*
             (Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
              Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]) - 
            Subscript[el0, 1, 1][]*(Subscript[ey0, 2, 1][]^2*Subscript[k, 2, 
                 2][] + 2*Subscript[ey0, 2, 1][]*Subscript[ey0, 3, 1][]*
               Subscript[k, 2, 3][] + Subscript[ey0, 3, 1][]^2*Subscript[k, 
                 3, 3][]))*(Subscript[el0, 1, 1][]*Subscript[ey1, 1, 1, 1][]^
              2 + Subscript[ey0, 1, 1][]^2*Subscript[el2, 1, 1, 1, 1][] - 
            Subscript[ey0, 1, 1][]*(Subscript[el1, 1, 1, 1][]*Subscript[ey1, 
                 1, 1, 1][] + Subscript[el0, 1, 1][]*Subscript[ey2, 1, 1, 1, 
                 1][])))/Subscript[ey0, 1, 1][]^4) + 
        (Subscript[el0, 2, 1][]*Subscript[k, 2, 2][] + Subscript[el0, 3, 1][]*
           Subscript[k, 2, 3][] - (Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 2][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 2, 3][]))/
           Subscript[ey0, 1, 1][])*((Subscript[el0, 1, 1][]*
            Subscript[ey1, 1, 1, 1][]*Subscript[ey1, 2, 1, 1][])/
           Subscript[ey0, 1, 1][]^2 + Subscript[el2, 2, 1, 1, 1][] - 
          (Subscript[el1, 1, 1, 1][]*Subscript[ey1, 2, 1, 1][] + 
            Subscript[el0, 1, 1][]*Subscript[ey2, 2, 1, 1, 1][])/
           Subscript[ey0, 1, 1][]) + 
        (Subscript[el0, 2, 1][]*Subscript[k, 2, 3][] + Subscript[el0, 3, 1][]*
           Subscript[k, 3, 3][] - (Subscript[el0, 1, 1][]*
            (Subscript[ey0, 2, 1][]*Subscript[k, 2, 3][] + 
             Subscript[ey0, 3, 1][]*Subscript[k, 3, 3][]))/
           Subscript[ey0, 1, 1][])*((Subscript[el0, 1, 1][]*
            Subscript[ey1, 1, 1, 1][]*Subscript[ey1, 3, 1, 1][])/
           Subscript[ey0, 1, 1][]^2 + Subscript[el2, 3, 1, 1, 1][] - 
          (Subscript[el1, 1, 1, 1][]*Subscript[ey1, 3, 1, 1][] + 
            Subscript[el0, 1, 1][]*Subscript[ey2, 3, 1, 1, 1][])/
           Subscript[ey0, 1, 1][])}}]|>, "boundaryTerms" -> 
  <|"a0" -> zeroDimensionTensor, "k1" -> zeroDimensionTensor|>, 
 "solvabilityConditions" -> <||>|>
