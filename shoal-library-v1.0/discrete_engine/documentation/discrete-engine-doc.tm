<TeXmacs|2.1.4>

<style|<tuple|tmarticle|preview-ref|comment|cite-author-year|libertine-font|number-long-article>>

<\body>
  <\hide-preamble>
    <assign|ny|<macro|n<rsub|y>>>

    <assign|nl|<macro|n<rsub|l>>>

    <assign|nl0|<macro|<wide|n|\<check\>><rsub|l>>>

    <assign|nm|<macro|n<rsub|m>>>

    <assign|nE|<macro|n<rsub|E>>>

    <assign|nc|<macro|n<rsub|<text|c>>>>

    <assign|nh|<macro|n<rsub|h>>>

    <assign|nd|<macro|n<rsub|<text|d>>>>

    <assign|stval|<macro|x|<below|stval|<arg|x>>>>

    <assign|stpt|<macro|x|<below|stpt|<arg|x>>>>

    <assign|math-bf|<macro|body|<with|color|red|<frame|<arg|body>>>>>

    <assign|structure-coef|<macro|alpha|up|down|\<b-e\><rsup|<arg|up>><rsub|<arg|down>><around*|(|<arg|alpha>|)>>>

    <assign|tc2|<syntax|:|\<cdot\>>>

    <assign|tc3|<syntax|<shift|\<therefore\>||.06em>|\<cdot\>>>

    <assign|tc4|<syntax|:<around*|\<nobracket\>||\<nobracket\>>:|\<cdot\>>>

    <assign|tc5|<syntax|\<cdot\><around*|\<nobracket\>||\<nobracket\>>\<vdots\><around*|\<nobracket\>||\<nobracket\>>\<cdot\>|\<cdot\>>>

    <assign|tc6|<syntax|:<around*|\<nobracket\>||\<nobracket\>>:<around*|\<nobracket\>||\<nobracket\>>:|\<cdot\>>>

    <assign|Nb|N<rsub|<text|b>>>

    <assign|kroneck|<macro|a|b|\<b-delta\><rsub|<arg|a>><rsup|<arg|b>>>>

    <assign|citep|<xmacro|keys|(<cite-arg|<arg|keys|0>><map-args|cite-arg-extra|concat|keys|1>)>>

    <assign|citet|<xmacro|keys|<cite-arg|<arg|keys|0>><map-args|cite-arg-extra|concat|keys|1>>>
  </hide-preamble>

  <doc-data|<\doc-title>
    Discrete homogenization engince - shoal<next-line>Technical memo
  </doc-title>|<\doc-running-title>
    Generic, energy-based approach for asymptotic high-order homogenization
  </doc-running-title>|<doc-date|<date>>|<doc-author|<author-data|<author-name|B.
  Audoly>|<\author-affiliation>
    Laboratoire de M�canique des Solides

    CNRS, Institut Polytechnique de Paris

    91120 Palaiseau, France
  </author-affiliation>>>>

  <abstract-data|<\abstract>
    This memo describes the implementation of the discrete engine in the
    <samp|shoal> documentation.
  </abstract>>

  <section|Introduction>

  This documentation is based on the paper<nbsp><cite|Audoly-Lestringant-An-energy-approach-to-asymptotic-2023>.
  We refer the reader to this paper for the fundamentals of the model.

  The following modifications/additions have been done to the method
  described in the original paper:

  <\itemize>
    <item>allowing mechanisms (in project)

    <item>making the final integration by parts of the energy optional (in
    progress)
  </itemize>

  <section|Input to the homogenization procedure><label|sec:input>

  <subsection|Energy formulation of the input model><label|s:canonical-form>

  We proceed to specify the continuous elastic model used as an input to the
  homogenization procedure. The presentation is intentionally abstract:
  illustrations will be provided in Section<nbsp><reference|s:examples>.

  The model is formulated over a continuous domain
  <math|\<Omega\>\<subset\>\<bbb-R\><rsup|d>>, and we denote by
  <math|\<b-X\>\<in\>\<Omega\>> the space variable. A deformed configuration
  of the elastic body is parameterized by three vector fields
  <math|\<b-y\><around*|(|\<b-X\>|)>>, <math|\<b-l\><around*|(|\<b-X\>|)>>
  and <math|\<b-m\><around*|(|\<b-X\>|)>> defined over
  <math|\<Omega\>>:<math|>

  <\itemize>
    <item><em|microscopic degrees of freedom>
    <math|\<b-y\><around*|(|\<b-X\>|)>\<in\>\<bbb-R\><rsup|<ny>>> which we
    seek to eliminate using the homogenization procedure,

    <item><em|macroscopic variables> which are held fixed during
    homogenization, namely:

    <\itemize>
      <item>the <em|macroscopic strain> <math|\<b-l\><around*|(|\<b-X\>|)>\<in\>\<bbb-R\><rsup|<nl>>>

      <item><em|variable material parameters>
      <math|\<b-m\><around*|(|\<b-X\>|)>\<in\>\<bbb-R\><rsup|<nm>>>.
    </itemize>
  </itemize>

  The integers <math|<ny>>, <math|<nl>> and <math|<nm>> are input parameters
  of the homogenization procedure. The goal of the procedure is to slave the
  microscopic degrees of freedom <math|\<b-y\>> to the macroscopic variables
  <math|\<b-l\>> and <math|\<b-m\>>, thereby delivering a homogenized model
  depending on <math|\<b-l\>> and <math|\<b-m\>> only. The difference between
  the macroscopic variables <math|\<b-l\>> and <math|\<b-m\>> is that
  <math|\<b-m\>> captures the slowly variable properties of the elastic
  structure which are prescribed once for all, although <math|\<b-l\>> is
  considered fixed during the homogenization procedure but is actually an
  unknown of the structural problem that the homogenized energy helps
  solving.

  The input model makes use of microscopic strain variables, which are
  collected into a vector <math|\<b-E\>\<in\>\<bbb-R\><rsup|<nE>>>. The
  geometric definition of the strain <math|\<b-E\>> is taken of the form

  <\equation>
    \<b-E\>=\<b-E\><around*|(|\<b-m\><around*|(|\<b-X\>|)>;\<b-l\><around*|(|\<b-X\>|)>,\<nabla\>\<b-l\><around*|(|\<b-X\>|)>,\<nabla\><rsup|2>\<b-l\><around*|(|\<b-X\>|)>,\<ldots\>;\<b-y\><around*|(|\<b-X\>|)>,\<nabla\>\<b-y\><around*|(|\<b-X\>|)>,\<nabla\><rsup|2>\<b-y\><around*|(|\<b-X\>|)>,\<ldots\>|)><label|eq:EofX>
  </equation>

  where the dependence on the macroscopic strain <math|\<b-l\>> and of the
  microscopic degrees of freedom <math|\<b-y\>> and their gradients is linear
  in the context of linear elasticity,

  <\equation>
    <tabular|<tformat|<cwith|2|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-E\><around*|(|\<b-m\>;\<b-l\>,\<b-l\><rprime|'>,\<b-l\><rprime|''>,\<ldots\>;\<b-y\>,\<b-y\><rprime|'>,\<b-y\><rprime|''>,\<ldots\>|)>=\<b-E\><rsub|l><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>+\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\>|)><tc2>\<b-l\><rprime|'>+\<b-E\><rsub|l><rprime|''><around*|(|\<b-m\>|)><tc3>\<b-l\><rprime|''>+\<ldots\><space|2em>>>|<row|<cell|<around*|\<nobracket\>||\<nobracket\>>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<b-y\><rprime|'>+\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)><tc3>\<b-y\><rprime|''>+\<cdots\>>>>>><label|eq:strain-canonical-form>
  </equation>

  Here, <math|\<b-l\>\<in\>\<bbb-R\><rsup|<nl>>>,
  <math|\<b-l\><rprime|'>=\<nabla\>\<b-l\>\<in\>\<bbb-T\><rsup|<around*|(|<nl>,d|)>>>,
  <math|\<b-l\><rprime|''>=\<nabla\><rsup|2>\<b-l\>\<in\>\<bbb-T\><rsup|<around*|(|<nl>,d,d|)>>>,
  <math|\<b-y\>\<in\>\<bbb-R\><rsup|<ny>>>,
  <math|\<b-y\><rprime|'>=\<nabla\>\<b-y\>\<in\>\<bbb-T\><rsup|<around*|(|<ny>,d|)>>>,
  <math|\<b-y\><rprime|''>=\<nabla\><rsup|2>\<b-y\>\<in\>\<bbb-T\><rsup|<around*|(|<ny>,d,d|)>>>
  are dummy variables representing the local values of <math|\<b-l\>>,
  <math|\<b-y\>> and their successive gradients. The homogenization procedure
  is implemented in a symbolic calculation language, and the tensors
  <math|\<b-E\><rsub|l><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<nl>|)>>>,
  <math|\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>,
  <math|\<b-E\><rsub|l><rprime|''><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>,
  <text-dots>, <math|\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<ny>|)>>>,
  <math|\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<ny>,d|)>>>
  and <math|\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<ny>,d,d|)>>>
  are provided as input, see Table<nbsp><reference|tab:input>, in the form of
  symbolic, tensor-valued functions of <math|\<b-m\>>. Our tensor notations
  such as <math|\<bbb-T\><rsup|<around*|(|<nl>,d,d|)>>> for tensor spaces are
  given in Appendix<nbsp><reference|a:tensors>.

  <\remark>
    We will present the homogenization method without providing definitions
    of the macroscopic strain <math|\<b-l\>>, variable material parameters
    <math|\<b-m\>> and microscopic degrees of freedom <math|\<b-y\>>. These
    quantities can be anything as long as they match the postulated forms of
    the strain, constraints and energy in
    Equations<nbsp>(<reference|eq:strain-canonical-form>\U<reference|tab:leading-order-summary>).
    This abstract presentation makes the method quite versatile, and opens up
    the way for extensions that we will cover in future work. Specific
    choices of <math|\<b-l\>>, <math|\<b-m\>> and <math|\<b-y\>> are proposed
    in the illustration examples (�<reference|eq:phi-canonical>).
  </remark>

  The model used on input may make use of constraints and we denote by
  <math|<nc>\<geqslant\>0> the number of applicable (scalar) kinematic
  constraints. By convention, the left-hand sides of these kinematic
  constraints are incorporated into the microscopic strain vector
  <math|\<b-E\>\<in\>\<bbb-R\><rsup|<nE>>>, and are extracted from
  <math|\<b-E\>> using an appropriate matrix
  <math|\<b-cal-Q\>\<in\>\<bbb-T\><rsup|<around*|(|<nc>,<nE>|)>>>. In view of
  this, the kinematic constraints are written in the
  form<folded-comment|+2FxRM9JM5NgrCAZ|+E9w5aXx1KIWKs0X|comment|System
  Administrator|1678122356||Do we assume that <math|\<b-cal-Q\>> is a
  projector, i.e., <math|\<b-cal-Q\><rsup|2>=\<b-cal-Q\>>?>

  <\equation>
    \<b-cal-Q\>\<cdot\>\<b-E\><around*|(|\<b-m\><around*|(|\<b-X\>|)>;\<b-l\><around*|(|\<b-X\>|)>,\<nabla\>\<b-l\><around*|(|\<b-X\>|)>,\<ldots\>;\<b-y\><around*|(|\<b-X\>|)>,\<nabla\>\<b-y\><around*|(|\<b-X\>|)>,\<ldots\>|)>=\<b-0\><rsub|<nc>><separating-space|1em>\<forall\>\<b-X\>.<label|eq:QEisZero>
  </equation>

  Section<nbsp><reference|s:examples> provides a specific illustration on how
  <math|\<b-E\>> and <math|\<b-cal-Q\>> can be set up to conform to
  Equation<nbsp>(<reference|eq:QEisZero>). The constant tensor
  <math|\<b-cal-Q\>> is provided as an input of the homogenization procedure,
  see Table<nbsp><reference|tab:input>.

  In the input model, the strain energy is assumed to be of the form

  <\equation>
    \<Phi\><around*|[|\<b-m\>,\<b-l\>;\<b-y\>|]>=<big|int><rsub|\<Omega\>>W<around*|(|\<b-m\><around*|(|\<b-X\>|)>,\<b-E\><around*|(|\<b-m\><around*|(|\<b-X\>|)>;\<b-l\><around*|(|\<b-X\>|)>,\<nabla\>\<b-l\><around*|(|\<b-X\>|)>,\<ldots\>;\<b-y\><around*|(|\<b-X\>|)>,\<nabla\>\<b-y\><around*|(|\<b-X\>|)>,\<ldots\>|)>|)>*\<mathd\>\<b-X\>,<label|eq:phi-canonical>
  </equation>

  where the strain energy density in the bulk term is given in the context of
  linear elasticity by

  <\equation>
    W<around*|(|\<b-m\>,\<b-E\>|)>=<frac|1|2> \<b-E\>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\>.<label|eq:abstract-energy-density>
  </equation>

  The elasticity tensor <math|\<b-cal-K\><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nE>,<nE>|)>>>
  is provided as an input to the homogenization method in the form of a
  tensor-valued, symbolic function of <math|\<b-m\>>, see
  Table<nbsp><reference|tab:input>.<folded-comment|+2FxRM9JM5NgrCAf|+22pne0gb25GPOyAX|comment|System
  Administrator|1699346617||Note that the expression of
  <math|W<around*|(|\<b-m\>,\<b-E\>|)>> above should be used only with
  vectors <math|\<b-E\>> satisfying the constraint
  <math|\<b-cal-Q\>\<cdot\>\<b-E\>=\<b-0\>>: when this is not the case, the
  energy should be considered infinite.>

  The square brackets around the arguments of
  <math|\<Phi\><around*|[|\<b-m\>,\<b-l\>;\<b-y\>|]>> in the left-hand side
  of<nbsp>(<reference|eq:phi-canonical>) denote a <em|functional> dependence:
  the strain energy <math|\<Phi\>> depends on the <em|functions>
  <math|\<b-m\>>, <math|\<b-l\>> and <math|\<b-y\>> over the entire domain
  <math|\<Omega\>>.

  The list of parameters passed as an input to the homogenization procedure
  is recapitulated in Table<nbsp><reference|tab:input>.

  <\big-table|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|1|2|2|cell-halign|c>|<cwith|15|15|1|1|cell-bborder|0ln>|<cwith|2|-1|1|1|cell-lborder|0ln>|<cwith|2|-1|1|1|cell-rborder|1ln>|<cwith|2|-1|2|2|cell-lborder|1ln>|<cwith|1|1|2|-1|cell-tborder|0ln>|<cwith|1|1|2|-1|cell-bborder|1ln>|<cwith|2|2|2|-1|cell-tborder|1ln>|<cwith|1|1|4|4|cell-rborder|0ln>|<cwith|8|8|1|-1|cell-tborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|0ln>|<cwith|9|9|1|-1|cell-tborder|0ln>|<cwith|8|8|1|1|cell-lborder|0ln>|<cwith|8|8|4|4|cell-rborder|0ln>|<cwith|14|14|1|-1|cell-tborder|1ln>|<cwith|13|13|1|-1|cell-bborder|1ln>|<cwith|14|14|1|-1|cell-bborder|0ln>|<cwith|15|15|1|-1|cell-tborder|0ln>|<cwith|14|14|1|1|cell-lborder|0ln>|<cwith|14|14|4|4|cell-rborder|0ln>|<cwith|1|1|1|1|cell-tborder|0ln>|<cwith|1|1|1|1|cell-bborder|1ln>|<cwith|2|2|1|1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|1|1|cell-rborder|1ln>|<cwith|1|1|2|2|cell-lborder|1ln>|<table|<row|<cell|>|<cell|nature>|<cell|tensor
  space>|<cell|symmetries>>|<row|<cell|<math|d\<geqslant\>1>>|<cell|space
  dimension>|<cell|>|<cell|>>|<row|<cell|<math|<nm>\<geqslant\>0>>|<cell|number
  of material parameters>|<cell|>|<cell|>>|<row|<cell|<math|<nl>\<geqslant\>0>>|<cell|number
  of macroscopic degrees of freedom>|<cell|>|<cell|>>|<row|<cell|<math|<ny>\<geqslant\>0>>|<cell|number
  of microscopic degrees of freedom>|<cell|>|<cell|>>|<row|<cell|<math|<nE>\<geqslant\><nc>>>|<cell|number
  of strain variables>|<cell|>|<cell|>>|<row|<cell|<math|<nc>>>|<cell|number
  of kinematic constraints>|<cell|>|<cell|>>|<row|<cell|<math|\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<b-l\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>|)>>>>|<cell|>>|<row|<cell|<math|\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<nabla\>\<b-l\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>>|<cell|>>|<row|<cell|<math|\<b-E\><rsub|l><rprime|''><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<nabla\><rsup|2>\<b-l\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>>|<row|<cell|<math|\<b-E\><rsub|y><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<b-y\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<ny>|)>>>>|<cell|>>|<row|<cell|<math|\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<nabla\>\<b-y\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<ny>,d|)>>>>|<cell|>>|<row|<cell|<math|\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)>>>|<cell|dependence
  of strain on <math|\<nabla\><rsup|2>\<b-y\>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<ny>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>>|<row|<cell|<math|\<b-cal-Q\>>>|<cell|constraint
  extraction, see<nbsp>(<reference|eq:QEisZero>)>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nc>,<nE>|)>>>>|<cell|>>|<row|<cell|<math|\<b-cal-K\><around*|(|\<b-m\>|)>>>|<cell|stiffness
  matrix, see<nbsp>(<reference|eq:abstract-energy-density>)>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nE>|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>>>>>>>
    List of parameters passed as an input to the homogenization procedure.
    The notation used in the last two columns is defined in
    Appendix<nbsp><reference|a:tensors>.<label|tab:input>
  </big-table>

  <subsection|Assumption of slow variations><label|ssec:slow-variations>

  One of the key assumptions of homogenization is that there is a separation
  of scales between a microscopic length <math|\<ell\>> (typically the
  spatial period of the underlying discrete lattice or periodic continuum)
  and the size <math|L> of the domain, <math|\<ell\>\<ll\>L>. The goal of
  homogenization is to deliver an effective model applicable at the
  macroscopic scale <math|L>, by hiding the \<#2018\>details\<#2019\> taking
  place at the microscopic scale <math|\<ell\>>.

  Mathematically, this separation of scale is captured by the small parameter

  <\equation>
    \<eta\>=<frac|\<ell\>|L>\<ll\>1.<label|eta-def>
  </equation>

  The various fields <math|\<b-f\><around*|(|\<b-X\>|)>>, such as
  <math|\<b-f\>=\<b-m\>>, <math|\<b-f\>=\<b-l\>> or <math|\<b-f\>=\<b-y\>>,
  are assumed to depend on the variable <math|\<b-X\>> evolving on the slow
  scale <math|L=\<ell\>/\<eta\>>, implying that their successive gradients
  scale as

  <\equation>
    \<nabla\><rsup|k>\<b-f\><around*|(|\<b-X\>|)>=\<cal-O\><around*|(|\<eta\><rsup|k>|)>.<label|eq:nabla-order-of-magnitude>
  </equation>

  In the following, the gradient <math|\<nabla\>=<frac|\<partial\>|\<partial\>\<b-X\>>>
  will therefore be treated <em|implicitly> as a small quantity of order
  <math|\<eta\>>. This implicit notation has the benefit of avoiding a large
  number of predictable occurrences of the parameter <math|\<eta\>>, as
  discussed in Remark<nbsp><reference|rk:scaling-nabla> below.

  The scaling assumption<nbsp>(<reference|eq:nabla-order-of-magnitude>) is
  not applicable in the layers that are present near the boundaries or near
  the point of application of point-like force. The homogenization
  domain<nbsp><math|\<Omega\>> therefore needs to be slightly smaller than
  the actual physical domain of the elastic body (see also Equation<nbsp>[1]
  in the work of<nbsp><cite|abdoul2018strain> for an accurate description of
  how <math|\<Omega\>> can be shrunk). Alternatively, boundary layers can be
  solved rigorously and represented in the homogenized model by means of
  effective boundary terms, see for example<nbsp><cite|david2012homogenized>,
  but this is beyond the scope of the present work.

  <\remark>
    <label|rk:scaling-nabla>The scaling assumption<nbsp>(<reference|eq:nabla-order-of-magnitude>)
    can be motivated as follows. By convention, we consider the microscopic
    length <math|\<ell\>> to be <math|\<ell\>=\<cal-O\><around*|(|1|)>> and
    the macroscopic length to be <math|L=\<cal-O\><around*|(|\<eta\><rsup|-1>|)>>.
    In our notation, any macroscopic field <math|\<b-f\>> such as
    <math|\<b-m\>>, <math|\<b-l\>> or <math|\<b-y\>> is implicitly a function
    of the slow variable <math|<wide|\<b-X\>|\<wide-bar\>>=\<eta\>*\<b-X\>>,
    <em|i.e.>, what we write as <math|\<b-f\><around*|(|\<b-X\>|)>> should be
    spelled out as <math|\<b-f\><around*|(|\<b-X\>|)>=<wide|\<b-f\>|\<wide-bar\>><around*|(|\<eta\>*\<b-X\>|)>>,
    where <math|<wide|\<b-f\>|\<wide-bar\>>> is a dimensionless function,
    independent of <math|\<eta\>>. The gradients can then be obtained as
    <math|\<nabla\><rsup|k>\<b-f\><around*|(|\<b-X\>|)>=<frac|\<partial\><rsup|k>\<b-f\>|\<partial\>\<b-X\><rsup|k>><around*|(|\<b-X\>|)>=\<eta\><rsup|k>*<frac|\<partial\><rsup|k><wide|\<b-f\>|\<wide-bar\>>|\<partial\><wide|\<b-X\>|\<wide-bar\>><rsup|k>><around*|(|\<eta\>*\<b-X\>|)>=\<eta\><rsup|k>*<wide|\<nabla\>|\<wide-bar\>><rsup|k><wide|\<b-f\>|\<wide-bar\>><around*|(|\<eta\>*\<b-X\>|)>>,
    where <math|<wide|\<nabla\>|\<wide-bar\>><rsup|k><wide|\<b-f\>|\<wide-bar\>>=\<cal-O\><around*|(|1|)>>
    denotes the gradient with respect to the slow variable: this implies the
    scaling assumption <math|\<nabla\><rsup|k>\<b-f\><around*|(|\<b-X\>|)>=\<cal-O\><around*|(|\<eta\><rsup|k>|)>>
    in<nbsp>(<reference|eq:nabla-order-of-magnitude>). The formal
    rule<nbsp>(<reference|eq:nabla-order-of-magnitude>) dispenses with a
    notation for the slow variable.
  </remark>

  <\remark>
    The dependence on <math|\<b-m\><around*|(|\<b-X\>|)>> of the strain
    <math|\<b-E\>> in<nbsp>(<reference|eq:EofX>\U<reference|eq:strain-canonical-form>)
    and of the energy density <math|W> in<nbsp>(<reference|eq:phi-canonical>\U<reference|tab:leading-order-summary>)
    allows one to handle the case of structures whose elastic properties vary
    over the large scale <math|L=\<ell\>/\<eta\>>, as conveyed by the
    variations in thickness of the microstructure. The definition of
    <math|\<b-m\><around*|(|\<b-X\>|)>> is entirely up to the user. For a 2D
    elastic truss possessing rotational symmetry, for instance, one could
    define <math|\<b-m\><around*|(|\<b-X\>|)>=<around*|(|<sqrt|X<rsub|1><rsup|2>+X<rsub|2><rsup|2>>|)>>
    and <math|<nm>=1> to capture the dependence of the local truss properties
    on the distance to the center of symmetry. In the case of variable
    properties without any particular symmetry, one should set
    <math|\<b-m\><around*|(|\<b-X\>|)>=\<b-X\>> and <math|<nm>=d>. When
    specifying the input model, one should ensure that any dependence of the
    properties of the elastic medium on the slow variable <math|\<b-X\>>
    takes place through the quantity <math|\<b-m\><around*|(|\<b-X\>|)>>, as
    illustrated in Section<nbsp><reference|s:examples>. For structures having
    uniform properties over the large scale, one can ignore any dependence on
    <math|\<b-m\>> and set <math|<nm>=0>, see
    Appendix<nbsp><reference|app:homogeneous properties>.
  </remark>

  <section|Summary of the main results >

  <subsection|Homogenization as a partial energy relaxation>

  In this paper, we identify an equivalent continuum by making stationary the
  functional <math|\<Phi\>> in<nbsp>(<reference|eq:phi-canonical>) over the
  microscopic degrees of freedom <math|\<b-y\><around*|(|\<b-X\>|)>> for a
  fixed distribution of the macroscopic variables
  <math|\<b-m\><around*|(|\<b-X\>|)>> and
  <math|\<b-l\><around*|(|\<b-X\>|)>>. The stationary point is denoted as
  <math|\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>, and will be
  assumed to be unique\Vas indicated by the square brackets, the stationary
  point <math|\<b-y\><rprime|\<star\>>> is a <em|functional> of
  <math|\<b-m\>> and <math|\<b-l\>>; it is also a function of <math|\<b-X\>>,
  whose values are denoted as <math|\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]><around*|(|\<b-X\>|)>>.

  In view of<nbsp>(<reference|eq:strain-canonical-form>\U<reference|tab:tensors-delivered>),
  the kinematic constraint can be written as
  <math|\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\><around*|(|\<b-X\>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-y\><around*|(|\<b-X\>|)>+\<ldots\>+\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-l\><around*|(|\<b-X\>|)>+\<ldots\>|)>=\<b-0\>>
  <math|\<forall\>\<b-X\>>. It is treated using Lagrange multipliers
  <math|\<b-g\><around*|(|\<b-X\>|)>\<in\>\<bbb-R\><rsup|<nc>>>. The
  variational problem takes the following form: for given <math|\<b-m\>> and
  <math|\<b-l\>>, we seek the solution <math|<around*|(|\<b-y\>,\<b-g\>|)>=<around*|(|\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>,\<b-g\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>|)>>
  of

  <\equation>
    <around*|{|<tabular|<tformat|<table|<row|<cell|\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\><around*|(|\<b-X\>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-y\><around*|(|\<b-X\>|)>+\<ldots\>+\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-l\><around*|(|\<b-X\>|)>+\<ldots\><around*|\<nobracket\>||\<nobracket\>>|)>=\<b-0\><space|1em>\<forall\>\<b-X\>>>|<row|<cell|\<mathD\><rsub|\<b-y\>>\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>;\<delta\>\<b-y\>|]>+<big|int><rsub|\<Omega\>>\<b-g\><around*|(|\<b-X\>|)>\<cdot\>\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<delta\>\<b-y\><around*|(|\<b-X\>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<delta\>\<b-y\><around*|(|\<b-X\>|)>+\<ldots\><around*|\<nobracket\>||\<nobracket\>>|)>*\<mathd\>\<b-X\>=\<b-0\>,<separating-space|1em>\<forall\>\<delta\>\<b-y\>.>>>>>|\<nobracket\>><label|eq:y-star-withLagrange>
  </equation>

  where <math|\<mathD\><rsub|\<b-y\>>\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>;\<delta\>\<b-y\>|]>=lim<rsub|\<tau\>\<rightarrow\>0>
  <around*|(|\<Phi\><around*|[|\<b-h\>,\<b-y\>+\<tau\>*\<delta\>\<b-y\>|]>-\<Phi\><rsub|<text|c>><around*|[|\<b-h\>,\<b-y\>|]>|)>/\<tau\>>
  denotes the directional derivative and <math|\<delta\>\<b-y\><around*|(|\<b-X\>|)>>
  is an arbitrary perturbation. Equation<nbsp>(<reference|eq:y-star-withLagrange>)<rsub|1>
  warrants that the stationary point <math|\<b-y\>=\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>
  satisfies the kinematic constraint, while
  Equation<nbsp>(<reference|eq:y-star-withLagrange>)<rsub|2> warrants that it
  is indeed a stationary point among the <math|\<b-y\>>'s satisfying the
  kinematic constraints\Vthe integral term takes care of these constraints by
  multiplying the Lagrange multipliers by the incremental form of the
  constraint, as usual in the calculus of variations.

  Having slaved the microscopic degrees of freedom
  <math|\<b-y\>=\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>> to the
  macroscopic variables, we can define a homogenized energy functional
  <math|\<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>> by inserting
  the stationary point <math|\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>
  into the original <math|\<Phi\>>,

  <\equation>
    \<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>|]>.<label|eq:phi-star-new>
  </equation>

  The main goal of this paper is to derive an explicit expression of the
  homogenized functional <math|\<Phi\><rprime|\<star\>>>.

  The stationary point problem in<nbsp>(<reference|eq:y-star-withLagrange>)
  can be written formally as

  <\equation>
    \<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<stpt|\<b-y\><text|
    such that >\<b-cal-Q\>\<cdot\>\<b-E\>=\<b-0\><separating-space|0.2em>\<forall\>\<b-X\>>\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>.<label|eq:ystar-as-stpt>
  </equation>

  Equations<nbsp>(<reference|eq:phi-star-new>\U<reference|eq:ystar-as-stpt>)
  are at the heart of our variational approach to homogenization. They can be
  motivated as follows, by considering the broader structural problem of
  interest: a discrete truss, for instance, is governed by a total potential
  energy <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>>,
  where <math|\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>> is the strain
  energy of the truss and <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>> is the
  potential energy of the applied loading (under standard scaling
  assumptions, the latter does not depend on the microscopic degrees of
  freedom <math|\<b-y\>>). Recalling that the variable elastic properties
  <math|\<b-m\><around*|(|\<b-X\>|)>> are fixed by design, the full elastic
  problem is solved by making the total potential energy
  <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>>
  stationary with respect to both the macroscopic unknowns
  <math|\<b-l\><around*|(|\<b-X\>|)>> and the microscopic ones
  <math|\<b-y\><around*|(|\<b-X\>|)>>, subjected to the kinematic conditions
  <math|\<b-cal-Q\>\<cdot\>\<b-E\>=\<b-0\>>, <math|\<forall\>\<b-X\>>.
  Homogenization consists simply in enforcing the stationarity conditions
  sequentially, with respect to the microscopic degrees of freedom
  <math|\<b-y\>> first and to the macroscopic strain <math|\<b-l\>> next.
  Indeed, the stationarity condition of <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>>
  with respect to <math|\<b-y\>> is nothing but that considered
  in<nbsp>(<reference|eq:ystar-as-stpt>), given that
  <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>> does not depend on
  <math|\<b-y\>>. Next, it can be checked that the stationarity condition
  with respect to <math|\<b-l\>> of <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>>
  is equivalent to the stationarity condition of the modified functional
  <math|\<Psi\><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>
  based on the homogenized strain energy <math|\<Phi\><rprime|\<star\>>>
  introduced in<nbsp>(<reference|eq:phi-star-new>). This argument not only
  provides a justification to Equations<nbsp>(<reference|eq:phi-star-new>\U<reference|eq:ystar-as-stpt>),
  it also explains why the homogenized energy
  <math|\<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>> can be used as
  a substitute for the original energy <math|\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>>
  in the analysis of the structural problem.

  The homogenization works under the assumption that the energy is
  positive-definite in the subspace of admissible microscopic degrees of
  freedom, <em|i.e.>,

  <\equation>
    <around*|(|\<forall\><around*|(|\<b-m\>,\<b-y\>\<neq\>\<b-0\>|)><text|
    such that >\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>=\<b-0\>|)><separating-space|2em><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>|)>\<gtr\>0.<label|e:no-micro-mechanism>
  </equation>

  As we will see, this is a necessary condition for the variational
  problem<nbsp>(<reference|eq:y-star-withLagrange>) to have a unique solution
  at leading order. It is also a sufficient condition for the homogenization
  procedure to produce a result up to second-order.

  <subsection|Homogenization results in compact form>

  The variational problem<nbsp>(<reference|eq:y-star-withLagrange>) is
  impossible to solve in closed form in general but thanks to the assumption
  of scale separation (�<reference|ssec:slow-variations>), we can derive the
  following approximation of <math|\<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>,

  <\equation>
    \<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<big|int><rsub|\<Omega\>><rsup|><around*|(|\<b-K\><around*|[|\<b-m\>|]><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>+\<b-A\><around*|[|\<b-m\>|]><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>+\<b-B\><around*|[|\<b-m\>|]><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>+\<b-C\><around*|[|\<b-m\>|]><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>|)>*\<mathd\><rsup|2>\<b-X\>+\<cal-O\><around*|(|L<rsup|d>*\<eta\><rsup|3>|)><label|eq:homogenized-model-announce>
  </equation>

  In the boundary terms <math|<big|oint><rsub|\<partial\>\<Omega\>>\<ldots\>*\<mathd\>a>
  in the second line, <math|\<partial\>\<Omega\>> denotes the boundary of the
  domain, <math|\<b-n\>> is the unit outward normal, and <math|\<mathd\>a>
  the area (if <math|d=3>) or the length (if <math|d=2>) of a boundary
  element. The typographical variant of the integral sign <math|<big|oint>>
  will be used throughout for boundary integrals.

  The dimension and symmetries of the homogenized tensors
  <math|<math|\<b-K\>>>, <math|<math|\<b-A\>>>, <math|\<b-B\>>,
  <math|\<b-k\>> and <math|\<b-a\>> are specified in
  Table<nbsp><reference|tab:tensor-symmetries>. Our main result is to derive
  their expansion in the successive gradients
  <math|\<nabla\><rsup|k>\<b-m\>=\<cal-O\><around*|(|\<eta\><rsup|k>|)>>,

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|4|4|1|1|cell-halign|r>|<cwith|1|-1|5|5|cell-halign|c>|<cwith|1|-1|6|6|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|2|7|7|cell-halign|c>|<table|<row|<cell|\<b-K\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-K\><rsub|0><around*|(|\<b-m\>|)>>|<cell|+>|<cell|\<b-K\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>>|<cell|+>|<cell|<around*|<left|(|2>|\<b-K\><rsub|2><around*|(|\<b-m\>|)><tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>+<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>|<right|)|2>>>|<cell|+>|<cell|\<ldots\>>>|<row|<cell|\<b-A\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-A\><rsub|0><around*|(|\<b-m\>|)>>|<cell|+>|<cell|\<b-A\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>>|<cell|+>|<cell|\<ldots\>>|<cell|>|<cell|>>|<row|<cell|\<b-B\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-B\><rsub|0><around*|(|\<b-m\>|)>>|<cell|+>|<cell|\<ldots\>>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|\<b-C\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-C\><rsub|0><around*|(|\<b-m\>|)>>|<cell|+>|<cell|\<ldots\>>|<cell|>|<cell|>|<cell|>|<cell|>>>>><label|eq:KABka-expansion>
  </equation>

  The tensors <math|\<b-K\><rsub|i><around*|(|\<b-m\>|)>>,
  <math|\<b-A\><rsub|i><around*|(|\<b-m\>|)>>,
  <math|\<b-B\><rsub|0><around*|(|\<b-m\>|)>> and
  <math|\<b-C\><rsub|0><around*|(|\<b-m\>|)>> appearing in the right-hand
  sides of<nbsp>(<reference|eq:KABka-expansion>) are obtained in explicit
  form in terms of the local material parameters <math|\<b-m\>> in
  Appendix<nbsp><reference|app:homogeneous-solution>,
  see<nbsp>(<reference|eq:K0-def>), and in
  Appendix<nbsp><reference|app:gradient-effect>, see
  Sections<nbsp><reference|ssec:extract-A0-K1>
  and<nbsp><reference|ssec:work-out-Phi2>. Their properties are listed in
  Table<nbsp><reference|tab:tensors-delivered>.

  This completes the specification of the homogenized
  model<nbsp>(<reference|eq:homogenized-model-announce>) up to order
  <math|\<eta\><rsup|2>> included.

  <\big-table|<math|<tabular|<tformat|<cwith|2|6|1|1|cell-halign|r>|<cwith|5|5|1|1|cell-halign|r>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|1|1|-1|cell-tborder|0ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|3|3|cell-rborder|0ln>|<cwith|6|6|1|-1|cell-tborder|1ln>|<cwith|6|6|1|-1|cell-bborder|0ln>|<cwith|7|7|1|-1|cell-tborder|0ln>|<cwith|6|6|3|3|cell-rborder|0ln>|<cwith|1|1|1|1|cell-tborder|0ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|1|1|cell-rborder|1ln>|<cwith|1|-1|2|2|cell-lborder|1ln>|<cwith|8|8|1|-1|cell-tborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|0ln>|<cwith|9|9|1|-1|cell-tborder|0ln>|<cwith|8|8|1|1|cell-lborder|0ln>|<cwith|8|8|3|3|cell-rborder|0ln>|<table|<row|<cell|>|<cell|<text|tensor
  space>>|<cell|<text|symmetry>>>|<row|<cell|\<b-K\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,<nl>|)>>>|<cell|S<rsub|1\<nocomma\>2>>>|<row|<cell|\<b-A\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d|)>>>|<cell|<text|\U>>>|<row|<cell|\<b-B\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,d,<nl>,d|)>>>|<cell|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>>>|<row|<cell|\<b-C\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,d|)>>>|<cell|S<rsub|3\<nocomma\>4>>>|<row|<cell|\<b-Y\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<ny>,<nl>|)>>>|<cell|<text|\U>>>|<row|<cell|\<b-Y\><rprime|'><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<ny>,<nl>,d|)>>>|<cell|<text|\U>>>|<row|<cell|\<b-F\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nE>,<nl>|)>>>|<cell|<text|\U>>>|<row|<cell|\<b-F\><rprime|'><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>|<cell|<text|\U>>>|<row|<cell|\<b-F\><rprime|''><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>|<cell|S<rsub|3\<nocomma\>4>>>>>>>>
    Dimensions and symmetries of the tensors appearing in
    Equation<nbsp>(<reference|eq:homogenized-model-announce>),
    (<reference|eq:y-in-gradients-of-l>) and<nbsp>(<reference|eq:E-optimum-summary>).<label|tab:tensor-symmetries>
  </big-table>

  <\big-table|<tabular|<tformat|<cwith|2|-1|1|1|cell-halign|c>|<cwith|10|10|1|-1|cell-tborder|1ln>|<cwith|10|10|1|-1|cell-bborder|0ln>|<cwith|11|11|1|-1|cell-tborder|0ln>|<cwith|10|10|1|1|cell-lborder|0ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|0ln>|<cwith|3|3|1|-1|cell-tborder|0ln>|<cwith|2|2|1|1|cell-lborder|0ln>|<cwith|1|1|2|2|cell-tborder|0ln>|<cwith|1|-1|2|2|cell-lborder|1ln>|<cwith|1|-1|1|1|cell-rborder|1ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|3|3|cell-lborder|0ln>|<cwith|2|-1|2|-1|cell-halign|c>|<cwith|1|1|2|-1|cell-halign|c>|<cwith|2|-1|4|4|cell-halign|l>|<cwith|13|13|1|-1|cell-tborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<cwith|13|13|1|1|cell-lborder|0ln>|<cwith|14|14|1|-1|cell-tborder|1ln>|<cwith|13|13|1|-1|cell-bborder|1ln>|<cwith|14|14|1|-1|cell-bborder|0ln>|<cwith|15|15|1|-1|cell-tborder|0ln>|<cwith|14|14|1|1|cell-lborder|0ln>|<cwith|14|14|4|4|cell-rborder|0ln>|<cwith|14|14|2|2|cell-halign|c>|<cwith|14|14|3|3|cell-halign|c>|<cwith|14|14|2|2|cell-lborder|1ln>|<cwith|14|14|2|3|cell-tborder|1ln>|<cwith|14|14|2|3|cell-bborder|0ln>|<cwith|14|14|3|3|cell-rborder|0ln>|<cwith|18|18|2|2|cell-halign|c>|<cwith|18|18|3|3|cell-halign|c>|<cwith|18|18|2|2|cell-lborder|1ln>|<cwith|18|18|2|3|cell-tborder|0ln>|<cwith|20|20|2|2|cell-halign|c>|<cwith|20|20|3|3|cell-halign|c>|<cwith|20|20|2|2|cell-lborder|1ln>|<table|<row|<cell|>|<cell|tensor
  space>|<cell|symmetry>|<cell|usage>>|<row|<cell|<math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>>|<cell|<math|\<Phi\><rsub|<around*|[|0|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-K\><rsub|0><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>*\<mathd\>\<b-X\>>>>|<row|<cell|<math|\<b-K\><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,<nm>,d|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>>|<cell|<math|\<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-K\><rsub|1><tc2>\<nabla\>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-K\><rsub|2><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,<nm>,d,<nm>,d|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>,
  <math|S<rsub|<around*|{|3\<nocomma\>4|}>\<nocomma\><around*|{|5\<nocomma\>6|}>>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-K\><rsub|2><tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,<nm>,d,d|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>,
  <math|S<rsub|4\<nocomma\>5>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|<wide|\<b-K\>|~><rsub|2><tc3>\<nabla\><rsup|2>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-A\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-A\><rsub|0><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-A\><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,<nm>,d|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-A\><rsub|1><tc2>\<nabla\>\<b-m\>|)><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-B\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,<nl>,d|)>>>>|<cell|<math|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-B\><rsub|0><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-C\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-C\><rsub|0><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nl>|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>>>|<row|<cell|<math|\<b-Y\><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nm>,d,<nl>|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>=<around*|(|\<b-Y\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)>\<cdot\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>=\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-G\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nc>,<nl>|)>>>>|<cell|\U>|<cell|<math|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>>>|<row|<cell|<math|\<b-F\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>|)>>>>|<cell|<text|\U>>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<b-F\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-F\><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,<nm>,d|)>>>>|<cell|>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<around*|(|\<b-F\><rsub|1><around*|(|\<b-m\>|)><value|tc2>\<nabla\>\<b-m\>|)>\<cdot\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-F\><rsub|2><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,<nm>,d,<nm>,d|)>>>>|<cell|<math|S<rsub|<around*|{|3\<nocomma\>4|}>\<nocomma\><around*|{|5\<nocomma\>6|}>>>>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<around*|(|\<b-F\><rsub|2><around*|(|\<b-m\>|)><value|tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>|)>\<cdot\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,<nm>,d,d|)>>>>|<cell|<math|S<rsub|4\<nocomma\>5>>>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<around*|(|<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>|)>\<cdot\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-F\><rprime|'><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>>|<cell|<text|\U>>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<b-F\><rprime|'><rsub|0><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-F\><rprime|'><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,<nm>,d|)>>>>|<cell|>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=<around*|(|\<b-F\><rprime|'><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc2>\<nabla\>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-F\><rprime|''><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<b-F\><rprime|''><rsub|0><around*|(|\<b-m\>|)><value|tc3>\<nabla\><rsup|2>\<b-l\>+\<cdots\>>>>>>>>
    Tensors delivered by the homogenization procedure, defining the
    homogenized model in Equations<nbsp>(<reference|eq:homogenized-model-announce>\U<reference|eq:F-breakup>).<label|tab:tensors-delivered>
  </big-table>

  The expansion<nbsp>(<reference|eq:homogenized-model-announce>\U<reference|eq:KABka-expansion>)
  is established in section <reference|sec:justification> by solving the
  variational problem<nbsp>(<reference|eq:y-star-withLagrange>) for
  <math|\<b-y\>> order by order in <math|\<eta\>>. The solution is found in
  the form

  <\equation>
    \<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]><around*|(|\<b-X\>|)>=\<b-Y\><around*|[|\<b-m\>|]><around*|(|\<b-X\>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-Y\><rprime|'><around*|[|\<b-m\>|]><around*|(|\<b-X\>|)><tc2>\<nabla\>\<b-l\><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\><rsup|2>|)><label|eq:y-in-gradients-of-l>
  </equation>

  where <math|\<b-Y\><around*|[|\<b-m\>|]>> and
  <math|\<b-Y\><rprime|'><around*|[|\<b-m\>|]>> are given as expansions in
  the successive gradients of <math|\<b-m\>>,

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-Y\><around*|[|\<b-m\>|]><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>+\<b-Y\><rsub|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-m\><around*|(|\<b-X\>|)>+\<cdots\>>>|<row|<cell|\<b-Y\><rprime|'><around*|[|\<b-m\>|]><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)>+\<cdots\>>>>>><label|eq:Y-vs-gradM>
  </equation>

  and the localization tensors <math|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>>,
  <math|\<b-Y\><rsub|1><around*|(|\<b-m\>|)>> and
  <math|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)>> are derived in
  explicit form in terms of the variable material parameters <math|\<b-m\>>
  in the Appendix, see (<reference|eq:localization-tensor-order0>)
  and<nbsp>(<reference|eq:Yp-extraction>).

  The microscopic strain corresponding to the optimal displacement
  <math|\<b-y\><rprime|\<star\>>> is found in the form

  <\equation>
    \<b-E\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<b-F\><around*|[|\<b-m\>|]>\<cdot\>\<b-l\>+\<b-F\><rprime|'><around*|[|\<b-m\>|]><tc2>\<nabla\>\<b-l\>+\<b-F\><rprime|''><around*|[|\<b-m\>|]><value|tc3>\<nabla\><rsup|2>\<b-l\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>+\<cdots\><label|eq:E-optimum-summary>
  </equation>

  where the displacement <math|\<b-y\><rsub|<around*|[|2|]>>> needs to be
  resolved by pushing homogenization to a higher order and

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<b-F\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-F\><rsub|0><around*|(|\<b-m\>|)>+\<b-F\><rsub|1><around*|(|\<b-m\>|)><value|tc2>\<nabla\>\<b-m\>+\<b-F\><rsub|2><around*|(|\<b-m\>|)><value|tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>+<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>+\<cdots\>>>|<row|<cell|\<b-F\><rprime|'><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-F\><rprime|'><rsub|0><around*|(|\<b-m\>|)>+\<b-F\><rprime|'><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>+\<cdots\>>>|<row|<cell|\<b-F\><rprime|''><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-F\><rprime|''><rsub|0><around*|(|\<b-m\>|)>+\<cdots\>>>>>><label|eq:F-breakup>
  </equation>

  <subsection|Homogenization results in the form of a systematic expansion>

  The various contributions to <math|\<Phi\><rprime|\<star\>>>
  in<nbsp>(<reference|eq:homogenized-model-announce>) can be grouped order by
  order as follows, by inserting<nbsp>(<reference|eq:KABka-expansion>)
  into<nbsp>(<reference|eq:homogenized-model-announce>) and
  using<nbsp>(<reference|eq:nabla-order-of-magnitude>):

  <\itemize>
    <item>The leading-order contribution <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>>=O<around*|(|L<rsup|d>*\<eta\><rsup|0>|)>>
    is given by

    <\equation>
      \<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>=<big|int><rsub|\<Omega\>><rsup|>\<b-K\><rsub|0><around*|(|\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>*\<mathd\>\<b-X\>,<label|eq:Phi-0-form>
    </equation>

    and characterizes an equivalent Cauchy medium through a homogenized
    stiffness tensor <math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>> depending
    only on the local material parameters <math|\<b-m\>>: this homogenized
    stiffness <math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>> matches that
    predicted by classical homogenization.

    <item>The first correction <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>>=O<around*|(|L<rsup|d>*\<eta\><rsup|1>|)>>
    is given by

    <\equation>
      \<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>><around*|[|\<b-m\>,\<b-l\>|]>=<big|int><rsub|\<Omega\>><rsup|><around*|(|<around*|(|\<b-K\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>+\<b-A\><rsub|0><around*|(|\<b-m\>|)><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>|)>*\<mathd\>\<b-X\>.<label|eq:Phi-1-form>
    </equation>

    <item>The second correction <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|2|]>>=O<around*|(|L<rsup|d>*\<eta\><rsup|2>|)>>
    is given by

    <\equation>
      \<Phi\><rprime|\<star\>><rsub|<around*|[|2|]>><around*|[|\<b-m\>,\<b-l\>|]>=<big|int><rsub|\<Omega\>><rsup|><around*|(|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|r>|<table|<row|<cell|<around*|(|\<b-K\><rsub|2><around*|(|\<b-m\>|)><tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>+<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>>>|<row|<cell|<space|2em>+<around*|(|\<b-A\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>+\<b-B\><rsub|0><around*|(|\<b-m\>|)><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>+\<b-C\><rsub|0><around*|(|\<b-m\>|)><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>>>>>>|)>*\<mathd\>\<b-X\><around*|\<nobracket\>||\<nobracket\>>.<label|eq:Phi-2-form>
    </equation>
  </itemize>

  The homogenized energy <math|\<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>
  in<nbsp>(<reference|eq:homogenized-model-announce>) is nothing but the sum

  <\equation>
    \<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>>+\<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>>+\<Phi\><rprime|\<star\>><rsub|<around*|[|2|]>>+\<cal-O\><around*|(|L<rsup|d>*\<eta\><rsup|3>|)>,<label|eq:relaxed-energy-order-by-order>
  </equation>

  and it is asymptotically exact up to a higher-order contribution
  <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|3|]>>=\<cal-O\><around*|(|L<rsup|d>*\<eta\><rsup|3>|)>>
  which we do not attempt to resolve.

  <\remark>
    The actual derivation of the homogenized model proceeds in the reverse
    order than the high-level presentation above: the order-by-order
    expansion<nbsp>(<reference|eq:Phi-0-form>\U<reference|eq:relaxed-energy-order-by-order>)
    is derived first, and the compact form<nbsp>(<reference|eq:homogenized-model-announce>-<reference|eq:KABka-expansion>)
    is obtained next by rearranging the terms.
  </remark>

  The solution for <math|\<b-y\>> in<nbsp>(<reference|eq:y-in-gradients-of-l>\U<reference|eq:Y-vs-gradM>)
  is derived based on the assumption that the microscopic variables
  <math|\<b-y\>=\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>> can be
  expanded in powers of <math|\<eta\>>,

  <\equation>
    \<b-y\><around*|(|\<b-X\>|)>=\<b-y\><rsub|<around*|[|0|]>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|2|]>><around*|(|\<b-X\>|)>+\<cdots\>,<label|eq:y-expansion>
  </equation>

  where <math|\<b-y\><rsub|<around*|[|k|]>><around*|(|\<b-X\>|)>=\<cal-O\><around*|(|\<eta\><rsup|k>|)>>
  denotes the contribution of order <math|\<eta\><rsup|k>> to <math|\<b-y\>>.
  Specifically, the microscopic solution <math|\<b-y\><rprime|\<star\>>=\<b-y\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<cdots\>>
  is derived order by order as

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>|<cell|=>|<cell|<around*|(|\<b-Y\><rsub|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-l\><around*|(|\<b-X\>|)>,>>>>><label|eq-y-order-by-order>
  </equation>

  which yields<nbsp>(<reference|eq:y-in-gradients-of-l>\U<reference|eq:Y-vs-gradM>)
  by rearranging the terms.

  <\remark>
    As discussed in Section<nbsp><reference|ssec:slow-variations>, there are
    implicit scaling factors <math|\<eta\><rsup|k>> in all our formulas.
    Their consistency can be checked as follows. Take
    equation<nbsp>(<reference|eq-y-order-by-order>), for instance: the
    subscript \<#2018\><math|<around*|[|1|]>>\<#2019\> in the left-hand side
    indicates that this is a quantity of order <math|\<eta\>>; this is
    consistent with the fact that the right-hand side is homogeneous of
    degree 1 with respect to the symbol <math|\<nabla\>=\<cal-O\><around*|(|\<eta\>|)>>.
    When checking homogeneity, the boundary terms must be treated with
    special care: in Equation<nbsp>(<reference|eq:Phi-2-form>), for instance,
    the integrand of the bulk integral is as quantity of order
    <math|\<eta\><rsup|2>>, in line with the subscript
    \<#2018\><math|<around*|[|2|]>>\<#2019\> appearing in the left-hand. The
    integrand of the boundary integral is however a quantity of order
    <math|\<eta\><rsup|1>>; the paradox is resolved by noting that the
    measure of the domain is <math|\<cal-O\><around*|(|L<rsup|d>|)>> for the
    bulk integral, but \ <math|\<cal-O\><around*|(|L<rsup|d-1>|)>=\<cal-O\><around*|(|L<rsup|d>*\<eta\>|)>>
    for the boundary integral\Vrecall that
    <math|\<ell\>=\<cal-O\><around*|(|1|)>> and
    <math|L=\<cal-O\><around*|(|\<eta\><rsup|-1>|)>>. Ultimately, both
    integrals are \ of order <math|\<cal-O\><around*|(|L<rsup|d>*\<eta\><rsup|2>|)>>.
  </remark>

  <section|Illustrations><label|s:examples>

  See the JTCAM 2023 paper.

  \ <section|Derivation of the homogenized model><label|sec:justification>

  <subsection|Leading order (classical homogenization)><label|ssec:order-0-detailed-justification>

  At order <math|\<eta\><rsup|0>>, the microscopic
  displacement<nbsp>(<reference|eq:y-expansion>) is given by
  <math|\<b-y\><around*|(|\<b-X\>|)>=\<b-y\><rsub|<around*|[|0|]>><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\>|)>>.
  The gradients terms <math|\<nabla\><rsup|k>\<b-m\>>,
  <math|\<nabla\><rsup|k>\<b-l\>> and <math|\<nabla\><rsup|k>\<b-y\>> are of
  order <math|\<eta\><rsup|k>> by<nbsp>(<reference|eq:nabla-order-of-magnitude>)
  and can be ignored for <math|k\<geqslant\>1>. With the gradients neglected,
  we denote the microscopic strain in<nbsp>(<reference|eq:strain-canonical-form>)
  as

  <\equation>
    \<b-E\><rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>=\<b-E\><around*|(|\<b-m\>;\<b-l\>,\<b-0\>,\<b-0\>,\<ldots\>;\<b-y\>,\<b-0\>,\<b-0\>,\<ldots\>|)>=\<b-E\><rsub|l><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>,<label|eq:E-phi-0>
  </equation>

  and the bulk energy density in<nbsp>(<reference|eq:abstract-energy-density>)
  as

  <\equation>
    W<rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>=W<around*|(|\<b-m\>,\<b-E\><rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>|)>.<label|eq:W-0>
  </equation>

  The order <math|\<eta\><rsup|0>> approximation of the strain
  energy<nbsp>(<reference|eq:phi-canonical>) can then be obtained as
  <math|\<Phi\><around*|[|\<b-m\>,\<b-l\>,\<b-y\>|]>=\<Phi\><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|0|]>>|]>+\<cal-O\><around*|(|L<rsup|d>*\<eta\>|)>>,
  where

  <\equation>
    <math|\<Phi\><rsub|<around*|[|0|]>>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|0|]>>|]>=<big|int><rsub|\<Omega\>>W<rsup|<around*|(|0|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>,\<b-l\><around*|(|\<b-X\>|)>,\<b-y\><rsub|<around*|[|0|]>><around*|(|\<b-X\>|)>|)>*\<mathd\>\<b-X\>.<label|eq:phi-continuous-order0>
  </equation>

  At leading order <math|\<eta\><rsup|0>>, the variational
  problem<nbsp>(<reference|eq:y-star-withLagrange>) can be written as

  <\equation>
    <around*|{|<tabular|<tformat|<table|<row|<cell|\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\><rsub|<around*|[|0|]>><around*|(|\<b-X\>|)>+\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>|)>=\<b-0\><space|1em>\<forall\>\<b-X\>
    >>|<row|<cell|\<mathD\><rsub|\<b-y\>><math|\<Phi\><rsub|<around*|[|0|]>>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|0|]>>;\<delta\>\<b-y\>|]>+<big|int><rsub|\<Omega\>><around*|(|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|\<nobracket\>>|)><rsup|T>\<cdot\>\<b-g\><rsub|<around*|[|0|]>><around*|(|\<b-X\>|)>|)>\<cdot\>\<delta\>\<b-y\><around*|(|\<b-X\>|)><around*|\<nobracket\>||\<nobracket\>>*\<mathd\>\<b-X\>=\<b-0\><space|1em>\<forall\>\<delta\>\<b-y\>.>>>>>|\<nobracket\>><label|eq:order-0-tmpA>
  </equation>

  Its solution is denoted as <math|<around*|(|\<b-y\><rsub|<around*|[|0|]>>,\<b-g\><rsub|<around*|[|0|]>>|)>=<around*|(|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>,\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>>|)>>.

  No gradient of <math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>>> is present in
  the expression of <math|\<Phi\><rsub|<around*|[|0|]>>>
  in<nbsp>(<reference|eq:phi-continuous-order0>) nor in the integral
  in<nbsp>(<reference|eq:order-0-tmpA>)<rsub|2>, implying that this
  variational problem is <em|local>: at any point <math|\<b-X\>>, we must
  solve the following problem for the unknowns
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  and <math|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>,

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-0\>>>|<row|<cell|<frac|\<partial\>W<rsup|<around*|(|0|)>>|\<partial\>\<b-y\>><around*|(|\<b-m\><around*|(|\<b-X\>|)>,\<b-l\><around*|(|\<b-X\>|)>,\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>|)>+<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|\<nobracket\>>|)><rsup|T>\<cdot\>\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-0\>,>>>>><label|eq:constrained-variational-pb-yHom>
  </equation>

  where we have used the expression of <math|\<Phi\><rsub|<around*|[|0|]>>>
  in<nbsp>(<reference|eq:phi-continuous-order0>). The solution
  <math|<around*|(|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>,\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>|)>>
  at any particular point <math|\<b-X\>> depends on the local values of
  <math|\<b-m\><around*|(|\<b-X\>|)>> and <math|\<b-l\><around*|(|\<b-X\>|)>>
  only. In Appendix<nbsp><reference|app:homogeneous-solution>, the solution
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  is obtained in the form announced earlier
  in<nbsp>(<reference|eq-y-order-by-order>)<rsub|1>,

  <\equation>
    \<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>,<label|eq:y-hom-from-h>
  </equation>

  and an explicit expression for the localization tensor
  <math|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>> is given
  in<nbsp>(<reference|eq:R-order0>\U<reference|eq:localization-tensor-order0>).

  Inserting<nbsp>(<reference|eq:y-hom-from-h>)
  into<nbsp>(<reference|eq:phi-continuous-order0>), we derive in
  Appendix<nbsp><reference|app:homogeneous-solution> the dominant
  contribution to the energy <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>=<math|\<Phi\><rsub|<around*|[|0|]>>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>|]>>
  that was announced in<nbsp>(<reference|eq:Phi-0-form>), namely

  <\equation>
    \<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>=<math|<big|int><rsub|\<Omega\>><frac|1|2>*\<b-l\><around*|(|\<b-X\>|)>\<cdot\>\<b-K\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>>.<label|eq:phi-0-in-terms-of-W-hom><rsup|>
  </equation>

  The expression of the leading-order stiffness tensor
  <math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>> is given in
  Equation<nbsp>(<reference|eq:K0-def>) in the Appendix.

  <subsection|Analysis of the gradient effect><label|sec:analysis-gradient-effect>

  We now proceed to solve the next orders in the microscopic displacement,
  see<nbsp>(<reference|eq:y-expansion>) and<nbsp>(<reference|eq:y-hom-from-h>),

  <\equation>
    \<b-y\><around*|(|\<b-X\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|2|]>><around*|(|\<b-X\>|)>+\<cdots\><label|eq:y-expansion-leading-order-known>
  </equation>

  Inserting this into<nbsp>(<reference|eq:phi-canonical>), we derive a Taylor
  expansion of the energy as

  <\equation>
    \<Phi\>=\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><rsub|<around*|[|2|]>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]><label|eq:Phic-expansion-showing-dep-on-y1>
  </equation>

  where <math|\<Phi\><rsub|<around*|[|k|]>>> denotes the term of order
  <math|\<eta\><rsup|k>>, and the star is used to mark energy contributions
  that do not depend on the yet-unknown corrector
  <math|\<b-y\><rsub|<around*|[|1|]>>>. The dominant term
  <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>>> is the functional
  found earlier in<nbsp>(<reference|eq:phi-0-in-terms-of-W-hom>), while the
  next-order terms <math|\<Phi\><rsub|<around*|[|1|]>>> and
  <math|\<Phi\><rsub|<around*|[|2|]>>> are obtained in
  Equations<nbsp>(<reference|eq:phi-1-just-A>)
  and<nbsp>(<reference|eq:phi2-tmp2>) in Appendix<nbsp><reference|app:gradient-effect>
  as

  <\equation>
    <tabular|<tformat|<cwith|1|3|1|1|cell-halign|r>|<table|<row|<cell|\<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>><around*|[|\<b-m\>,\<b-l\>|]>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-cal-A\><around*|(|\<b-m\>|)><tc2>\<b-h\>\<otimes\>\<b-h\>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>>>|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<cwith|3|3|1|1|cell-halign|r>|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|<around*|(|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>+<around*|(|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\><space|2em>>>|<row|<cell|+<around*|(|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|)><around*|\<nobracket\>||\<nobracket\>>+\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)><tc2><frac|\<b-y\><rsub|<around*|[|1|]>>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|2>>>|<row|<cell|+<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><rsup|><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>>>>>>|)>*\<mathd\>\<b-X\>>>>>><label|eq-phi-1-2-to-be-relaxed>
  </equation>

  Closed-form expressions for the operators appearing in the right-hand side
  are derived in Appendix<nbsp><reference|app:gradient-effect>. In the
  right-hand sides above, we have introduced the vector
  <math|\<b-h\>=<around*|(|\<b-l\>,\<b-m\>,<around*|(|1|)>|)>> obtained by
  concatenating the macroscopic variables <math|\<b-l\>> and <math|\<b-m\>>,
  and adding a trailing entry 1, see<nbsp>Section<nbsp><reference|ssec:packed-h>:
  this notation trick simplifies the calculations significantly.

  It is remarkable that <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|1|]>><around*|[|\<b-m\>,\<b-l\>|]>>
  does not depend on the corrector <math|\<b-y\><rsub|<around*|[|1|]>>>, even
  though both are of order <math|\<eta\>>. As a result, the first correction
  <math|\<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>> depends on the
  macroscopic variables <math|<around*|(|\<b-m\>,\<b-l\>|)>> only, as
  conveyed by the star notation which we reserve for the output of the
  homogenization procedure.

  For a similar reason explained in the Appendix,
  <math|\<Phi\><rsup|><rsub|<around*|[|2|]>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>>
  does not depend on <math|\<b-y\><rsub|<around*|[|2|]>>> even though both
  are of order <math|\<eta\><rsup|2>>. It does depend on the unknown
  correction <math|\<b-y\><rsub|<around*|[|1|]>>>, however. The gradient term
  <math|\<nabla\>\<b-y\><rsub|<around*|[|1|]>>> appearing in the integrand of
  <math|\<Phi\><rsub|<around*|[|2|]>>> in<nbsp>(<reference|eq-phi-1-2-to-be-relaxed>)<rsub|2>
  can be removed by integrating by parts the
  <math|\<b-cal-C\><rsup|<around*|(|1|)>><rsup|>> term\Vthe benefit is that
  the problem of optimizing <math|\<Phi\><rsub|<around*|[|2|]>>> with respect
  to the function <math|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>>
  then becomes a <em|local> problem. This integration by parts is carried out
  in section<nbsp><reference|ssec:ibp> the Appendix and the result is given
  in<nbsp>(<reference|eq:Phi-2-after-ibp>) as

  <\equation>
    \<Phi\><rsub|<around*|[|2|]>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>=<wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|bt>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>+<wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|it>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>,<label|eq:split-phi-2>
  </equation>

  where the boundary terms <math|<wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|bt>>>
  and integral terms <math|<wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|it>>>
  are given respectively by

  <\equation>
    <wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|bt>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>|]>=<big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2><around*|(|\<b-y\><rsub|<around*|[|1|]>>\<otimes\>\<b-n\>|)>*\<mathd\>a<label|eq:phi-2-bt>
  </equation>

  and

  <\equation>
    <wide|\<Phi\>|~><rsub|<around*|[|2|]>><rsup|<text|it>><around*|[|\<b-h\>,\<b-y\><rsub|<around*|[|1|]>>|]>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)><tc2><frac|\<b-y\><rsub|<around*|[|1|]>>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|2>+<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|)>+\<ldots\>|)>*\<mathd\>\<b-X\><label|eq:PhiExpansionAnnounce>
  </equation>

  where the new operator <math|\<b-cal-B\><rsup|<around*|(|1|)>>> is defined
  in terms of <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>>
  and <math|\<b-cal-C\><rsup|<around*|(|1|)>><rsup|>>
  in<nbsp>(<reference|eq:delta-B>,<reference|eq:Att-F-Bparenth>), and the
  ellipsis stands for terms that do not depend on
  <math|\<b-y\><rsub|<around*|[|1|]>>>. As intented, the gradient term
  <math|\<nabla\>\<b-y\><rsub|<around*|[|1|]>>> has disappeared.

  Having worked out the expansion of the energy, we proceed to solve the
  variational problem<nbsp>(<reference|eq:y-star-withLagrange>) at order
  <math|\<eta\>>: inserting the expansion
  <math|\<b-y\><around*|(|\<b-X\>|)>=\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<cdots\>>
  into the energy given by<nbsp>(<reference|eq:Phic-expansion-showing-dep-on-y1>),
  (<reference|eq-phi-1-2-to-be-relaxed>)<rsub|1>
  and<nbsp>(<reference|eq:split-phi-2>\U<reference|eq:PhiExpansionAnnounce>),
  we get a variational problem for the corrector
  <math|\<b-y\><rsub|<around*|[|1|]>>> and a Lagrange multiplier
  <math|\<b-g\><rsub|<around*|[|1|]>>>,

  <\equation>
    <around*|{|<tabular|<tformat|<table|<row|<cell|\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>|)>=\<b-0\><space|1em>\<forall\>\<b-X\>>>|<row|<cell|\<mathD\><rsub|\<b-y\>><math|\<Phi\><rsub|<around*|[|2|]>>><around*|[|\<b-m\>,\<b-l\>,\<b-y\><rsub|<around*|[|1|]>>;\<delta\>\<b-y\>|]>+<big|int><rsub|\<Omega\>>\<b-g\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>\<cdot\>\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<delta\>\<b-y\><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>=\<b-0\><space|1em><around*|(|\<forall\>\<delta\>\<b-y\>|)>.>>>>>|\<nobracket\>><label|eq:stationarity-phi2>
  </equation>

  The incremental form of the kinematic constraint appearing
  in<nbsp>(<reference|eq:stationarity-phi2>)<rsub|1> above is established in
  the Appendix in terms of an operator <math|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>,
  see (<reference|eq:Q-yi-is-zero>), (<reference|eq:structure-coefficients>)<rsub|1>
  and (<reference|eq:E-alpha-expansion-nearly-done-components>)<rsub|1>.

  In the absence of any gradient of <math|\<b-y\><rsub|<around*|[|1|]>>>,
  see<nbsp>(<reference|eq:PhiExpansionAnnounce>), the variational problem for
  <math|\<b-y\><rsub|<around*|[|1|]>>> in<nbsp>(<reference|eq:stationarity-phi2>)
  is local. This variational problem is solved in the Appendix,
  �<reference|sec:app-optimal-corrective-displacement>:

  <\itemize>
    <item>The boundary integral <math|\<Phi\><rsub|<around*|[|2|]>><rsup|<text|bt>>>
    from Equation<nbsp>(<reference|eq:phi-2-bt>) yields a stationarity
    condition applicable on the boundary <math|\<partial\>\<Omega\>> of the
    domain, see Equation<nbsp>(<reference|eq:condition-on-boundary>) in the
    Appendix. This condition does not depend on
    <math|\<b-y\><rsub|<around*|[|1|]>>> and it warrants variational
    consistency of the input model <math|\<Phi\>>. We will analyze this
    condition further in future work.

    <item>The bulk integral <math|\<Phi\><rsub|<around*|[|2|]>><rsup|<text|it>>>
    from Equation<nbsp>(<reference|eq:PhiExpansionAnnounce>) yields a
    stationarity condition applicable in the interior
    <math|\<Omega\><rsup|\<circ\>>> of the domain, that yields the corrector
    <math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
    and Lagrange multiplier <math|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>.
    The solution is of the form announced
    in<nbsp>(<reference|eq-y-order-by-order>)<rsub|2>,

    <\equation>
      \<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=<around*|(|\<b-Y\><rsub|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-l\><around*|(|\<b-X\>|)>,<label|eq:y1-star>
    </equation>

    where the localization tensors <math|\<b-Y\><rsub|1><around*|(|\<b-m\>|)>>
    and <math|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>
    are derived in Equation<nbsp>(<reference|eq:Yp-extraction>) in the
    Appendix.
  </itemize>

  \;

  Inserting this solution into<nbsp>(<reference|eq:phi-canonical>) yields the
  functional <math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>>
  that has been announced in<nbsp>(<reference|eq:Phi-2-form>).

  <section|Symbolic implementation: The <verbatim|shoal>
  library><label|sec:numerical-implementation>

  We have implemented the method in the symbolic calculation language Wolfram
  Mathematica<nbsp><cite|Mathematica>, and we distribute it as an open-source
  library named <verbatim|shoal> (for Second-order HOmogenization Automated
  using a Library). The library can be obtained with the command
  <verbatim|git clone https://git.renater.fr/anonscm/git/shoal/shoal.git>,
  and is also available from the <em|permanent> Software Heritage
  Archive<nbsp><cite|shoalLibPermanent>.

  The data listed in Table<nbsp><reference|tab:input> describing the problem
  at hand is passed to this library, which then returns the tensors listed in
  Table<nbsp><reference|tab:tensors-delivered>. The extension to
  rank-deficient problems presented in Appendix<nbsp><reference|app:rank-deficient>
  is implemented. The homogenization proceeds by computing the following
  quantities, in the following order:

  <\itemize>
    <item><math|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:Wcal>)<rsub|2\U3>

    <item><math|\<b-P\><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:P-matrix>), its null vectors
    <math|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>>
    in<nbsp>(<reference|eq:NA-def>), its Moore-Penrose inverse
    <math|\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>>, and
    <math|<wide|\<b-cal-I\>|\<check\>>> and <math|<wide|\<b-cal-I\>|^>>
    in<nbsp>(<reference|eq:deficient-injection-matrices>)

    <item><math|\<b-cal-R\><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:def-modify-R>),
    <math|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>>,
    <math|\<b-G\><rsub|0><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:localization-tensor-order0>\U<reference|eq:localization-tensor-Lambda-0>),
    and then <math|\<b-F\><rsub|0><around*|(|\<b-m\>|)>>,
    <math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>> and
    <math|\<b-S\><rsub|0><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:E-localization>), (<reference|eq:K0-def>)
    and<nbsp>(<reference|eq:total-stress-S0>), see also
    Table<nbsp><reference|tab:leading-order-summary>

    <item><math|\<b-cal-V\><rsup|l>>, <math|\<b-cal-V\><rsup|m>>,
    <math|\<b-cal-V\><rsup|1>> using<nbsp>(<reference|eq:Vs>)

    <item><math|\<b-cal-L\><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:L-calligraphic-operator>),
    <math|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:cal-L-operators>),
    <math|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:structure-coefficients>)

    <item><math|\<b-cal-A\><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:W1-A0-def>), and then
    <math|\<b-A\><rsub|0><around*|(|\<b-m\>|)>>
    <math|\<b-K\><rsub|1><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:A0-K1>)

    <item><math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>,
    <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:deficient-Bcirc-C>)

    <item><math|\<Delta\>\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:delta-B>), and then
    <math|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:Att-F-Bparenth>)

    <item><math|\<b-cal-R\><rprime|'><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-G\><rprime|'><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:implement-P-R>\U<reference|eq:lambda1sol>), and
    then <math|\<b-Y\><rsub|1><around*|(|\<b-m\>|)>>,
    <math|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:Yp-extraction>)

    <item><math|\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-F\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>> and
    <math|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:calF-def>) and then
    <math|\<b-F\><rsub|0><rprime|'><around*|(|\<b-m\>|)>>, <text-dots>,
    <math|<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:extract-F>)

    <item><math|\<b-cal-B\><around*|(|\<b-m\>|)>>,
    <math|\<b-cal-C\><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:B-C-rank-deficient-aware>) and then
    <math|\<b-K\><rsub|2><around*|(|\<b-m\>|)>>,
    <math|\<b-A\><rsub|1><around*|(|\<b-m\>|)>>,
    <math|\<b-B\><rsub|0><around*|(|\<b-m\>|)>>,
    <math|<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)>> and
    <math|\<b-C\><rsub|0><around*|(|\<b-m\>|)>>
    using<nbsp>(<reference|eq:BC-extract>)

    <item>if <math|<nd>\<gtr\>0>, the solvability conditions appearing
    in<nbsp>(<reference|eq:solvability-y0>), (<reference|eq:solvability-y1>)
    and<nbsp>(<reference|eq:solvability-y2>).
  </itemize>

  The implementation makes use of standard tensor algebra operations on
  symbolic tensors, including general transpositions and multiple
  contractions, as well as symbolic differentiation with respect to
  <math|\<b-m\>>, see<nbsp>(<reference|eq:delta-B>). Note that the vector
  <math|\<b-l\>> never appears explicitly in the implementation.

  At leading order and in the non-deficient case, the procedure is
  implemented by the equations listed in Table<nbsp><reference|tab:leading-order-summary>,
  corresponding to the first three bullet points above. The homogenization at
  the two following orders makes use of the subsequent bullet points. The
  special case of uniform properties, when the parameter <math|\<b-m\>> is
  absent, is worked out in Appendix<nbsp><reference|app:homogeneous
  properties>, see Table<nbsp><reference|tab:ho-homogenization-uniform-props>
  in particular.

  <\big-table|<math|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
  \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-E\><rsub|y><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
  \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-E\><rsub|l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-P\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>|<cell|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>>|<cell|\<b-0\><rsub|<nc>\<times\><nc>>>>>>>>>|<row|<cell|\<b-cal-R\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|-\<b-P\><rsup|-1><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>>>|<row|<cell|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-cal-R\><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-G\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><nc>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<cdot\>\<b-cal-R\><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-F\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|l><around*|(|\<b-m\>|)>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-Y\><rsub|0><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-K\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-F\><rsub|0><rsup|T><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-F\><rsub|0><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-S\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-F\><rsub|0><around*|(|\<b-m\>|)>+\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>>>>>>>>
    Implementation of the leading-order procedure in the non-deficient case
    (<math|n<rsub|<text|d>>=0>), based on the formulas referenced in the
    first three items in the bullet list from
    Section<nbsp><reference|sec:numerical-implementation>.<label|tab:leading-order-summary>
  </big-table>

  <\bibliography|bib|tm-alpha|biblio>
    <\bib-list|5>
      <bibitem*|21><label|bib-Mathematica>Wolfram Research, Inc.
      <newblock>Mathematica, Version 13.0.0. <newblock>2021.
      <newblock>Champaign, IL, 2021.<newblock>

      <bibitem*|AL23><label|bib-Audoly-Lestringant-An-energy-approach-to-asymptotic-2023>B.<nbsp>Audoly<localize|
      and >C.<nbsp>Lestringant. <newblock>An energy approach to asymptotic,
      higher-order, linear homogenization.
      <newblock><with|font-shape|italic|Journal of Theoretical, Computational
      and Applied Mechanics>, 2023.<newblock>

      <bibitem*|AS18><label|bib-abdoul2018strain>Houssam
      Abdoul-Anziz<localize| and >Pierre Seppecher. <newblock>Strain gradient
      and generalized continua obtained by homogenizing frame lattices.
      <newblock><with|font-shape|italic|Mathematics and mechanics of complex
      systems>, 6(3):213\U250, 2018.<newblock>

      <bibitem*|DMP12><label|bib-david2012homogenized>Martin David, J-J
      Marigo<localize|, and >C Pideri. <newblock>Homogenized interface model
      describing inhomogeneities located on a surface.
      <newblock><with|font-shape|italic|Journal of Elasticity>, 109:153\U187,
      2012.<newblock>

      <bibitem*|sho23><label|bib-shoalLibPermanent>The shoal library.
      <newblock><slink|https://archive.softwareheritage.org/browse/origin/https://git.renater.fr/anonscm/git/shoal/shoal.git>,
      2023.<newblock>
    </bib-list>
  </bibliography>

  <appendix|Tensor algebra><label|a:tensors>

  The dimension of the Euclidean space is denoted as <math|d>. The Euclidean
  space is endowed with an orthonormal Cartesian basis
  <math|<around*|(|\<b-e\><rsub|1>,\<ldots\>,\<b-e\><rsub|d>|)>>. A generic
  point in the Euclidean space is denoted as
  <math|\<b-X\>\<in\>\<bbb-R\><rsup|d>>.

  We denote as <math|\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>>
  the tensor space <math|\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>=\<bbb-R\><rsup|n<rsub|1>>\<otimes\>\<bbb-R\><rsup|n<rsub|2>>\<otimes\>\<ldots\>\<otimes\>\<bbb-R\><rsup|n<rsub|p>>>
  made of tensors <math|\<b-R\>> of rank <math|p> and dimensions
  <math|n<rsub|1>\<times\>n<rsub|2>\<times\>\<cdots\>\<times\>n<rsub|p>>. In
  particular, <math|\<b-I\><rsub|k>\<in\>\<bbb-T\><rsup|<around*|(|k,k|)>>>
  denotes the identity matrix in dimension <math|k> and
  <math|\<b-0\><rsub|n<rsub|1>\<times\>\<cdots\>\<times\>n<rsub|p>>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>>
  the null tensor with dimensions <math|n<rsub|1>\<times\>\<cdots\>\<times\>n<rsub|p>>.

  Tensors and vectors are denoted using bold symbols, while scalars
  (including tensor <em|components>) are denoted using non-bold symbols.

  Given two tensors <math|\<b-R\>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>>
  and <math|\<b-R\><rprime|'>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1><rprime|'>,n<rsub|2><rprime|'>,\<ldots\>,n<rsub|p><rprime|'>|)>>>,
  we denote as

  <\itemize>
    <item><math|\<b-R\>\<cdot\>\<b-R\><rprime|'>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p-1>,n<rsub|2><rprime|'>,\<ldots\>,<rsub|>n<rsub|p><rprime|'>|)>>>
    their simple contraction (whose existence requires
    <math|n<rsub|p>=n<rsub|1><rprime|'>>),

    <item><math|\<b-R\><tc2>\<b-R\><rprime|'>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p-2>,n<rsub|3><rprime|'>,\<ldots\>,<rsub|>n<rsub|p><rprime|'>|)>>>
    their double contraction (whose existence requires
    <math|n<rsub|p-1>=n<rsub|1><rprime|'>> and
    <math|n<rsub|p>=n<rsub|2><rprime|'>>),

    <item><math|\<b-R\><tc3>\<b-R\><rprime|'>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p-3>,n<rsub|4><rprime|'>,\<ldots\>,<rsub|>n<rsub|p><rprime|'>|)>>>
    their triple contraction, (whose existence requires
    <math|n<rsub|p-2>=n<rsub|1><rprime|'>>,
    <math|n<rsub|p-1>=n<rsub|2><rprime|'>> and
    <math|n<rsub|p>=n<rsub|3><rprime|'>>),

    <item>etc.
  </itemize>

  If they exist, the contracted tensors are given by

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<around*|(|\<b-R\>\<cdot\>\<b-R\><rprime|'>|)><rsub|i<rsub|1>\<ldots\>i<rsub|p-1>\<nocomma\>i<rsub|2><rprime|'>\<ldots\>i<rsub|p><rprime|'>>>|<cell|=>|<cell|R<rsub|i<rsub|1>\<ldots\>i<rsub|p-1>\<nocomma\>j>*R<rprime|'><rsub|j\<nocomma\>i<rsub|2><rprime|'>\<ldots\>i<rsub|p><rprime|'>>>>|<row|<cell|<around*|(|\<b-R\><tc2>\<b-R\><rprime|'>|)><rsub|i<rsub|1>\<ldots\>i<rsub|p-2>\<nocomma\>i<rsub|3><rprime|'>\<ldots\>i<rsub|p><rprime|'>>>|<cell|=>|<cell|R<rsub|i<rsub|1>\<ldots\>i<rsub|p-2>\<nocomma\>j\<nocomma\>k>*R<rprime|'><rsub|j\<nocomma\>k\<nocomma\>i<rsub|3><rprime|'>\<ldots\>i<rsub|p><rprime|'>>>>|<row|<cell|<around*|(|\<b-R\><tc3>\<b-R\><rprime|'>|)><rsub|i<rsub|1>\<ldots\>i<rsub|p-3>\<nocomma\>i<rsub|4><rprime|'>\<ldots\>i<rsub|p><rprime|'>>>|<cell|=>|<cell|R<rsub|i<rsub|1>\<ldots\>i<rsub|p-3>\<nocomma\>j\<nocomma\>k\<nocomma\>l>*R<rprime|'><rsub|j\<nocomma\>k\<nocomma\>l\<nocomma\>i<rsub|4><rprime|'>\<ldots\>i<rsub|p><rprime|'>>.>>>>><label|eq:tensorContractions>
  </equation>

  Note the ordering of the contracted indices <math|j>, <math|k>, <math|l>,
  etc.<nbsp>in the right-hand sides\Vin particular, the double contraction of
  two rank-2 tensors <math|\<b-A\>> and <math|\<b-B\>> is given in our
  notation by <math|\<b-A\><tc2>\<b-B\>=tr
  <around*|(|\<b-A\>\<cdot\>\<b-B\><rsup|T>|)>>. Here and elsewhere in the
  paper, we use Einstein summation whereby any index that is repeated on one
  side of the equal sign is implicitly summed.

  The action of a matrix <math|\<b-R\>> on a vector <math|\<b-v\>> is viewed
  as a special case of the contraction of a tensor of rank 2 with a tensor of
  rank 1, and is denoted as <math|\<b-R\>\<cdot\>\<b-v\>>, with a dot.

  The outer product of two tensors <math|\<b-T\>> and
  <math|\<b-T\><rprime|'>> is denoted as <math|\<b-T\>\<otimes\>\<b-T\><rprime|'>>.
  In particular, the outer product of two vectors is denoted as
  <math|\<b-v\>\<otimes\>\<b-v\><rprime|'>>. Vector transposition is not a
  meaningful operation in our notation.

  Given a tensor <math|\<b-R\>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>>
  and a permutation <math|<around*|(|\<sigma\><rsub|1>,\<ldots\>,\<sigma\><rsub|p>|)>>
  of the levels <math|<around*|(|1,\<ldots\>,p|)>> of the tensor, we denote
  as <math|\<b-R\><rsup|T<rsub|\<sigma\><rsub|1>\<ldots\>\<sigma\><rsub|p>>>>
  the generalized transpose of <math|\<b-R\>><math|>, such that the level
  <math|i> in the original tensor becomes level <math|\<sigma\><rsub|i>> in
  the transpose:

  <\equation>
    <around*|(|\<b-R\><rsup|T<rsub|\<sigma\><rsub|1>\<ldots\>\<sigma\><rsub|p>>>|)><rsub|i<rsub|1>\<nocomma\>\<ldots\>\<nocomma\>i<rsub|p>>=R<rsub|i<rsub|\<sigma\><rsub|1>>\<nocomma\>\<ldots\>\<nocomma\>i<rsub|\<sigma\><rsub|p>>>.
  </equation>

  For a tensor of rank <math|p=4> and the permutation
  <math|<around*|(|\<sigma\><rsub|1>,\<sigma\><rsub|2>,\<sigma\><rsub|3>,\<sigma\><rsub|4>|)>=<around*|(|1,3,4,2|)>>,
  for instance, we have <math|<around*|(|R<rsup|T<rsub|1\<nocomma\>3\<nocomma\>4\<nocomma\>2>>|)><rsub|i\<nocomma\>j\<nocomma\>k\<nocomma\>l>=R<rsub|i\<nocomma\>k\<nocomma\>l\<nocomma\>j>>.

  Transposing will allow us to reorder the indices of a tensor in any desired
  order. Suppose for instance that we wish to rewrite an expression
  <math|R<rsub|i\<nocomma\>k\<nocomma\>l\<nocomma\>j>> as the component
  <math|R<rprime|'><rsub|i\<nocomma\>j\<nocomma\>k\<nocomma\>l>> of another
  tensor whose indices must appear in <em|alphabetical> order:
  <math|\<b-R\><rprime|'>> is clearly a transpose of <math|\<b-R\>>, and the
  permutation is found by noting that the levels <math|<around*|(|1,2,3,4|)>>
  in the original tensor <math|\<b-R\>>, corresponding to the indices
  <math|<around*|(|i,k,l,j|)>>, become respectively the levels
  <math|<around*|(|1,3,4,2|)>=<around*|(|\<sigma\><rsub|1>,\<sigma\><rsub|2>,\<sigma\><rsub|3>,\<sigma\><rsub|4>|)>>
  in <math|\<b-R\><rprime|'>>. This yields

  <\equation>
    R<rsub|i\<nocomma\>k\<nocomma\>l\<nocomma\>j>=<around*|(|\<b-R\><rsup|T<rsub|13\<nocomma\>4\<nocomma\>2>>|)><rsub|i\<nocomma\>j\<nocomma\>k\<nocomma\>l>.
  </equation>

  Index reordering using transposition will be routinely used in combination
  with contractions to remove indices in tensor algebra, as in
  <math|R<rsub|i\<nocomma\>k\<nocomma\>l\<nocomma\>j>*R<rprime|'><rsub|i\<nocomma\>j\<nocomma\>k\<nocomma\>l>=\<b-R\><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>><tc4>\<b-R\><rprime|'>>.

  The transpose <math|\<b-R\><rsup|T>> of a <em|matrix>
  <math|\<b-R\>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>|)>>> is a
  particular case of the generalized transpose,
  <math|\<b-R\><rsup|T>=\<b-R\><rsup|T<rsub|2\<nocomma\>1>>>.

  The symmetrization of a tensor <math|\<b-R\>> with respect to a pair of
  indices <math|<around*|(|i,j|)>> is denoted as
  <math|\<b-R\><rsup|S<rsub|i\<nocomma\>j>>>. For a tensor <math|\<b-R\>> of
  rank <math|p=4>, for instance, the symmetrization with respect to the first
  and third indices is given by

  <\equation>
    \<b-R\><rsup|S<rsub|1\<nocomma\>3>>=<frac|1|2>*<around*|(|\<b-R\>+\<b-R\><rsup|T<rsub|3\<nocomma\>2\<nocomma\>1\<nocomma\>4>>|)>,
  </equation>

  where <math|\<b-R\><rsup|T<rsub|3\<nocomma\>2\<nocomma\>1\<nocomma\>4>>> is
  obtained from <math|\<b-R\>> by permuting the first and third levels.\ 

  A tensor invariant by a permutation of its levels <math|i> and <math|j>
  will be said to satisfy the <math|S<rsub|i\<nocomma\>j>> symmetry; for
  instance, <math|\<b-R\><rsup|S<rsub|1\<nocomma\>3>>> is
  <math|S<rsub|1\<nocomma\>3>> symmetric, by construction. More generally, a
  tensor will be said to satisfy the <math|S<rsub|<around*|{|i\<nocomma\>j|}>\<nocomma\><around*|{|k\<nocomma\>l|}>>>
  symmetry if it is symmetric by the <em|combined> permutation of indices
  <math|i\<leftrightarrow\>k> and <math|j\<leftrightarrow\>l>. For instance,

  <\equation>
    <text|<math|\<b-R\>> is <math|S<rsub|<around*|{|2\<nocomma\>3|}>\<nocomma\><around*|{|4\<nocomma\>5|}>>>
    symmetric><space|1em>\<Leftrightarrow\><space|1em>R<rsub|i\<nocomma\>j\<nocomma\>k\<nocomma\>l\<nocomma\>m>=R<rsub|i\<nocomma\>l\<nocomma\>m\<nocomma\>j\<nocomma\>k>.<label|eq:symmetrize-with-respect-to-groups-of-indices>
  </equation>

  The symmetrization with respect to pairs of indices works similarly,

  <\equation>
    \<b-R\><rsup|S<rsub|<around*|{|23|}><around*|{|45|}>>>=<frac|1|2>*<around*|(|\<b-R\>+\<b-R\><rsup|T<rsub|1\<nocomma\>4\<nocomma\>5\<nocomma\>2\<nocomma\>3>>|)>.<label|eq:symmetrize-group-indices-demo>
  </equation>

  Clearly, <math|\<b-R\>> is <math|S<rsub|<around*|{|2\<nocomma\>3|}>\<nocomma\><around*|{|4\<nocomma\>5|}>>>
  symmetric if and only if <math|\<b-R\><rsup|S<rsub|<around*|{|23|}><around*|{|45|}>>>=\<b-R\>>.

  The composition of symmetrizations is represented by the symbol
  <math|\<circ\>>. In Equation<nbsp>(<reference|eq:cal-L-operators>), for
  instance, it stands for

  <\equation>
    \<b-R\><rsup|S<rsub|2\<nocomma\>3>\<circ\>S<rsub|<around*|{|45|}><around*|{|67|}>>>=<around*|(|\<b-R\><rsup|S<rsub|<around*|{|45|}><around*|{|67|}>>>|)><rsup|S<rsub|2\<nocomma\>3>><rsup|>.<label|eq:symmetrize-compose>
  </equation>

  \;

  Given a tensor <math|\<b-R\><around*|(|\<b-q\>|)>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>|)>>>
  taking an argument <math|\<b-q\>\<in\>\<bbb-R\><rsup|n<rsub|q>>>, we denote
  as <math|<frac|\<mathd\>\<b-R\>|\<mathd\>\<b-q\>>\<in\>\<bbb-T\><rsup|<around*|(|n<rsub|1>,n<rsub|2>,\<ldots\>,n<rsub|p>,n<rsub|q>|)>>>
  its gradient,

  <\equation*>
    <around*|(|<frac|\<mathd\>\<b-R\>|\<mathd\>\<b-q\>>|)><rsub|i<rsub|1>\<nocomma\>\<ldots\>\<nocomma\>i<rsub|p>\<nocomma\>j>=<frac|\<partial\>R<rsub|i<rsub|1>\<nocomma\>\<ldots\>\<nocomma\>i<rsub|p>>|\<partial\>q<rsub|j>>.
  </equation*>

  By a standard convention, the index <math|j> corresponding to
  differentiation appears <em|last> in the gradient. When the parameter
  <math|\<b-q\>> coincides with the spatial variable
  <math|\<b-X\>\<in\>\<bbb-R\><rsup|d>>, we use the nabla notation,

  <\equation*>
    \<nabla\>\<b-R\>=<frac|\<mathd\>\<b-R\>|\<mathd\>\<b-X\>>.
  </equation*>

  The alternate notation <math|\<b-R\>\<nabla\>> has the advantage of
  respecting the order of indices but is also less standard.

  <appendix|Detailed analysis of leading order (classical
  homogenization)><label|app:homogeneous-solution>

  Inserting the microscopic strain <math|\<b-E\><rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>>
  given in<nbsp>(<reference|eq:E-phi-0>) into the
  expression<nbsp>(<reference|eq:W-0>) of the strain energy density
  <math|W<rsup|<around*|(|0|)>><around*|(|\<b-h\>,\<b-y\>|)>>, we have

  <\equation>
    <tabular|<tformat|<table|<row|<cell|W<rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>>|<cell|=>|<cell|W<around*|(|\<b-m\>,\<b-E\><rsub|l><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>|)>>>>>><label|eq:W0-basic>
  </equation>

  Using the expression of <math|W> in<nbsp>(<reference|eq:abstract-energy-density>)
  and expanding, we rewrite this in block-matrix notation as

  <\equation>
    W<rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>|)>=<frac|1|2>*<matrix|<tformat|<table|<row|<cell|\<b-l\>>>|<row|<cell|\<b-y\>>>>>>\<cdot\>\<b-cal-W\><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-l\>>>|<row|<cell|\<b-y\>>>>>>,<text|<space|1em>where
    >\<b-cal-W\><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|l\<nocomma\>l><around*|(|\<b-m\>|)>>|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><rsup|T><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>>>>>,<label|eq:W0-quadratic>
  </equation>

  and the tensors <math|\<b-cal-W\><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nl>+<ny>,<nl>+<ny>|)>>>,
  <math|\<b-cal-W\><rsub|l\<nocomma\>l><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nl>,<nl>|)>>>,
  <math|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<ny>,<nl>|)>>>
  and <math|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<ny>,<ny>|)>>>
  are given by

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-W\><rsub|l\<nocomma\>l><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|l><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\><rsub|l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\><rsub|l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\><rsub|y><around*|(|\<b-m\>|)>.>>>>><label|eq:Wcal>
  </equation>

  \;

  Using<nbsp>(<reference|eq:W0-quadratic>), the optimality
  condition<nbsp>(<reference|eq:constrained-variational-pb-yHom>) for
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>> and
  <math|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>>> takes the form

  <\equation>
    \<b-P\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>+<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>>>>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>=\<b-0\>.<label|eq:hom-sol-optimum-pb>
  </equation>

  where

  <\equation>
    \<b-P\><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>>|<cell|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>>|<cell|\<b-0\><rsub|<nc>\<times\><nc>>>>>>>\<in\>\<bbb-T\><rsup|<around*|(|<ny>+<nc>,<ny>+<nc>|)>>.<label|eq:P-matrix>
  </equation>

  We focus on the case where <math|\<b-P\><around*|(|\<b-m\>|)>> is
  invertible: the non-invertible (rank-deficient) case is treated in
  Appendix<nbsp><reference|app:rank-deficient>. The solution
  <math|<around*|(|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>,\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>|)>>
  is then found by inverting this linear system as

  <\equation>
    <matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>=\<b-cal-R\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)><label|eq:y0-lambda0-combined>
  </equation>

  where

  <\equation>
    \<b-cal-R\><around*|(|\<b-m\>|)>=-\<b-P\><rsup|-1><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>.<label|eq:R-order0>
  </equation>

  In the code, we implemented a more general expression of <math|\<b-cal-R\>>
  that applies to rank-deficient matrices, see
  Appendix<nbsp><reference|app:rank-deficient> and
  Equation<nbsp>(<reference|eq:def-modify-R>) in particular.

  Equation<nbsp>(<reference|eq:y0-lambda0-combined>) matches the form of the
  solution <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  announced in<nbsp>(<reference|eq:y-hom-from-h>) and the localization tensor
  is identified as

  <\equation>
    \<b-Y\><rsub|0><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-cal-R\><around*|(|\<b-m\>|)>.<label|eq:localization-tensor-order0>
  </equation>

  The solution for the Lagrange multipliers is
  <math|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  where

  <\equation>
    \<b-G\><rsub|0><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><ny>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<cdot\>\<b-cal-R\><around*|(|\<b-m\>|)>.<label|eq:localization-tensor-Lambda-0>
  </equation>

  \;

  We proceed to introduce important additional quantities that characterize
  the leading-order solution.

  The strain <math|\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>=\<b-E\><rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>>
  is found using<nbsp>(<reference|eq:E-phi-0>) as

  <\equation>
    <math|\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>=\<b-F\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>,><label|eq:E-hom-decompose>
  </equation>

  where the strain localization tensor <math|\<b-F\><rsub|0><around*|(|\<b-m\>|)>>
  is given by

  <\equation>
    \<b-F\><rsub|0><around*|(|\<b-m\>|)>=\<b-E\><rsub|l><around*|(|\<b-m\>|)>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-Y\><rsub|0><around*|(|\<b-m\>|)>.<label|eq:E-localization>
  </equation>

  \;

  The leading-order strain energy <math|W<rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-m\>,\<b-l\>|)>=W<rsup|<around*|(|0|)>><around*|(|\<b-m\>,\<b-l\>,\<b-y\>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>>
  can then be written with the help of<nbsp>(<reference|eq:W0-quadratic>\U<reference|eq:Wcal>)
  and<nbsp>(<reference|eq:E-localization>) in a form that matches that
  announced in<nbsp>(<reference|eq:phi-0-in-terms-of-W-hom>), namely
  <math|W<rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-m\>,\<b-l\>|)>=<frac|1|2>*\<b-l\>\<cdot\>\<b-K\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>,>
  where the elasticity tensor <math|\<b-K\><rsub|0><around*|(|\<b-m\>|)>>
  characterizing the equivalent Cauchy-type elastic continuum at order
  <math|\<eta\><rsup|0>> is identified as

  <\equation>
    \<b-K\><rsub|0><around*|(|\<b-m\>|)>=\<b-F\><rsub|0><rsup|T><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-F\><rsub|0><around*|(|\<b-m\>|)>.<label|eq:K0-def>
  </equation>

  \;

  To complete the analysis of solutions at order <math|\<eta\><rsup|0>>, we
  derive a useful identity that will help simplify the higher orders in the
  energy expansion. Inserting the solution
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  and <math|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  into the stationarity condition<nbsp>(<reference|eq:hom-sol-optimum-pb>)
  and using<nbsp>(<reference|eq:Wcal>) and identifying <math|\<b-F\><rsub|0>>
  from<nbsp>(<reference|eq:E-localization>), we get

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
    <around*|(|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-F\><rsub|0><around*|(|\<b-m\>|)>+\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>|)>\<cdot\>\<b-l\>>|<cell|=>|<cell|\<b-0\>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>>|<cell|=>|<cell|\<b-0\>.>>>>><label|eq:Z-Lambda-Identity>
  </equation>

  The quantity <math|<around*|(|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-F\><rsub|0><around*|(|\<b-m\>|)>+\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>|)>\<cdot\>\<b-l\>>
  appearing in the first equation can be identified as the leading-order
  stress, consisting of the elastic stress
  <math|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-F\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>=\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
  \<b-E\><rsup|<around*|[|0|]>>> plus the stress
  <math|\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  enforcing the constraint. We therefore introduce the stress localization
  tensor as

  <\equation>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>=\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-F\><rsub|0><around*|(|\<b-m\>|)>+\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>,<label|eq:total-stress-S0>
  </equation>

  and rewrite Equation<nbsp>(<reference|eq:Z-Lambda-Identity>), after
  simplification by the arbitrary factor <math|\<b-l\>>, as

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-0\>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-F\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-0\>.>>>>><label|eq:PVW0>
  </equation>

  Equation<nbsp>(<reference|eq:PVW0>)<rsub|1> is the principle of virtual
  work at leading order: multiplying by a virtual displacement
  <math|\<delta\>\<b-y\>> on the left-hand side and by <math|\<b-l\>> on the
  right-hand side, and rearranging, it takes the usual form
  <math|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<delta\>\<b-y\>|)>=\<b-0\>>,
  where the left-hand side is the stress contracted with the virtual
  increment of strain.

  <appendix|Detailed analysis of the gradient
  effect><label|app:gradient-effect>

  <\big-table|<tabular|<tformat|<cwith|16|16|1|-1|cell-tborder|1ln>|<cwith|15|15|1|-1|cell-bborder|1ln>|<cwith|16|17|1|1|cell-lborder|0ln>|<cwith|16|17|5|5|cell-rborder|0ln>|<cwith|9|9|1|-1|cell-tborder|1ln>|<cwith|8|8|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<cwith|10|10|1|-1|cell-tborder|1ln>|<cwith|9|9|1|1|cell-lborder|0ln>|<cwith|9|9|5|5|cell-rborder|0ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|0ln>|<cwith|3|3|1|-1|cell-tborder|0ln>|<cwith|2|2|1|1|cell-lborder|0ln>|<cwith|2|2|5|5|cell-rborder|0ln>|<cwith|14|14|1|-1|cell-tborder|1ln>|<cwith|13|13|1|-1|cell-bborder|1ln>|<cwith|14|14|1|-1|cell-bborder|0ln>|<cwith|15|15|1|-1|cell-tborder|0ln>|<cwith|14|14|1|1|cell-lborder|0ln>|<cwith|14|14|5|5|cell-rborder|0ln>|<cwith|17|17|1|-1|cell-bborder|0ln>|<table|<row|<cell|>|<cell|tensor
  space>|<cell|symmetry>|<cell|usage>|<cell|content>>|<row|<cell|<math|\<b-cal-L\><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nh>|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\>\<cdot\>\<b-h\>>>|<cell|<math|\<b-l\>>>>|<row|<cell|<math|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,<nh>,d,<nh>|)>>>>|<cell|\U>|<cell|<math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>>>>|<row|<cell|<math|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,d,<nh>,d,<nh>,d,<nh>|)>>>>|<cell|<math|S<rsub|2\<nocomma\>3>>,
  <math|S<rsub|<around*|{|4\<nocomma\>5|}>\<nocomma\><around*|{|6\<nocomma\>7|}>>>>|<cell|<math|\<nabla\><rsup|2>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>\<otimes\>\<nabla\>\<b-m\>>>>|<row|<cell|<math|\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,d,<nh>,d,d,<nh>|)>>>>|<cell|<math|S<rsub|2\<nocomma\>3>>,
  <math|S<rsub|5\<nocomma\>6>>>|<cell|<math|\<nabla\><rsup|2>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<cdots\>>>|<cell|<math|<around*|(|\<nabla\><rsup|2>\<b-l\>,\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-m\>|)>>>>|<row|<cell|<math|<around*|\<nobracket\>|<tabular|<tformat|<table|<row|<cell|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>>>>>>|}>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nh>,d,<nh>|)>>>>|<cell|\U>|<cell|<math|\<b-E\><rsup|<around*|[|1|]>>=<around*|(|\<b-cal-J\><rsup|1>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>>>>|<row|<cell|<around*|\<nobracket\>|<tabular|<tformat|<table|<row|<cell|<math|\<b-cal-J\><rsup|1\<nocomma\>1>><around*|(|\<b-m\>|)>>>|<row|<cell|<math|\<b-cal-F\><rsup|1\<nocomma\>1>><around*|(|\<b-m\>|)>>>>>>|}>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nh>,d,<nh>,d,<nh>|)>>>>|<cell|<math|S<rsub|<around*|{|2\<nocomma\>3|}>\<nocomma\><around*|{|4\<nocomma\>5|}>>>>|<cell|<math|\<b-E\><rsup|<around*|[|2|]>>=<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>\<otimes\>\<nabla\>\<b-m\>>>>|<row|<cell|<around*|\<nobracket\>|<tabular|<tformat|<table|<row|<cell|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>>>>>>|}>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nh>,d,d,<nh>|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<b-E\><rsup|<around*|[|2|]>>=<around*|(|\<b-cal-J\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<cdots\>>>|<cell|<math|<around*|(|\<nabla\><rsup|2>\<b-l\>,\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-m\>|)>>>>|<row|<cell|<math|\<b-cal-A\><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,<nh>,<nh>|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<Phi\><rsub|<around*|[|1|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-A\><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc2>\<nabla\>\<b-h\>*\<mathd\>\<b-X\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>>>>|<row|<cell|<math|<tabular|<tformat|<table|<row|<cell|<deleted|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>>>>>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,<nh>,d,<nh>,<nh>|)>>>>|<cell|(delayed)>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-B\><rsup|<around*|(|0|)>><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)><rsup|\<otimes\>2>>>>|<row|<cell|<around*|\<nobracket\>|<math|<tabular|<tformat|<table|<row|<cell|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>>|<row|<cell|\<Delta\>\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>>>>>>|}>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,<ny>,<nh>|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-B\><rsup|<around*|(|1|)>>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-y\><rsub|1>|)>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>\<otimes\>\<b-y\><rsub|1>>>>|<row|<cell|<math|<deleted|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,d,<nh>,<nh>|)>>>>|<cell|(delayed)>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|0|)>><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|\<b-l\>\<otimes\><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-m\>,\<nabla\><rsup|2>\<b-l\>|)>>>>|<row|<cell|<math|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,<nh>|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-y\><rsub|1>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|\<b-l\>\<otimes\>\<nabla\>\<b-y\><rsub|1>>>>|<row|<cell|<math|\<b-cal-B\><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,<nh>,d,<nh>,<nh>|)>>>>|<cell|<math|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>>,
  <math|S<rsub|5\<nocomma\>6>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-B\><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc2><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)><rsup|\<otimes\>2>>>>|<row|<cell|<math|\<b-cal-C\><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nh>,d,d,<nh>,<nh>|)>>>>|<cell|<math|S<rsub|2\<nocomma\>3>>,
  <math|S<rsub|4\<nocomma\>5>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>*\<mathd\>\<b-X\>+\<cdots\>>>|<cell|<math|\<b-l\>\<otimes\><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-m\>,\<nabla\><rsup|2>\<b-l\>|)>>>>|<row|<cell|<math|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nh>,d,<nh>|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>=<around*|(|\<b-cal-Y\><rprime|'>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>>>>|<row|<cell|<math|\<b-cal-G\><rprime|'><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nc>,<nh>,d,<nh>|)>>>>|<cell|\U>|<cell|<math|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>>=<around*|(|\<b-cal-G\><rprime|'>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>>|<cell|<math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>>>>>>>>
    Summary of the tensors used internally by the homogenization procedure in
    Appendix<nbsp><reference|app:gradient-effect>. All these tensors make use
    of the compact <math|\<b-h\>> notation,
    see<nbsp>(<reference|eq:def-h>).<label|tab:internal-tensors>
  </big-table>

  <subsection|Packed macroscopic variables><label|ssec:packed-h>

  For the analysis of the gradient effect, it is convenient to introduce the
  quantity <math|\<b-h\><around*|(|\<b-X\>|)>> obtained by concatenating the
  microscopic variables <math|\<b-l\>> and <math|\<b-m\>> together with a
  trailing 1: in block-vector notation,

  <\equation>
    \<b-h\><around*|(|\<b-X\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-l\><around*|(|\<b-X\>|)>>|<cell|\<b-m\><around*|(|\<b-X\>|)>>|<cell|<around*|(|1|)>>>>>>\<in\>\<bbb-R\><rsup|<nh>><text|<space|1em>where
    <math|<nh>=<nl>+<nm>+1>.><label|eq:def-h>
  </equation>

  With the help of the matrices <math|\<b-cal-V\><rsup|l>\<in\>\<bbb-T\><rsup|<around*|(|<nl>,<nh>|)>>>,
  <math|\<b-cal-V\><rsup|m>\<in\>\<bbb-T\><rsup|<around*|(|<nm>,<nh>|)>>> and
  the vector <math|\<b-cal-V\><rsup|1>\<in\>\<bbb-R\><rsup|<nh>>> defined in
  block-matrix notation by

  <\equation>
    \<b-cal-V\><rsup|l>=<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<nl>>>|<cell|\<b-0\><rsub|<nl>\<times\><around*|(|<nm>+1|)>>>>>>>,<separating-space|2em>\<b-cal-V\><rsup|m>=<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nm>\<times\><nl>>>|<cell|\<b-I\><rsub|<nm>>>|<cell|\<b-0\><rsub|<nm>\<times\>1>>>>>>,<separating-space|2em>\<b-cal-V\><rsup|1>=<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<around*|(|<nl>+<nm>|)>>>|<cell|1>>>>>,<label|eq:Vs>
  </equation>

  on can rewrite the definition of <math|\<b-h\>>
  in<nbsp>(<reference|eq:def-h>) as

  <\equation>
    \<b-h\><around*|(|\<b-X\>|)>=<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>+<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<cdot\>\<b-m\><around*|(|\<b-X\>|)>+\<b-cal-V\><rsup|1>.<label|eq:lm-to-h>
  </equation>

  The converse (unpacking) operation is implemented as

  <\equation>
    \<b-l\><around*|(|\<b-X\>|)>=\<b-cal-V\><rsup|l>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>,<separating-space|2em>\<b-m\><around*|(|\<b-X\>|)>=\<b-cal-V\><rsup|m>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>.<label|eq:h-unpack>
  </equation>

  <subsection|Structure coefficients>

  The leading-order prediction<nbsp>(<reference|eq-y-order-by-order>)<rsub|1>
  for the microscopic degrees of freedom,
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>>
  can be expressed in terms of <math|\<b-h\><around*|(|\<b-X\>|)>> with the
  help of<nbsp>(<reference|eq:h-unpack>) as

  <\equation>
    \<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)><text|<space|1em>where
    >\<b-cal-L\><around*|(|\<b-m\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>.<label|eq:L-calligraphic-operator>
  </equation>

  The successive gradients of <math|\<b-y\><around*|(|\<b-X\>|)>>
  in<nbsp>(<reference|eq:y-expansion-leading-order-known>) can then be
  calculated as

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-y\><around*|(|\<b-X\>|)>>|<cell|=>|<cell|\<b-cal-L\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|2|]>><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>>>|<row|<cell|\<nabla\>\<b-y\><around*|(|\<b-X\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>+\<nabla\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>>>|<row|<cell|\<nabla\><rsup|2>\<b-y\><around*|(|\<b-X\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc4><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>|)>+<around*|(|\<b-cal-L\><rsup|2><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>>>>>><label|eq:y-expand>
  </equation>

  where the symbol <math|\<cal-O\><around*|(|\<eta\><rsup|3>|)>> stands for
  terms of order <math|\<eta\><rsup|3>> and higher, such as
  <math|\<nabla\><rsup|2>\<b-y\><rsub|<around*|[|1|]>>=\<cal-O\><around*|(|\<eta\><rsup|2+1>|)>>
  and <math|\<nabla\>\<b-y\><rsub|<around*|[|2|]>>=\<cal-O\><around*|(|\<eta\><rsup|1+2>|)>>.

  By design, the tensor <math|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>>,
  <math|><math|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>> and
  <math|\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>> capture the successive
  gradients of <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>> as
  <math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>
  and <math|\<nabla\><rsup|2>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\><rsup|2>\<b-h\>|)>>.
  They are identified by differentiating<nbsp>(<reference|eq:L-calligraphic-operator>)
  with respect to <math|\<b-X\>>, which yields

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<frac|\<mathd\>\<b-cal-L\>|\<mathd\>\<b-m\>>\<cdot\>\<b-cal-V\><rsup|m>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>3\<nocomma\>2\<nocomma\>4>>+<around*|(|\<b-cal-L\><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>5\<nocomma\>2\<nocomma\>4>>>>|<row|<cell|\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<around*|(|<frac|\<mathd\>\<b-cal-L\><rsup|1>|\<mathd\>\<b-m\>>\<cdot\>\<b-cal-V\><rsup|m>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>6\<nocomma\>7\<nocomma\>8\<nocomma\>4\<nocomma\>3\<nocomma\>5>>+<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>4\<nocomma\>5\<nocomma\>6\<nocomma\>8\<nocomma\>3\<nocomma\>7>>|]><rsup|S<rsub|2\<nocomma\>3>\<circ\>S<rsub|<around*|{|4\<nocomma\>5|}>\<nocomma\><around*|{|6\<nocomma\>7|}>>>>>|<row|<cell|\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>4\<nocomma\>5\<nocomma\>7\<nocomma\>3\<nocomma\>6>>|]><rsup|S<rsub|2\<nocomma\>3>\<circ\>S<rsub|5\<nocomma\>6>>>>>>><label|eq:cal-L-operators>
  </equation>

  Table<nbsp><reference|tab:internal-tensors> lists the properties of all the
  tensors used in this appendix, starting with the tensors
  <math|\<b-cal-L\>>, <math|\<b-cal-L\><rsup|1>>,
  <math|\<b-cal-L\><rsup|1\<nocomma\>1>> and <math|\<b-cal-L\><rsup|2>> just
  defined.

  The symmetrization operations outside the square brackets
  in<nbsp>(<reference|eq:cal-L-operators>)<rsub|2,3> are a matter of
  convention. They reflect the symmetries of the tensors with which the
  operators <math|\<b-cal-L\>> are contracted.

  The \<#2018\>content\<#2019\> column in
  Table<nbsp><reference|tab:internal-tensors> can be explained as follows. By
  design, <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>>
  can be \<#2018\>unpacked\<#2019\> (<em|i.e.>, expressed in terms of
  <math|\<b-l\>> and <math|\<b-m\>>) as <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>,
  which is a function of <math|\<b-m\>> contracted with <math|\<b-l\>>: the
  dependence on <math|\<b-m\>> will be treated implicitly, and we express
  this by writing that <math-it|the content of
  <math|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>>
  is <math|\<b-l\>>>, hence the symbol <math|\<b-l\>> appearing in the
  \<#2018\>content\<#2019\> column for the row <math|\<b-cal-L\>>. Similarly,
  <math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>
  can be unpacked as <math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<nabla\><around*|(|\<b-Y\><rsub|0><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>|)>=<around*|(|<frac|\<mathd\>\<b-Y\><rsub|0>|\<mathd\>\<b-m\>>|)><rsub|y\<nocomma\>l,m>*\<b-l\><rsub|l>*\<nabla\>\<b-m\><rsub|m,a>+<around*|(|\<b-Y\><rsub|0>|)><rsub|y\<nocomma\>l>*\<nabla\>\<b-l\><rsub|l,a>=<frac|\<mathd\>\<b-Y\><rsub|0>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>|)>+\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<nabla\>\<b-l\>>:
  this is the sum of two terms, one being a function of <math|\<b-m\>>
  contracted with <math|\<b-l\>\<otimes\>\<nabla\>\<b-m\>>, the other one
  being a function of <math|\<b-m\>> contracted with <math|\<nabla\>\<b-l\>>:
  this is conveyed by the <em|content> column in the table, which shows
  <math|<around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-m\>,\<nabla\>\<b-l\>|)>> for
  the row labelled <math|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>> used for
  reconstructing <math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=<around*|(|\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>.

  The point of the <math|\<b-h\>> notation is to deal in a simple way with
  the multiplicity of terms appearing in the last column of
  Table<nbsp><reference|tab:input>. The remainder of the appendix will make
  use of this higher-level <math|\<b-h\>> notation. On the other hand, we use
  Table<nbsp><reference|tab:input> to keep track of the actual content of the
  various tensors.

  Now, we proceed to represent the strain in terms of <math|\<b-h\>> and its
  successive gradients. Inserting<nbsp>(<reference|eq:y-expand>) into the
  strain expression <math|\<b-E\>> in<nbsp>(<reference|eq:strain-canonical-form>),
  identifying <math|<math|\<b-E\><rsup|<around*|[|0|]>>><around*|(|\<b-m\>,\<b-l\>|)>>
  using<nbsp>(<reference|eq:E-hom-decompose>), and rearranging the other
  terms, we get

  <\equation>
    <tabular|<tformat|<cwith|2|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-E\>=<math|\<b-E\><rsup|<around*|[|0|]>>><around*|(|\<b-m\>,\<b-l\>|)>+<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>+<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc4><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>|)>>>|<row|<cell|<space|7em>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\><around*|(|\<b-X\>|)>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\><around*|(|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|2|]>><around*|(|\<b-X\>|)>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>>>>>><label|eq:E-alpha-using-structure-coefs>
  </equation>

  where the so-called structure coefficients are identified by

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<cwith|3|3|1|1|cell-halign|r>|<cwith|2|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-E\><rsub|l><rprime|'><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<b-cal-L\><rsup|1><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)><tc3>\<b-cal-L\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-E\><rsub|l><rprime|''><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>3>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>4\<nocomma\>2\<nocomma\>5>>+\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)><tc3>\<b-cal-L\><rsup|2><around*|(|\<b-m\>|)>>>>>><label|eq:structure-coefficients>
  </equation>

  As indicated in Table<nbsp><reference|tab:input>,
  <math|\<b-cal-J\><rsup|1\<nocomma\>1>> is
  <math|S<rsub|<around*|{|2\<nocomma\>3|}>\<nocomma\><around*|{|4\<nocomma\>5|}>>>-symmetric:
  this is a consequence of the fact that <math|\<b-cal-L\><rsup|1\<nocomma\>1>>
  is <math|S<rsub|<around*|{|4\<nocomma\>5|}>\<nocomma\><around*|{|6\<nocomma\>7|}>>>-symmetric.
  The <math|S<rsub|3\<nocomma\>4>>-symmetry of <math|\<b-cal-J\><rsup|2>> can
  be justified by a similar argument.

  Grouping the terms order by order, we can rewrite the strain
  in<nbsp>(<reference|eq:E-alpha-using-structure-coefs>) as

  <\equation>
    \<b-E\>=\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>+\<b-E\><rsup|<around*|[|1|]>>+\<b-E\><rsup|<around*|[|2|]>>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>,<label|eq:E-alpha-expansion-nearly-done>
  </equation>

  where the contributions <math|\<b-E\><rsup|<around*|[|1|]>>=\<cal-O\><around*|(|\<eta\>|)>>
  and <math|\<b-E\><rsup|<around*|[|2|]>>=\<cal-O\><around*|(|\<eta\><rsup|2>|)>>
  are given, respectively, by

  <\equation>
    <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<cwith|2|2|3|3|cell-background|>|<table|<row|<cell|\<b-E\><rsup|<around*|[|1|]>>>|<cell|=>|<cell|<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\><rsub|<around*|[|1|]>>>>|<row|<cell|\<b-E\><rsup|<around*|[|2|]>>>|<cell|=>|<cell|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>.>>>>><label|eq:E-alpha-expansion-nearly-done-components>
  </equation>

  \;

  Inserting<nbsp>(<reference|eq:E-alpha-expansion-nearly-done>\U<reference|eq:E-alpha-expansion-nearly-done-components>)
  into the kinematic constraint <math|\<b-cal-Q\>\<cdot\>\<b-E\>=\<b-0\>>,
  see<nbsp>(<reference|eq:QEisZero>), we obtain the expression of the
  constraint order by order in <math|\<eta\>> as

  <\equation>
    \<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|i|]>>=\<b-0\><text|<space|2em>for
    <math|i=0,1,2,\<ldots\>>><label|eq:Q-yi-is-zero>
  </equation>

  The constraint <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>=\<b-0\>>
  has been enforced during the solution of the leading order,
  see<nbsp>(<reference|eq:Z-Lambda-Identity>)<rsub|2>.

  <subsection|Strain energy expansion in terms of corrective
  displacement><label|ssec:elim-yi>

  Inserting<nbsp>(<reference|eq:E-alpha-expansion-nearly-done>) into the
  energy<nbsp>(<reference|eq:phi-canonical>), and using the quadratic
  expression<nbsp>(<reference|eq:abstract-energy-density>) of the energy
  density, we obtain a Taylor expansion of the energy as

  <\equation*>
    \<Phi\>=<big|int><rsub|\<Omega\>><around*|(|<frac|1|2>*\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>+<around*|(|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>|)>\<cdot\><around*|(|\<b-E\><rsup|<around*|[|1|]>>+\<b-E\><rsup|<around*|[|2|]>>|)>+<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>+\<cal-O\><around*|(|\<eta\><rsup|3>|)>|)>*\<mathd\>\<b-X\>.
  </equation*>

  Grouping the terms in the integrand order by order, identifying the term of
  order <math|\<eta\><rsup|0>> as <math|<frac|1|2>*\<b-l\>\<cdot\>\<b-K\><rsub|0>\<cdot\>\<b-l\>>
  using<nbsp>(<reference|eq:K0-def>), and the quantity
  <math|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|0|]>><around*|(|\<b-m\>,\<b-l\>|)>=\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-F\><rsup|<around*|[|0|]>><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>=<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>-\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>|)>\<cdot\>\<b-l\>>
  by<nbsp>(<reference|eq:E-hom-decompose>)
  and<nbsp>(<reference|eq:total-stress-S0>), we obtain the energy expansion
  as

  <\equation>
    \<Phi\>=\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>+\<Phi\><rsup|><rsub|<around*|[|1|]>>+\<Phi\><rsup|><rsub|<around*|[|2|]>>+\<cdots\><label|energyexpansion>
  </equation>

  where <math|\<Phi\><rsup|><rsub|<around*|[|i|]>>=\<cal-O\><around*|(|\<eta\><rsup|i>|)>>
  are the successive terms in the expansion, the leading term is the quantity
  <math|\<Phi\><rprime|\<star\>><rsub|<around*|[|0|]>><around*|[|\<b-m\>,\<b-l\>|]>>
  identified earlier in<nbsp>(<reference|eq:phi-0-in-terms-of-W-hom>) and the
  higher-order terms are given by

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<Phi\><rsub|<around*|[|1|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>-\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>*\<mathd\>\<b-X\>>>|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>+<around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>-\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>|)>*\<mathd\>\<b-X\>.>>>>>
  </equation>

  \;

  The <math|\<b-cal-Q\><rsup|T>> terms appearing in both bulk integrals can
  be removed, as <math|-<around*|(|\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|i|]>>=-<around*|(|\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|i|]>>|)>=\<b-0\>>
  by<nbsp>(<reference|eq:Q-yi-is-zero>). This yields

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<Phi\><rsub|<around*|[|1|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>*\<mathd\>\<b-X\>>>|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>+<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>|)>*\<mathd\>\<b-X\>.>>>>><label|eq:phi1-phi2-tmp><rsup|>
  </equation>

  \;

  Inserting the expression of <math|\<b-E\><rsup|<around*|[|1|]>>>
  from<nbsp>(<reference|eq:E-alpha-expansion-nearly-done-components>)<rsub|1>
  in <math|\<Phi\><rsub|<around*|[|1|]>>> and using the principle of virtual
  work at dominant order in<nbsp>(<reference|eq:PVW0>)<rsub|1>, we have

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<Phi\><rsub|<around*|[|1|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\><rsub|<around*|[|1|]>>|)>*\<mathd\>\<b-X\>>>|<row|<cell|>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>+<around*|(|<around*|[|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>\<b-S\><rsub|0><around*|(|\<b-m\>|)>|]>\<cdot\>\<b-l\>|)>\<cdot\>\<b-y\><rsub|<around*|[|1|]>>|)>*\<mathd\>\<b-X\>>>|<row|<cell|>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>*\<mathd\>\<b-X\>>>>>><label|eq:Phi1-eliminate-y1>
  </equation>

  Inserting the expression of <math|\<b-E\><rsup|<around*|[|2|]>>>
  from<nbsp>(<reference|eq:E-alpha-expansion-nearly-done-components>)<rsub|2>
  in the expression of <math|\<Phi\><rsub|<around*|[|2|]>>>
  in<nbsp>(<reference|eq:phi1-phi2-tmp>), we have

  <\equation>
    \<Phi\><rsup|><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<table|<row|<cell|<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>>>|<row|<cell|<separating-space|1em><around*|\<nobracket\>||\<nobracket\>>+<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>|)>>>|<row|<cell|<space|1em>+<around*|(|<around*|[|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>\<b-S\><rsub|0><around*|(|\<b-m\>|)>|]>\<cdot\>\<b-l\>|)>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>>>>>>|)>*\<mathd\>\<b-X\>
  </equation>

  Equation<nbsp>(<reference|eq:PVW0>)<rsub|1> shows that the term on the
  third line is zero, by the same argument as
  in<nbsp>(<reference|eq:Phi1-eliminate-y1>). This yields

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<Phi\><rsub|<around*|[|1|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>*\<mathd\>\<b-X\>>>|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<table|<row|<cell|<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>>>|<row|<cell|<separating-space|1em><around*|\<nobracket\>||\<nobracket\>>+<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>|)>>>>>>|)>*\<mathd\>\<b-X\>.>>>>><label|eq:Phi12-yi-eliminated>
  </equation>

  <subsection|Correction at order <math|\<eta\>>><label|ssec:extract-A0-K1>

  We can rewrite (<reference|eq:Phi12-yi-eliminated>)<rsub|1> in terms of the
  packed macroscopic variable <math|\<b-h\>> as

  <\equation>
    \<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-A\><around*|(|\<b-m\>|)><tc2>\<b-h\>\<otimes\>\<b-h\>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>,<label|eq:phi-1-just-A>
  </equation>

  where

  <\equation>
    \<b-cal-A\><around*|(|\<b-m\>|)>=<around*|<left|[|2>|<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>|)><rsup|T<rsub|4\<nocomma\>1\<nocomma\>2\<nocomma\>3>>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>|<right|]|2>><rsup|S<rsub|3\<nocomma\>4>>.<label|eq:W1-A0-def>
  </equation>

  In<nbsp>(<reference|eq:phi-1-just-A>), we use the star notation
  <math|\<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>> to emphasize that the
  right-hand side no longer depends on the unknown corrector
  <math|\<b-y\><rsub|<around*|[|1|]>>> and only depends on the macroscopic
  fields <math|\<b-m\>> and <math|\<b-h\>>, thanks to the elimination of the
  unknown corrector <math|\<b-y\><rsub|<around*|[|1|]>>> done earlier
  in<nbsp>(<reference|eq:Phi1-eliminate-y1>).

  As announced in Table<nbsp><reference|tab:internal-tensors>, the content of
  <math|\<b-cal-A\><around*|(|\<b-m\>|)>> is
  <math|\<b-l\>\<otimes\>\<b-l\>\<otimes\>\<nabla\>\<b-m\>> and
  <math|\<b-l\>\<otimes\>\<nabla\>\<b-l\>>, which means that the right-hand
  side of Equation<nbsp>(<reference|eq:phi-1-just-A>) can be unpacked
  using<nbsp>(<reference|eq:lm-to-h>) as

  <\equation>
    \<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|\<b-A\><rsub|0><around*|(|\<b-m\>|)><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>+<around*|(|\<b-K\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>|)>*\<mathd\>\<b-X\>,<label|eq:A-unpacked>
  </equation>

  where the tensors <math|\<b-A\><rsub|0><around*|(|\<b-m\>|)>> and
  <math|\<b-K\><rsub|1><around*|(|\<b-m\>|)>> are extracted from
  <math|\<b-cal-A\><around*|(|\<b-m\>|)>> as

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-A\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|[|\<b-cal-A\><rsup|T<rsub|2\<nocomma\>1\<nocomma\>3\<nocomma\>4>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3>>|]><rsup|T<rsub|3\<nocomma\>2\<nocomma\>1>>>>|<row|<cell|\<b-K\><rsub|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|[|\<b-cal-A\><rsup|T<rsub|2\<nocomma\>1\<nocomma\>3\<nocomma\>4>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>>|]><rsup|T<rsub|4\<nocomma\>3\<nocomma\>1\<nocomma\>2>>>>>>><label|eq:A0-K1>
  </equation>

  No other term can be present in the right-hand side
  of<nbsp>(<reference|eq:A-unpacked>): a term such as
  <math|\<b-D\><rsub|0><around*|(|\<b-m\>|)><tc4><around*|(|\<b-l\>\<otimes\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>>,
  for instance, would be inconsistent with the fact that the energy is
  homogeneous with degree 2 in <math|\<b-l\>> and <math|\<nabla\>\<b-l\>>,
  see<nbsp>(<reference|eq:strain-canonical-form>)
  and<nbsp>(<reference|eq:abstract-energy-density>).

  The unpacking in<nbsp>(<reference|eq:A0-K1>) removes any reference to the
  packed vector <math|\<b-h\>> used internally in this Appendix, in favor of
  <math|\<b-l\>> and <math|\<b-m\>>.

  The properties of the tensors <math|\<b-A\><rsub|0><around*|(|\<b-m\>|)>>
  and <math|\<b-K\><rsub|1><around*|(|\<b-m\>|)>> are listed in
  Table<nbsp><reference|tab:tensors-delivered>.

  The expression of the first correction <math|\<Phi\><rsub|<around*|[|1|]>><rprime|\<star\>>>
  to the energy in<nbsp>(<reference|eq:A-unpacked>) was announced
  in<nbsp>(<reference|eq:Phi-1-form>).

  <subsection|Functional governing corrector><label|ssec:elim-y2>

  We now turn attention to the correction
  <math|\<Phi\><rsub|<around*|[|2|]>>> in<nbsp>(<reference|eq:Phi12-yi-eliminated>)<rsub|2>.
  Inserting the expression of <math|\<b-E\><rsup|<around*|[|1|]>>>
  in<nbsp>(<reference|eq:E-alpha-expansion-nearly-done-components>)<rsub|1>,
  we get after rearranging the terms

  <\equation>
    \<Phi\><rsup|><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|<around*|(|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>+<around*|(|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>>>|<row|<cell|<space|2em>+<around*|(|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|)>+\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)><tc2><frac|\<b-y\><rsub|<around*|[|1|]>>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|2>+<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><rsup|><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>>>>>>|)>*\<mathd\>\<b-X\>,<label|eq:phi2-tmp2>
  </equation>

  where <math|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>> is the
  tensor introduced in the analysis of the leading order,
  see<nbsp>(<reference|eq:Wcal>)<rsub|3>, and<todo|operators
  <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>
  and <math|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>> are no
  longer required: remove from this documentation and from the code?>

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|3|3|1|1|cell-halign|r>|<cwith|1|3|1|1|cell-halign|r>|<cwith|1|2|3|3|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<table|<row|<cell|<deleted|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>>|<cell|=>|<cell|<around*|<left|(|1>|<around*|(|\<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|><rsup|T<rsub|412\<nocomma\>3>>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|<right|)|1>><rsup|T<rsub|1\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>4\<nocomma\>6>>>|<cell|+>|<cell|2*<around*|(|\<b-cal-J\><rsup|\<nocomma\>1\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|T<rsub|61234\<nocomma\>5>>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>>>|<row|<cell|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|<left|(|2>|<around*|(|\<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|><rsup|T<rsub|412\<nocomma\>3>>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\><rsub|y><around*|(|\<b-m\>|)>|<right|)|2>><rsup|T<rsub|1\<nocomma\>2\<nocomma\>4\<nocomma\>3>>>|<cell|>|<cell|>>|<row|<cell|<deleted|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>>|<cell|=>|<cell|>|<cell|>|<cell|<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>|)><rsup|T<rsub|5123\<nocomma\>4>>\<cdot\>
    \ \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>>>|<row|<cell|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|>|<cell|>|<cell|<around*|(|\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)>|)><rsup|T<rsub|312>>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>.>>>>><label|eq:Kt2-At1-Ft1-etc-tmp>
  </equation>

  As indicated by the \<#2018\>delayed\<#2019\> keyword in
  Table<nbsp><reference|tab:internal-tensors>, we do not yet enforce the
  natural symmetries of <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>>>,
  which reflect the symmetries of the tensor
  <math|<frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>\<otimes\>\<b-h\>\<otimes\>\<b-h\>>
  with which it gets contracted: they will be enforced later on the children
  of <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>>>. The same
  holds for the tensor <math|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>,
  whose natural <math|S<rsub|2\<nocomma\>3>> and <math|S<rsub|4\<nocomma\>5>>
  symmetries are ignored for the moment.

  In the code, we implemented an extension
  of<nbsp>(<reference|eq:Kt2-At1-Ft1-etc-tmp>) that covers the rank-deficient
  case as well, see Equation<nbsp>(<reference|eq:deficient-Bcirc-C>) in
  Appendix<nbsp><reference|app:rank-deficient>.

  Note that <math|\<Phi\><rsub|<around*|[|2|]>>> no longer depends on
  <math|\<b-y\><rsub|<around*|[|2|]>>> thanks to the work done in
  Section<nbsp><reference|ssec:elim-yi>. It still depends on
  <math|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>> and its gradient,
  however. We proceed to remove the dependence on the gradient
  <math|\<nabla\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>> by
  integrating by parts.

  <subsection|Integration by parts><label|ssec:ibp>

  The <math|\<b-cal-C\><rsup|<around*|(|1|)>><rsup|>> term appearing
  in<nbsp>(<reference|eq:phi2-tmp2>) can be integrated by parts as

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>=>>|<row|<cell|<around*|\<nobracket\>||\<nobracket\>><space|8em><big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2><around*|(|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>\<otimes\>\<b-n\><around*|(|\<b-X\>|)>|)>*\<mathd\>a>>|<row|<cell|<space|8em><space|2em>+<big|int><rsub|\<Omega\>><around*|(|\<Delta\>\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>|)>*\<mathd\>\<b-X\>>>>>><label|eq:C1-ibp>
  </equation>

  where

  <\equation>
    \<Delta\>\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>=-<around*|(|<frac|\<mathd\>\<b-cal-C\><rsup|<around*|(|1|)>>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>|)><rsup|T<rsub|3\<nocomma\>2\<nocomma\>4\<nocomma\>1\<nocomma\>>>-<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>|)><rsup|T<rsub|3\<nocomma\>2\<nocomma\>1>>\<otimes\>\<b-cal-V\><rsup|1>,<label|eq:delta-B>
  </equation>

  <\remark>
    We no longer integrate the <math|\<b-cal-C\><rsup|<around*|(|0|)>>> term
    by parts as we did in the JTCAM 2023 paper, as this breaks convexity.
  </remark>

  Inserting<nbsp>(<reference|eq:C1-ibp>) into<nbsp>(<reference|eq:phi2-tmp2>),
  we have

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>>>|<cell|=>|<cell|<big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2><around*|(|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>\<otimes\>\<b-n\><around*|(|\<b-X\>|)>|)>*\<mathd\>a>>|<row|<cell|>|<cell|>|<cell|<space|2em>+<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)><tc2><frac|\<b-y\><rsub|<around*|[|1|]>>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|2>+<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-y\><rsub|<around*|[|1|]>>|)>>>|<row|<cell|<space|2em>+<around*|(|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>+<around*|(|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>>>>>>|)>*\<mathd\>\<b-X\>>>>>><label|eq:Phi-2-after-ibp>
  </equation>

  where

  <\equation>
    \<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>=<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>+\<Delta\>\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>.<label|eq:Att-F-Bparenth>
  </equation>

  Equation<nbsp>(<reference|eq:Phi-2-after-ibp>) has been announced
  in<nbsp>(<reference|eq:split-phi-2>\U<reference|eq:PhiExpansionAnnounce>).

  <subsection|Optimal corrective displacement><label|sec:app-optimal-corrective-displacement>

  The integrand of <math|\<Phi\><rsub|<around*|[|2|]>>>
  in<nbsp>(<reference|eq:Phi-2-after-ibp>) depends on
  <math|\<b-y\><rsub|<around*|[|1|]>>> but not on its gradient, thanks to the
  integration by parts, �<reference|ssec:ibp>. It does not depend on
  <math|\<b-y\><rsub|<around*|[|2|]>>> either.

  The variational problem<nbsp>(<reference|eq:stationarity-phi2>) for
  <math|\<b-y\><rsub|<around*|[|1|]>>> is therefore local and we proceed to
  solve it. The functional <math|\<Phi\><rsub|<around*|[|2|]>>> is given
  in<nbsp>(<reference|eq:Phi-2-after-ibp>) as the sum of a bulk integral and
  a boundary integral. The variational problem<nbsp>(<reference|eq:stationarity-phi2>)
  therefore yields two sets of local conditions:

  <\itemize>
    <item>at any point <math|\<b-X\>\<in\>\<partial\>\<Omega\>> <em|on the
    boundary>, the increment <math|<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2><around*|(|\<delta\>\<b-y\>\<otimes\>\<b-n\>|)>>
    coming from the boundary integral should vanish for any perturbation
    <math|\<delta\>\<b-y\>> satisfying the incremental constraint
    <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<delta\>\<b-y\>=\<b-0\>>;
    using a Lagrange multiplier <math|<math|<wide|\<b-g\>|~>>>, we must solve
    <math|<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2><around*|(|\<delta\><wide|\<b-y\>|~>\<otimes\>\<b-n\>|)>+<wide|\<b-g\>|~>\<cdot\>\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<delta\><wide|\<b-y\>|~>=\<b-0\>>
    for any <math|\<delta\><wide|\<b-y\>|~>\<in\>\<bbb-R\><rsup|<ny>>>.
    Eliminating the virtual quantity <math|\<delta\><wide|\<b-y\>|~>>, we can
    rewrite this as <math|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-n\>\<otimes\>\<b-h\>|)>+<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>\<cdot\><wide|\<b-g\>|~>=\<b-0\>>.
    A solution <math|<wide|\<b-g\>|~>> exists if and only if the vector
    <math|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-n\>\<otimes\>\<b-h\>|)>>
    is contained in the image of the operator
    <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>.
    Inserting the expression of <math|\<b-h\>>
    in<nbsp>(<reference|eq:lm-to-h>), we therefore obtain the stationarity
    condition in the form<folded-comment|+2FxRM9JM5NgrCAb|+1dKAkUcD5u47Tyr|comment|System
    Administrator|1680694497||We are forgetting the (non-incremental)
    condition <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-y\>+\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<ldots\>=\<b-0\>>
    since it is degenerate, and we will choose the value of <math|\<b-y\>> by
    continuity with the solution in the interior
    <math|\<Omega\><rsup|\<circ\>>>>

    <\equation>
      \<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)><tc2><around*|(|\<b-n\>\<otimes\><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-l\>+<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<cdot\>\<b-m\>+\<b-cal-V\><rsup|1>|)>|)>\<in\>Im<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>.<label|eq:condition-on-boundary>
    </equation>

    <item>at any point <math|\<b-X\>\<in\>\<Omega\><rsup|\<circ\>>> <em|in
    the interior> of the domain <math|\<Omega\>>, the problem for
    <math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
    is a quadratic optimization problem with a linear constraint. Using a
    Lagrange multiplier <math|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>,
    its solution <math|<around*|(|<wide|\<b-y\>|~>,<wide|\<b-g\>|~>|)>=<around*|(|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>,\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>|)>>
    must satisfy

    <\equation>
      <around*|{|<tabular|<tformat|<table|<row|<cell|\<b-cal-Q\>\<cdot\><around*|(|\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\><wide|\<b-y\>|~>+<around*|(|\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>|)>=\<b-0\>>>|<row|<cell|\<b-cal-W\><rsub|y<separating-space|0.2em>y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2><around*|(|<wide|\<b-y\>|~>\<otimes\>\<delta\><wide|\<b-y\>|~>|)>+<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<delta\><wide|\<b-y\>|~>|)>+<wide|\<b-g\>|~>\<cdot\>\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<delta\><wide|\<b-y\>|~>=\<b-0\>,<separating-space|1em><around*|(|\<forall\>\<delta\><wide|\<b-y\>|~>\<in\>\<bbb-R\><rsup|<ny>>|)>.>>>>>|\<nobracket\>><label|eq:optim-problem-for-y1>
    </equation>
  </itemize>

  \;

  We take note of the stationarity condition<nbsp>(<reference|eq:condition-on-boundary>)
  on the boundary, which we will address in future work, and proceed to solve
  the problem<nbsp>(<reference|eq:optim-problem-for-y1>) in the interior. The
  latter can be rewritten in matrix form as<folded-comment|+2FxRM9JM5NgrCAd|+1dKAkUcD5u47Tyv|comment|System
  Administrator|1680695081||Revisit this as a principle of virtual work at
  order 1, try identifying some <math|\<b-F\><rsub|1>>,
  <math|\<b-S\><rsub|1>>?>

  <\equation>
    \<b-P\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>+<matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>|)><rsup|T<rsub|231\<nocomma\>4>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>>>><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>=\<b-0\>.<label|eq:pb-for-optimal-displacement>
  </equation>

  The matrix <math|\<b-P\>> in the left-hand side above is identical to the
  one which appeared in the analysis of the leading order,
  see<nbsp>(<reference|eq:hom-sol-optimum-pb>).

  The solution <math|<around*|(|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>,\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>|)>>
  is obtained by inverting <math|\<b-P\>> (the case where <math|\<b-P\>> is
  non-invertible is treated in Appendix<nbsp><reference|app:rank-deficient>).
  This yields the correction <math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  to the microscopic degrees of freedom in form

  <\equation>
    \<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>,<label|eq:corrector-packed>
  </equation>

  with the localization tensor given by

  <\equation>
    \<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-cal-R\><rprime|'><around*|(|\<b-m\>|)><label|eq:implement-P-R>
  </equation>

  where

  <\equation>
    \<b-cal-R\><rprime|'><around*|(|\<b-m\>|)>=-\<b-P\><rsup|-1><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>|)><rsup|T<rsub|231\<nocomma\>4>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>>>>>.<label|eq:RforY0prime>
  </equation>

  Compared to the leading-order in<nbsp>(<reference|eq:R-order0>), only the
  second factor in the right-hand side has changed. To handle the
  rank-deficient case, we implement an extension
  of<nbsp>(<reference|eq:RforY0prime>) that uses the pseudo-inverse rather
  than the inverse, see Equation<nbsp>(<reference|eq:def-Rprime>) in
  Appendix<nbsp><reference|app:rank-deficient>.

  The Lagrange multiplier is given as a byproduct as

  <\equation>
    \<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>=<around*|(|\<b-cal-G\><rprime|'><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>\<cdot\>\<b-h\><around*|(|\<b-X\>|)>|)><tc2>\<nabla\>\<b-h\><around*|(|\<b-X\>|)><text|<space|1em>where
    >\<b-cal-G\><rprime|'><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><ny>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<cdot\>\<b-cal-R\><rprime|'><around*|(|\<b-m\>|)>.<label|eq:lambda1sol>
  </equation>

  \;

  The unpacked form of <math|\<b-cal-Y\><rprime|'>> is obtained by a similar
  procedure as in Section<nbsp><reference|ssec:extract-A0-K1> and the result
  is

  <\equation>
    <tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>|<cell|=>|<cell|<around*|(|\<b-Y\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)>\<cdot\>\<b-l\>+\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-l\>>>>>><label|eq:Yp-unpack>
  </equation>

  where

  <\equation>
    <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-Y\><rsub|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-Y\><rprime|'><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>><tc2><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>3\<nocomma\>2>>>>|<row|<cell|\<b-Y\><rsub|0><rprime|'><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-Y\><rprime|'><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>><tc2><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>>>>>><label|eq:Yp-extraction>
  </equation>

  The expression of <math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>>
  announced in<nbsp>(<reference|eq:y1-star>) then follows
  from<nbsp>(<reference|eq:Yp-unpack>).

  <subsection|Microscopic strain>

  In the expansion of the macroscopic strain <math|\<b-E\>>
  in<nbsp>(<reference|eq:E-alpha-expansion-nearly-done>), we have so far
  obtained the leading-order correction <math|\<b-E\><rsup|<around*|[|0|]>>=\<b-F\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  in<nbsp>(<reference|eq:E-hom-decompose>). Here, we calculate the
  higher-order corrections.

  The gradient of the solution <math|\<b-y\><rsub|<around*|[|1|]>><around*|(|\<b-X\>|)>=\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  given in<nbsp>(<reference|eq:corrector-packed>) can be written as

  <\equation*>
    <tabular|<tformat|<table|<row|<cell|<around*|(|\<nabla\>\<b-y\><rsub|<around*|[|1|]>>|)><rsub|y\<nocomma\>b>>|<cell|=>|<cell|<frac|\<partial\>|\<partial\>X<rsub|b>><around*|[|\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k><around*|(|\<b-m\>|)>*\<nabla\>h<rsub|h\<nocomma\>a>*h<rsub|k>|]>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>|)><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k\<nocomma\>m>\<cal-V\><rsup|m><rsub|m\<nocomma\>n>\<nabla\>h<rsub|n\<nocomma\>b><rsub|>*\<nabla\>h<rsub|h\<nocomma\>a>*h<rsub|k>+\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>n><around*|(|\<b-m\>|)>*\<nabla\>h<rsub|h\<nocomma\>a>*\<nabla\>h<rsub|n\<nocomma\>b>+\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k><around*|(|\<b-m\>|)>*\<nabla\><rsup|2>h<rsub|h\<nocomma\>a\<nocomma\>b>*h<rsub|k>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>|)><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k\<nocomma\>n>*\<nabla\>h<rsub|n\<nocomma\>b><rsub|>*\<nabla\>h<rsub|h\<nocomma\>a>h<rsub|k>+\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>n><around*|(|\<b-m\>|)>*\<cal-V\><rsup|1><rsub|k>*\<nabla\>h<rsub|n\<nocomma\>b>*\<nabla\>h<rsub|h\<nocomma\>a>*h<rsub|k>+\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k><around*|(|\<b-m\>|)>*\<nabla\><rsup|2>h<rsub|h\<nocomma\>a\<nocomma\>b>*h<rsub|k>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>3\<nocomma\>5\<nocomma\>4>>|)><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k\<nocomma\>n>*\<nabla\>h<rsub|n\<nocomma\>b><rsub|>*\<nabla\>h<rsub|h\<nocomma\>a>h<rsub|k>+\<cal-Y\><rprime|'><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k><around*|(|\<b-m\>|)>*\<nabla\><rsup|2>h<rsub|h\<nocomma\>a\<nocomma\>b>*h<rsub|k>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>3\<nocomma\>5\<nocomma\>4>>|)>\<otimes\>\<b-I\><rsub|d>|)><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k\<nocomma\>n\<nocomma\>b\<nocomma\>c>*\<nabla\>h<rsub|n\<nocomma\>c><rsub|>*\<nabla\>h<rsub|h\<nocomma\>a>h<rsub|k>>>|<row|<cell|>|<cell|>|<cell|<space|2em>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-I\><rsub|d>|)><rsub|y\<nocomma\>h\<nocomma\>a\<nocomma\>k*b*c>*\<nabla\><rsup|2>h<rsub|h\<nocomma\>a\<nocomma\>c>*h<rsub|k>>>>>>
  </equation*>

  which we can rewrite in tensor form as

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<nabla\>\<b-y\><rsub|<around*|[|1|]>>>|<cell|=>|<cell|<around*|[|<around*|(|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>3\<nocomma\>5\<nocomma\>4>>|)>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>6\<nocomma\>7\<nocomma\>3\<nocomma\>2\<nocomma\>4>>\<cdot\>\<b-h\>|]><value|tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>>>|<row|<cell|>|<cell|>|<cell|<space|2em>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>4\<nocomma\>6*2*5>><value|tc4><around*|(|\<nabla\><rsup|2>\<b-h\>\<otimes\>\<b-h\>|)>.>>>>><label|eq:grad-y1-final>
  </equation>

  Inserting into<nbsp>(<reference|eq:E-alpha-expansion-nearly-done-components>),
  we have

  <\equation>
    <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<cwith|2|2|3|3|cell-background|>|<table|<row|<cell|\<b-E\><rsup|<around*|[|1|]>>>|<cell|=>|<cell|<around*|(|\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>>|<row|<cell|\<b-E\><rsup|<around*|[|2|]>>>|<cell|=>|<cell|\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>+<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>.>>>>><label|eq:higher-order-E-relocalized>
  </equation>

  where

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-F\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2><around*|(|<around*|(|<frac|\<mathd\>\<b-cal-Y\><rprime|'>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>+<around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>3\<nocomma\>5\<nocomma\>4>>|)>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>6\<nocomma\>7\<nocomma\>3\<nocomma\>2\<nocomma\>4>>|]><rsup|S<rsub|<around*|{|2\<nocomma\>3|}>\<nocomma\><around*|{|4\<nocomma\>5|}>>>>>|<row|<cell|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2><around*|(|\<b-cal-Y\><rprime|'><around*|(|\<b-m\>|)>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>4\<nocomma\>6*2*5>>|]><rsup|S<rsub|3\<nocomma\>4>>.>>>>><label|eq:calF-def>
  </equation>

  The symmetries of the tensors are listed in
  Table<nbsp><reference|tab:internal-tensors>.

  The unpacked form of these tensors is given by

  <\equation>
    <tabular|<tformat|<cwith|1|3|1|1|cell-halign|r>|<cwith|1|2|1|1|cell-halign|r>|<cwith|2|2|3|3|cell-background|>|<table|<row|<cell|<around*|(|\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc2>\<nabla\>\<b-h\>>|<cell|=>|<cell|<around*|(|\<b-F\><rsub|1><around*|(|\<b-m\>|)><value|tc2>\<nabla\>\<b-m\>|)>\<cdot\>\<b-l\>+\<b-F\><rprime|'><rsub|0><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-l\>>>|<row|<cell|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>>|<cell|=>|<cell|<around*|(|\<b-F\><rsub|2><around*|(|\<b-m\>|)><value|tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>|)>\<cdot\>\<b-l\>+<around*|(|\<b-F\><rprime|'><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc2>\<nabla\>\<b-l\>>>|<row|<cell|<around*|(|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>>|<cell|=>|<cell|<around*|(|<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>|)>\<cdot\>\<b-l\>+\<b-F\><rprime|''><rsub|0><around*|(|\<b-m\>|)><value|tc3>\<nabla\><rsup|2>\<b-l\>>>>>>
  </equation>

  where

  <\equation>
    <tabular|<tformat|<cwith|1|6|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<cwith|2|2|1|1|cell-halign|r>|<cwith|4|4|1|1|cell-halign|r>|<table|<row|<cell|\<b-F\><rsub|0><rprime|'><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>><around*|(|\<b-m\>|)><value|tc2><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>>>|<row|<cell|\<b-F\><rprime|''><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|2>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>3\<nocomma\>5>><around*|(|\<b-m\>|)><value|tc2><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>4\<nocomma\>2>>>>|<row|<cell|\<b-F\><rsub|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>><around*|(|\<b-m\>|)><value|tc2><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>3\<nocomma\>2>>>>|<row|<cell|\<b-F\><rsub|1><rprime|'><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>><around*|(|\<b-m\>|)><value|tc3><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3>>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>5\<nocomma\>2\<nocomma\>4>>>>|<row|<cell|\<b-F\><rsub|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>><around*|(|\<b-m\>|)><value|tc3><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>6\<nocomma\>3\<nocomma\>5\<nocomma\>2>>>>|<row|<cell|<wide|\<b-F\>|~><rsub|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|2>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>3\<nocomma\>5>><around*|(|\<b-m\>|)><value|tc2><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>5\<nocomma\>3\<nocomma\>2>>>>>>><label|eq:extract-F>
  </equation>

  The symmetries of the tensors are listed in
  Table<nbsp><reference|tab:tensors-delivered>.

  Combining<nbsp>(<reference|eq:higher-order-E-relocalized>)
  and<nbsp>(<reference|eq:calF-def>), we obtain the microscopic strain in the
  form announced in<nbsp>(<reference|eq:E-optimum-summary>\U<reference|eq:F-breakup>).

  <subsection|Homogenized energy at order
  <math|\<eta\><rsup|2>>><label|ssec:work-out-Phi2>

  Insert expression of <math|\<b-E\><rsup|<around*|[|1|]>>> from
  (<reference|eq:higher-order-E-relocalized>)<rsub|1> into
  energy<nbsp>(<reference|eq:Phi12-yi-eliminated>) and observe that the
  quantity in factor of <math|\<b-S\><rsub|0>\<cdot\>\<b-l\>> in the
  integrand is nothing but <math|\<b-E\><rsup|<around*|[|2|]>>-\<b-E\><rsub|y>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>>
  which is given by (<reference|eq:higher-order-E-relocalized>)<rsub|2> as
  <math|\<b-E\><rsup|<around*|[|2|]>>-\<b-E\><rsub|y>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>=<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-F\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>>.
  The result is

  <\equation>
    <tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|\<Phi\><rsup|><rsub|<around*|[|2|]>>>|<cell|=>|<cell|<big|int><rsub|\<Omega\>><around*|(|<tabular|<tformat|<table|<row|<cell|<frac|1|2>*\<b-E\><rsup|<around*|[|1|]>>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-E\><rsup|<around*|[|1|]>>>>|<row|<cell|<separating-space|1em><around*|\<nobracket\>||\<nobracket\>>+<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\><around*|<left|(|2>|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-F\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>|<right|)|2>>>>>>>|)>*\<mathd\>\<b-X\>.>>>>>
  </equation>

  which we rewrite as

  <\equation>
    \<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>><around*|(|<around*|(|\<b-cal-B\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>+<around*|(|\<b-cal-C\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>|)>*\<mathd\>\<b-X\><label|eq:new-Phi2-star-packed>
  </equation>

  where

  <\equation>
    <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-B\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<tabular|<tformat|<table|<row|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|1>|)><rsup|T<rsub|4\<nocomma\>1\<nocomma\>2\<nocomma\>3>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>4\<nocomma\>6>>>>|<row|<cell|<space|2em>+2*<around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-cal-F\><rsup|1\<nocomma\>1>|)><rsup|T<rsub|6\<nocomma\>1\<nocomma\>2\<nocomma\>3\<nocomma\>4\<nocomma\>5>>>>>>>|]><rsup|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>\<circ\>S<rsub|5\<nocomma\>6>>>>|<row|<cell|\<b-cal-C\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-cal-F\><rsup|2>|)><rsup|T<rsub|5\<nocomma\>1\<nocomma\>2\<nocomma\>3\<nocomma\>4>>|]><rsup|S<rsub|4\<nocomma\>5>>.>>>>><label|eq:implement-B-C>
  </equation>

  The <math|S<rsub|2\<nocomma\>3>> symmetry of
  <math|\<b-cal-C\><around*|(|\<b-m\>|)>> derives automatically from the
  <math|S<rsub|3\<nocomma\>4>> symmetry of <math|\<b-cal-F\><rsup|2>>. The
  other symmetries are enforced.

  <em|In the library, we implement the variant
  of<nbsp>(<reference|eq:implement-B-C>) given
  in<nbsp>(<reference|eq:B-C-rank-deficient-aware>) to deal more elegantly
  with rank-deficient problems.>

  With the help of the content tracking done in
  Table<nbsp><reference|tab:internal-tensors>, we obtain the unpacked form of
  these tensors as

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<around*|(|\<b-cal-B\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc4><frac|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|2>>|<cell|=>|<cell|<around*|(|\<b-K\><rsub|2><around*|(|\<b-m\>|)><tc4><around*|(|\<nabla\>\<b-m\>\<otimes\>\<nabla\>\<b-m\>|)>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>+<around*|(|\<b-A\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>>>|<row|<cell|>|<cell|>|<cell|<space|2em>+\<b-B\><rsub|0><around*|(|\<b-m\>|)><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>>>|<row|<cell|<around*|(|\<b-cal-C\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\>>|<cell|=>|<cell|<around*|(|<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2>\<b-m\>|)><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>+\<b-C\><rsub|0><around*|(|\<b-m\>|)><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>>>>>><label|eq:upacking-BC>
  </equation>

  where

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<b-K\><rsub|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-B\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>6>><around*|(|\<b-m\>|)><tc4><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>2\<nocomma\>6\<nocomma\>3\<nocomma\>7\<nocomma\>4\<nocomma\>8>>|)><rsup|T<rsub|46\<nocomma\>3\<nocomma\>5\<nocomma\>1\<nocomma\>2>>>>|<row|<cell|\<b-A\><rsub|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|\<b-cal-B\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>6>><around*|(|\<b-m\>|)><tc4><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>2\<nocomma\>6\<nocomma\>3\<nocomma\>7\<nocomma\>4>>|)><rsup|T<rsub|3\<nocomma\>5\<nocomma\>2\<nocomma\>4\<nocomma\>1>>>>|<row|<cell|\<b-B\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|(|\<b-cal-B\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>6>><around*|(|\<b-m\>|)><tc4><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>5\<nocomma\>2\<nocomma\>6\<nocomma\>3\<nocomma\>4>>|)><rsup|T<rsub|2\<nocomma\>4\<nocomma\>1\<nocomma\>3>>>>|<row|<cell|<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|\<b-cal-C\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2\<nocomma\>4\<nocomma\>5>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>>|)><rsup|T<rsub|4\<nocomma\>5\<nocomma\>3\<nocomma\>1\<nocomma\>2>>>>|<row|<cell|\<b-C\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|\<b-cal-C\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2\<nocomma\>4\<nocomma\>5>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>3\<nocomma\>5>>|)><rsup|T<rsub|3\<nocomma\>4\<nocomma\>1\<nocomma\>2>>>>>>><label|eq:BC-extract>
  </equation>

  The expression of <math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>>
  announced in<nbsp>(<reference|eq:Phi-2-form>) follows
  from<nbsp>(<reference|eq:new-Phi2-star-packed>)
  and<nbsp>(<reference|eq:upacking-BC>).

  <subsection|Optional integration by parts>

  In option, another integration by parts is possible. This is for
  consistency with JTCAM paper, but it is <em|not recommended> as it breaks
  convexity.

  The <math|\<b-cal-C\><rsup|>> terms appearing
  in<nbsp>(<reference|eq:new-Phi2-star-packed>) can be integrated by parts as

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2><around*|(|\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>|)><tc3>\<nabla\><rsup|2>\<b-h\><around*|(|\<b-X\>|)>*\<mathd\>\<b-X\>=>>|<row|<cell|<around*|\<nobracket\>||\<nobracket\>><space|8em><big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-cal-C\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2><around*|(|\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>|)><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-n\><around*|(|\<b-X\>|)>|)>*\<mathd\>a>>|<row|<cell|<space|9em>+<big|int><rsub|\<Omega\>><around*|(|\<Delta\>\<b-cal-B\><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)><tc2><around*|(|\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>|)><tc4><frac|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<nabla\>\<b-h\><around*|(|\<b-X\>|)>|2>*\<mathd\>\<b-X\>>>>>><label|eq:C0-ibp>
  </equation>

  where

  <\equation>
    \<Delta\>\<b-cal-B\><rsup|><around*|(|\<b-m\>|)>=-2*<around*|(|<frac|\<mathd\>\<b-cal-C\>|\<mathd\>\<b-m\>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-V\><rsup|m>|)><rsup|T<rsub|3\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>6\<nocomma\>1>>-4*<around*|(|\<b-cal-C\><around*|(|\<b-m\>|)>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>4\<nocomma\>3\<nocomma\>5>>\<otimes\>\<b-cal-V\><rsup|1>
  </equation>

  The extraction proceeds as follows:

  <\itemize>
    <item>add <math|\<Delta\>\<b-cal-B\><rsup|><around*|(|\<b-m\>|)>> to
    <math|\<b-cal-B\><around*|(|\<b-m\>|)>> <em|before> extracting
    <math|\<b-K\><rsub|2>>, <math|\<b-A\><rsub|1>>, <math|\<b-B\><rsub|0>>

    <item>suppress the <math|<wide|\<b-K\>|~><rsub|2><around*|(|\<b-m\>|)>>
    and <math|\<b-C\><rsub|0><around*|(|\<b-m\>|)>> terms associated with
    <math|\<b-cal-C\>> from the output

    <item>extract the boundary terms as earlier in JTCAM paper:

    <\equation>
      <big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-cal-C\><around*|(|\<b-m\>|)><tc2><around*|(|\<b-h\>\<otimes\>\<b-h\>|)>|)><tc3><around*|(|\<nabla\>\<b-h\>\<otimes\>\<b-n\>|)>=<big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-k\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>|)><tc3><around*|(|<frac|\<b-l\>\<otimes\>\<b-l\>|2>\<otimes\>\<b-n\>|)>+\<b-a\><rsub|0><around*|(|\<b-m\>|)><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>\<otimes\>\<b-n\>|)>
    </equation>

    where

    <\equation>
      <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-k\><rsub|1><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|\<b-cal-C\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2\<nocomma\>4\<nocomma\>5>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|m>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>>|)><rsup|T<rsub|5\<nocomma\>3\<nocomma\>4\<nocomma\>1\<nocomma\>2>>>>|<row|<cell|\<b-a\><rsub|0><around*|(|\<b-m\>|)>>|<cell|=>|<cell|2*<around*|(|\<b-cal-C\><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2\<nocomma\>4\<nocomma\>5>><around*|(|\<b-m\>|)><tc3><around*|(|<around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\><around*|(|\<b-cal-V\><rsup|l>|)><rsup|T>\<otimes\>\<b-cal-V\><rsup|1>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3>>|)><rsup|T<rsub|3\<nocomma\>4\<nocomma\>2\<nocomma\>1>>>>>>>
    </equation>

    <item>in Equations<nbsp>(<reference|eq:homogenized-model-announce>\U<reference|eq:KABka-expansion>),
    add a boundary contribution

    <\equation>
      \<Phi\><rprime|\<star\>><around*|[|\<b-m\>,\<b-l\>|]>=\<cdots\>+<big|oint><rsub|\<partial\>\<Omega\>><around*|[|\<b-k\><around*|[|\<b-m\>|]><tc3><around*|(|<frac|\<b-l\>\<otimes\>\<b-l\>|2>\<otimes\>\<b-n\>|)>+\<b-a\><around*|[|\<b-m\>|]><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>\<otimes\>\<b-n\>|)>|]>*\<mathd\>a,<label|eq:final-ibp-Phi-star>
    </equation>

    where

    <\equation>
      <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<cwith|1|2|5|5|cell-halign|c>|<cwith|1|2|6|6|cell-halign|c>|<cwith|1|2|3|3|cell-halign|c>|<table|<row|<cell|\<b-k\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|>|<cell|>|<cell|\<b-k\><rsub|1><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-m\>>|<cell|+>|<cell|>|<cell|>|<cell|>>|<row|<cell|\<b-a\><around*|[|\<b-m\>|]>>|<cell|=>|<cell|\<b-a\><rsub|0><around*|(|\<b-m\>|)>>|<cell|+>|<cell|\<cdots\>>|<cell|>|<cell|>|<cell|>|<cell|>>>>><label|eq:final-ibp-k-a>
    </equation>

    The property of the additional tensors are listed in
    Tables<nbsp><reference|eq:final-ibp-tensors>
    and<nbsp><reference|eq:final-ibp-tensors-content>, in complement with the
    tensors already in given in Tables<nbsp><reference|tab:tensor-symmetries>
    and<nbsp><reference|tab:tensors-delivered>.

    <\big-table|<tabular|<tformat|<table|<row|<cell|<math|<tabular|<tformat|<cwith|2|3|1|1|cell-halign|r>|<cwith|2|2|1|1|cell-halign|r>|<cwith|2|3|2|2|cell-halign|c>|<cwith|2|3|3|3|cell-halign|c>|<cwith|2|3|1|1|cell-lborder|0ln>|<cwith|1|1|2|-1|cell-halign|c>|<cwith|3|3|1|-1|cell-bborder|0ln>|<cwith|3|3|1|1|cell-lborder|0ln>|<cwith|3|3|3|3|cell-rborder|0ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|0ln>|<cwith|3|3|1|-1|cell-tborder|0ln>|<cwith|2|2|1|1|cell-lborder|0ln>|<cwith|2|2|3|3|cell-rborder|0ln>|<cwith|1|1|2|2|cell-tborder|0ln>|<cwith|3|3|2|2|cell-bborder|0ln>|<cwith|1|-1|2|2|cell-lborder|1ln>|<cwith|1|-1|1|1|cell-rborder|1ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|3|3|cell-lborder|0ln>|<table|<row|<cell|>|<cell|tensor
    space>|<cell|symmetries>>|<row|<cell|\<b-k\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d|)>>>|<cell|S<rsub|1\<nocomma\>2>>>|<row|<cell|\<b-a\><around*|[|\<b-m\>|]>>|<cell|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,d|)>>>|<cell|<text|\U>>>>>>>>>>>>>
      Tensors associated with the boundary term
      in<nbsp>(<reference|eq:final-ibp-Phi-star>).<label|eq:final-ibp-tensors>
    </big-table>

    <\big-table|<tabular|<tformat|<cwith|2|3|1|1|cell-halign|c>|<cwith|2|3|2|4|cell-halign|c>|<cwith|2|3|4|4|cell-halign|l>|<cwith|3|3|1|-1|cell-bborder|0ln>|<cwith|3|3|1|1|cell-lborder|0ln>|<cwith|3|3|4|4|cell-rborder|0ln>|<cwith|1|1|2|2|cell-tborder|0ln>|<cwith|3|3|2|2|cell-bborder|0ln>|<cwith|1|-1|2|2|cell-lborder|1ln>|<cwith|1|-1|1|1|cell-rborder|1ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|3|3|cell-lborder|0ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|0ln>|<cwith|3|3|1|-1|cell-tborder|0ln>|<cwith|2|2|1|1|cell-lborder|0ln>|<cwith|2|2|4|4|cell-rborder|0ln>|<cwith|1|1|2|2|cell-halign|c>|<cwith|1|1|3|3|cell-halign|c>|<cwith|1|1|4|4|cell-halign|c>|<table|<row|<cell|>|<cell|tensor
    space>|<cell|symmetry>|<cell|usage>>|<row|<cell|<math|\<b-k\><rsub|1><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,<nm>,d|)>>>>|<cell|<math|S<rsub|1\<nocomma\>2>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|oint><rsub|\<partial\>\<Omega\>><around*|(|\<b-k\><rsub|1><around*|[|\<b-m\>|]><tc2>\<nabla\>\<b-m\>|)><tc3><around*|(|<frac|\<b-l\>\<otimes\>\<b-l\>|2>\<otimes\>\<b-n\>|)>*\<mathd\>a+\<cdots\>>>>|<row|<cell|<math|\<b-a\><rsub|0><around*|(|\<b-m\>|)>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,d|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|oint><rsub|\<partial\>\<Omega\>>\<b-a\><rsub|0><around*|[|\<b-m\>|]><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>\<otimes\>\<b-n\>|)>**\<mathd\>a+\<cdots\>>>>>>>>
      Content of the tensors associated with the boundary term,
      see<nbsp>(<reference|eq:final-ibp-k-a>).<label|eq:final-ibp-tensors-content>
    </big-table>
  </itemize>

  <appendix|Special case of homogeneous properties><label|app:homogeneous
  properties>

  The special case of homogeneous properties (applicable to a perfectly
  periodic elastic truss for instance) is considered here. In this special
  case, the parameter <math|\<b-m\><around*|(|\<b-X\>|)>> goes away
  (<math|<nm>=0>) and we can remove the trailing \<#2018\>1\<#2019\> in the
  definition<nbsp>(<reference|tab:internal-tensors>) of <math|\<b-h\>>: as a
  result, we can simply set

  <\equation>
    \<b-h\>=\<b-l\>,<separating-space|2em>\<b-cal-V\><rsup|l>=\<b-I\><rsub|n<rsub|l>>,<separating-space|2em>\<b-cal-V\><rsup|m>=\<b-0\>,<separating-space|2em>\<b-cal-V\><rsup|1>=\<b-0\>.
  </equation>

  The analysis of leading order is unchanged: it delivers
  <math|\<b-Y\><rsub|0>> and <math|\<b-S\><rsub|0>>, which are no longer
  functions of <math|\<b-m\>>, as illustrated in
  Table<nbsp><reference|tab:leading-order-summary> for the non-deficient
  case. There are many simplifications at the next orders and the
  corresponding, specialized formulas are provided in
  Table<nbsp><reference|tab:ho-homogenization-uniform-props>. Note that the
  tensor dimensions are changed compared to the general case, see also the
  \<#2018\>usage\<#2019\> column in the table. As a consequence, the indices
  used in the transpose operations are affected. Although it is
  straightforward in principle, the specialization of the general formulas to
  this special case is cumbersome\Vto a point that we found it easier to
  re-derive them from scratch. Their consistency with the general formulas is
  checked in a dedicated Mathematica notebook (see
  <verbatim|shoal-library-v1.0/discrete_engine/tests/verifySpecialFormulasHomogeneousCase.nb>).

  <\big-table|<tabular|<tformat|<cwith|1|1|3|6|cell-halign|c>|<cwith|2|-1|1|1|cell-halign|r>|<cwith|2|-1|4|4|cell-halign|c>|<cwith|2|-1|5|5|cell-halign|c>|<cwith|7|7|1|1|cell-halign|r>|<cwith|7|7|4|4|cell-halign|c>|<cwith|7|7|5|5|cell-halign|c>|<cwith|8|8|1|1|cell-halign|r>|<cwith|8|8|4|4|cell-halign|c>|<cwith|8|8|5|5|cell-halign|c>|<cwith|8|8|1|1|cell-halign|r>|<cwith|8|8|4|4|cell-halign|c>|<cwith|9|10|1|1|cell-halign|r>|<cwith|9|10|4|4|cell-halign|c>|<cwith|9|10|5|5|cell-halign|c>|<cwith|11|11|1|1|cell-halign|r>|<cwith|11|11|1|1|cell-halign|r>|<cwith|11|11|1|1|cell-halign|r>|<cwith|11|11|1|1|cell-halign|r>|<cwith|11|11|4|4|cell-halign|c>|<cwith|11|11|4|4|cell-halign|c>|<cwith|11|11|4|4|cell-halign|c>|<cwith|11|11|5|5|cell-halign|c>|<cwith|11|11|5|5|cell-halign|c>|<cwith|6|6|1|-1|cell-background|#f0f0f0>|<cwith|13|19|1|-1|cell-background|#f0f0f0>|<cwith|15|16|1|1|cell-halign|r>|<cwith|15|16|4|4|cell-halign|c>|<cwith|15|16|5|5|cell-halign|c>|<cwith|15|16|1|6|cell-background|#f0f0f0>|<table|<row|<cell|>|<cell|>|<cell|definition>|<cell|dimension>|<cell|sym.>|<cell|usage>>|<row|<cell|<math|\<b-cal-L\><rsup|1>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-Y\><rsub|0>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2\<nocomma\>4>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,<nl>,d|)>>>>|<cell|>|<cell|<math|\<nabla\>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\><rsup|1><tc2>\<nabla\>\<b-l\>>>>|<row|<cell|<math|\<b-cal-L\><rsup|2>>>|<cell|<math|=>>|<cell|<math|<around*|[|<around*|(|\<b-Y\><rsub|0>\<otimes\>\<b-I\><rsub|d>\<otimes\>\<b-I\><rsub|d>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>6>>|]><rsup|S<rsub|2\<nocomma\>3>\<circ\>S<rsub|5\<nocomma\>6>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,d,<nl>,d,d|)>>>>|<cell|<math|S<rsub|2\<nocomma\>3>>,
  <math|S<rsub|5\<nocomma\>6>>>|<cell|<math|\<nabla\><rsup|2>\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>>=\<b-cal-L\><rsup|2><tc3>\<nabla\><rsup|2>\<b-l\>>>>|<row|<cell|<math|\<b-cal-J\><rsup|1>>>|<cell|<math|=>>|<cell|<math|\<b-E\><rsub|l><rprime|'>+\<b-E\><rsub|y><rprime|'><tc2>\<b-cal-L\><rsup|1>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>>|<cell|>|<cell|<math|\<b-E\><rsup|<around*|[|1|]>>=\<b-cal-J\><rsup|1><tc2>\<nabla\>\<b-l\>+\<ldots\>>>>|<row|<cell|<math|\<b-cal-J\><rsup|2>>>|<cell|<math|=>>|<cell|<math|\<b-E\><rsub|l><rprime|''>+\<b-E\><rsub|y><rprime|''><tc3>\<b-cal-L\><rsup|2>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<b-E\><rsup|<around*|[|2|]>>=\<b-cal-J\><rsup|2><tc3>\<nabla\><rsup|2>\<b-l\>+\<ldots\>>>>|<row|<cell|<math|\<b-A\><rsub|0>>>|<cell|<math|=>>|<cell|<math|<around*|[|<around*|(|\<b-cal-J\><rsup|1>|)><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2>>\<cdot\>\<b-S\><rsub|0>|]><rsup|T<rsub|2\<nocomma\>3\<nocomma\>1>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d|)>>>>|<cell|>|<cell|<math|\<Phi\><rsub|<around*|[|1|]>>=<big|int><rsub|\<Omega\>>\<b-A\><rsub|0><tc3><around*|(|\<b-l\>\<otimes\>\<nabla\>\<b-l\>|)>*\<mathd\>\<b-X\>>>>|<row|<cell|<math|<deleted|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>>>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-cal-J\><rsup|1>|)><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2>>\<cdot\>\<b-cal-K\>\<cdot\>\<b-cal-J\><rsup|1>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,<nl>,d|)>>>>|<cell|<math|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>>
  (delayed)>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-cal-J\><rsup|1>|)><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2>>\<cdot\>\<b-cal-K\>\<cdot\>\<b-E\><rsub|y>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,<ny>|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><tc3><around*|(|\<nabla\>\<b-l\>\<otimes\>\<b-y\><rsub|1>|)>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|<deleted|\<b-cal-C\><rsup|<around*|(|0|)>>>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-cal-J\><rsup|2>|)><rsup|T<rsub|4123>>\<cdot\>
  \<b-S\><rsub|0>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,d,<nl>|)>>>>|<cell|<math|S<rsub|2\<nocomma\>3>>
  (delayed)>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|0|)>>\<cdot\>\<b-l\>|)><tc3>\<nabla\><rsup|2>\<b-l\>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-cal-C\><rsup|<around*|(|1|)>>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-E\><rsub|y><rprime|'>|)><rsup|T<rsub|312>>\<cdot\>
  \<b-S\><rsub|0>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,d,<nl>|)>>>>|<cell|\U>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>>=<big|int><rsub|\<Omega\>><around*|(|\<b-cal-C\><rsup|<around*|(|1|)>>\<cdot\>\<b-l\>|)><tc2>\<nabla\>\<b-y\><rsub|1>*\<mathd\>\<b-X\>+\<cdots\>>>>|<row|<cell|<math|\<b-cal-B\><rsup|<around*|(|1|)>>>>|<cell|<math|=>>|<cell|<math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>-<around*|(|\<b-cal-C\><rsup|<around*|(|1|)>>|)><rsup|T<rsub|3\<nocomma\>2\<nocomma\>1>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,<ny>|)>>>>|<cell|\U>|same
  as <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>>>|<row|<cell|<math|\<b-cal-R\><rprime|'>>>|<cell|<math|=>>|<cell|<math|-\<b-P\><rprime|\<dag\>>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>>|)><rsup|T<rsub|2\<nocomma\>3\<nocomma\>1>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1>>>>>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>+<nc>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|<matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>>|<cell|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>>>>>>>=\<b-cal-R\><rprime|'><tc2>\<nabla\>\<b-l\>>>>|<row|<cell|<math|\<b-Y\><rsub|0><rprime|'>>>|<cell|<math|=>>|<cell|<math|<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-cal-R\><rprime|'>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<ny>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>>=\<b-Y\><rsub|0><rprime|'><tc2>\<nabla\>\<b-l\>>>>|<row|<cell|<math|\<b-G\><rsub|0><rprime|'>>>|<cell|<math|=>>|<cell|<math|<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><ny>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<cdot\>\<b-cal-R\><rprime|'>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nc>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>>=\<b-G\><rsub|0><rprime|'><tc2>\<nabla\>\<b-l\>>>>|<row|<cell|<math|\<b-F\><rsub|0><rprime|'>>>|<cell|=>|<cell|<math|\<b-cal-J\><rsup|1>+\<b-E\><rsub|y>\<cdot\>\<b-Y\><rsub|0><rprime|'>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d|)>>>>|<cell|\U>|<cell|<math|\<b-E\><rsup|<around*|[|1|]>>=\<b-F\><rsub|0><rprime|'><value|tc2>\<nabla\><rsup|>\<b-l\>>>>|<row|<cell|<math|\<b-F\><rsub|0><rprime|''>>>|<cell|=>|<cell|<math|<around*|[|\<b-cal-J\><rsup|2>+<around*|(|<around*|(|\<b-E\><rsub|y><rprime|'>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>\<cdot\>\<b-Y\><rsub|0><rprime|'>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>2\<nocomma\>3>>|]><rsup|S<rsub|3\<nocomma\>4>>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nE>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<b-E\><rsup|<around*|[|2|]>>=\<b-F\><rprime|''><rsub|0><value|tc3>\<nabla\><rsup|2>\<b-l\>+\<cdots\>>>>|<row|<cell|<math|\<b-B\><rsub|0>>>|<cell|<math|=>>|<cell|<math|<around*|(|\<b-F\><rsub|0><rprime|'>|)><rsup|T<rsub|3\<nocomma\>1\<nocomma\>2>>\<cdot\>\<b-cal-K\>\<cdot\>\<b-F\><rsub|0><rprime|'>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,d,<nl>,d|)>>>>|<cell|<math|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-B\><rsub|0><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>*\<mathd\>\<b-X\>+\<ldots\>>>>|<row|<cell|<strong|<math|\<b-C\><rsub|0>>>>|<cell|=>|<cell|<math|\<b-S\><rsub|0><rsup|T>\<cdot\>\<b-F\><rsub|0><rprime|''>>>|<cell|<math|\<bbb-T\><rsup|<around*|(|<nl>,<nl>,d,d|)>>>>|<cell|<math|S<rsub|3\<nocomma\>4>>>|<cell|<math|\<Phi\><rsub|<around*|[|2|]>><rprime|\<star\>>=<big|int><rsub|\<Omega\>>\<b-C\><rsub|0><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>*\<mathd\>\<b-X\>+\<ldots\>>>>>>>>
    Higher-order homogenization in the special case of homogeneous
    properties. A complete implementation of the method in this special case
    is possible based on Table<nbsp><reference|tab:leading-order-summary>
    (leading order, ignoring any dependence on <math|\<b-m\>>) and on the
    definitions appearing in the first column of the table above. The
    quantities appearing in the grey rows are the main results (localization
    tensors for corrective displacement <math|\<b-Y\><rsub|0><rprime|'>>,
    Lagrange multipliers <math|\<b-G\><rsub|0><rprime|'>>, bulk energy
    contributions <math|\<b-A\><rsub|0>> and <math|\<b-B\><rsub|0>> and
    boundary contribution <math|\<b-A\><rsub|0>>).<label|tab:ho-homogenization-uniform-props>
  </big-table>

  As can be expected, all tensors that get contracted with gradients of
  <math|\<b-m\>> are zero, <math|\<b-K\><rsub|1>=\<b-0\>>,
  <math|\<b-K\><rsub|2>=\<b-0\>>, <math|<wide|\<b-K\>|~><rsub|2>=\<b-0\>>,
  <math|\<b-A\><rsub|1>=\<b-0\>>, <math|\<b-Y\><rsub|1>=\<b-0\>> and the
  homogenized energy takes the form

  <\equation>
    \<Phi\><rprime|\<star\>><around*|[|\<b-l\>|]>=<big|int><rsub|\<Omega\>><rsup|><around*|(|\<b-K\><rsub|0><tc2><frac|\<b-l\>\<otimes\>\<b-l\>|2>+\<b-A\><rsub|0>+\<b-B\><rsub|0><tc4><frac|\<nabla\>\<b-l\>\<otimes\>\<nabla\>\<b-l\>|2>+\<b-C\><rsub|0><tc4><around*|(|\<b-l\>\<otimes\>\<nabla\><rsup|2>\<b-l\>|)>|)>*\<mathd\><rsup|2>\<b-X\>+\<cal-O\><around*|(|L<rsup|d>*\<eta\><rsup|3>|)>.
  </equation>

  The microscopic displacement is given by

  <\equation>
    \<b-y\>=\<b-Y\><rsub|0>\<cdot\>\<b-l\>+\<b-Y\><rsub|0><rprime|'><value|tc2>\<nabla\>\<b-l\>+\<cdots\>
  </equation>

  The microscopic strain is given by

  <\equation>
    \<b-E\>=\<b-F\><rsub|0>\<cdot\>\<b-l\>+\<b-F\><rsub|0><rprime|'><value|tc2>\<nabla\><rsup|>\<b-l\>+<around*|(|\<b-F\><rprime|''><rsub|0><value|tc3>\<nabla\><rsup|2>\<b-l\>+\<b-E\><rsub|y>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>|)>+\<cdots\>
  </equation>

  <appendix|Extension to a rank-deficient matrix><label|app:rank-deficient>

  <subsection|Special form of null vectors>

  Assuming that they exist, let us first characterize the null vectors of the
  symmetric matrix <math|\<b-P\><around*|(|\<b-m\>|)>> introduced
  in<nbsp>(<reference|eq:P-matrix>), entering in both the leading order
  problem<nbsp>(<reference|eq:hom-sol-optimum-pb>) and in the determination
  of the corrective displacement<nbsp>(<reference|eq:pb-for-optimal-displacement>).

  For any <math|\<b-z\>=<around*|(|\<b-z\><rsub|y>,\<b-z\><rsub|c>|)>\<in\>\<bbb-R\><rsup|<ny>+<nc>>>
  such that <math|\<b-P\><around*|(|\<b-m\>|)>\<cdot\>\<b-z\>=\<b-0\>>, we
  have

  <\equation*>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|\<cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>\<cdot\>\<b-z\><rsub|y>+\<b-z\><rsub|c>\<cdot\>\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>>|<cell|=>|<cell|\<b-0\>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-z\><rsub|y>>|<cell|=>|<cell|\<b-0\>>>>>>
  </equation*>

  Multiplying the first equation by <math|\<b-z\><rsub|y>>, and using the
  second equation, we get <math|\<b-z\><rsub|y>\<cdot\>\<cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>\<cdot\>\<b-z\><rsub|y>=0>.
  We observe that the assumption<nbsp>(<reference|e:no-micro-mechanism>)
  (positive-definiteness of the energy on the subspace of admissible
  microscopic degrees of freedom) can be rewritten as:
  <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>=\<b-0\>>
  and <math|\<b-y\>\<cdot\>\<cal-W\><rsub|y\<nocomma\>y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>\<gtr\>0>
  implies <math|\<b-y\>=\<b-0\>>. Therefore, we have
  <math|\<b-z\><rsub|y>=0>, which then yields
  <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>\<cdot\>\<b-z\><rsub|c>=\<b-0\>>,
  <em|i.e.>, the <math|\<b-z\><rsub|c>> block is a null vector of
  <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>.
  We have just shown

  <\equation>
    \<b-P\><around*|(|\<b-m\>|)>\<cdot\>\<b-z\>=\<b-0\><separating-space|1em>\<Rightarrow\><separating-space|1em>\<b-z\>=<around*|(|\<b-0\><rsub|<ny>>,\<b-z\><rsub|c>|)><text|
    with <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>\<cdot\>\<b-z\><rsub|c>=\<b-0\>>>.<label|eq:characterize-A-null-vectors>
  </equation>

  The only way that the matrix <math|\<b-P\><around*|(|\<b-m\>|)>> can be
  singular is because of the <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>>
  block.

  With <math|<nd>> denoting the rank deficiency of the matrix <math|\<b-P\>>
  or <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>
  (both are the same by the argument above), we denote as
  <math|\<b-N\><around*|(|\<b-m\>|)>\<in\>\<bbb-T\><rsup|<around*|(|<nd>,<nc>|)>>>
  a list of null vectors of <math|<around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y><around*|(|\<b-m\>|)>|)><rsup|T>>,
  arranged in rows. Equation<nbsp>(<reference|eq:characterize-A-null-vectors>)
  then shows that the null vectors of <math|\<b-P\><around*|(|\<b-m\>|)>> are
  the rows of

  <\equation>
    \<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>=\<b-N\><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><ny>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<in\>\<bbb-T\><rsup|<around*|(|<nd>,<ny>+<nc>|)>>.<label|eq:NA-def>
  </equation>

  <subsection|Solutions of the linear equation>

  We consider the linear equation for a vector
  <math|\<b-X\>\<in\>\<bbb-R\><rsup|<ny>+<nc>>>,

  <\equation>
    \<b-P\><around*|(|\<b-m\>|)>\<cdot\>\<b-X\>=\<b-Y\>.<label|eq:linear-singular-problem>
  </equation>

  Multiplying by any null vector <math|\<b-z\>> of <math|\<b-P\>> and using
  the symmetry <math|\<b-P\><rsup|T>=\<b-P\>>, we obtain
  <math|\<b-z\>\<cdot\>\<b-Y\>=0>. Repeating this argument with all the null
  vectors that have been arranged into <math|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>>,
  we obtain <math|<nd>> solvability conditions

  <\equation>
    \<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>\<cdot\>\<b-Y\>=\<b-0\>.<label|eq:linear-singular-solvability>
  </equation>

  When<nbsp>(<reference|eq:linear-singular-solvability>) is satisfied, the
  solutions <math|\<b-X\>> of<nbsp>(<reference|eq:linear-singular-problem>)
  can be expressed with the help of the Moore-Penrose inverse
  <math|\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>> of
  <math|\<b-P\><around*|(|\<b-m\>|)>> as

  <\equation>
    \<b-X\>=\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\>\<b-Y\>+\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-l\>|^>.<label|eq:linear-singular-solution>
  </equation>

  for an arbitrary choice of the coefficients
  <math|<wide|\<b-l\>|^>\<in\>\<bbb-R\><rsup|<nd>>>. In the right-hand side,
  the first term is a particular solution furnished by the pseudo-inverse
  <math|\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>>, and the second term is
  a linear combination of the column-vectors in
  <math|\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>> forming a basis
  of <math|ker \<b-P\><around*|(|\<b-m\>|)>>, with arbitrary coefficients
  <math|<around*|(|<wide|l|^><rsub|i>|)><rsub|1\<leqslant\>i\<leqslant\><nd>>>.

  <subsection|Extended macroscopic strain vector>

  When the matrix <math|\<b-P\>> is rank deficient, we append the <math|<nd>>
  coefficients <math|<wide|l|^><rsub|i>> appearing
  in<nbsp>(<reference|eq:linear-singular-solution>) to the macroscopic strain
  vector <math|\<b-l\>>, and write

  <\equation>
    \<b-l\>=<matrix|<tformat|<table|<row|<cell|<wide|\<b-l\>|\<check\>>>|<cell|<wide|\<b-l\>|^>>>>>>\<in\>\<bbb-R\><rsup|<around*|(|<wide|n|\<check\>><rsub|l>+<nd>|)>>,<label|eq:extended-l>
  </equation>

  where <math|<wide|\<b-l\>|\<check\>>> are the usual macroscopic strain
  vector that defines the microscopic strain <math|\<b-E\>>,
  see<nbsp>(<reference|eq:strain-canonical-form>), referred to as
  <math|\<b-l\>> in the main body of the paper, while <math|<wide|\<b-l\>|^>>
  are the additional parameters parametrizing the solution <math|\<b-X\>> of
  the rank-deficient linear problem. The dimension of <math|\<b-l\>> is now

  <\equation*>
    <nl>=<nl0>+<nd>.
  </equation*>

  We denote the injection matrices <math|<wide|\<b-cal-I\>|\<check\>>> and
  <math|<wide|\<b-cal-I\>|^>> of <math|<wide|\<b-l\>|\<check\>>> and
  <math|<wide|\<b-l\>|^>> into <math|\<b-l\>>, respectively,

  <\equation>
    <wide|\<b-cal-I\>|\<check\>>=<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<nl0>>>>|<row|<cell|\<b-0\><rsub|<nd>\<times\><nl0>>>>>>>\<in\>\<bbb-T\><rsup|<around*|(|<nl>\<times\><nl0>|)>>,<separating-space|2em><wide|\<b-cal-I\>|^>=<matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nl0>\<times\><nd>>>>|<row|<cell|\<b-I\><rsub|<nd>>>>>>>\<in\>\<bbb-T\><rsup|<around*|(|<nl>\<times\><nd>|)>>,<label|eq:deficient-injection-matrices>
  </equation>

  which enable us to rewrite <math|\<b-l\>> as
  <math|\<b-l\>=<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-l\>|\<check\>>+<wide|\<b-cal-I\>|^>\<cdot\><wide|\<b-l\>|^>>.
  Since <math|<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>>
  and <math|<wide|\<b-cal-I\>|^>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>> are
  orthogonal projections from the space <math|\<bbb-R\><rsup|n<rsub|l>>> in
  which <math|\<b-l\>> lives onto the subspaces with equations
  <math|<wide|\<b-l\>|^>=0> and <math|<wide|\<b-l\>|\<check\>>=0>,
  respectively, the following identity holds,

  <\equation>
    <wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>+<wide|\<b-cal-I\>|^>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>=\<b-I\><rsub|<nl>>.<label|eq:l-space-decomposition>
  </equation>

  \;

  To capture the fact that the macroscopic strain <math|\<b-E\>>
  in<nbsp>(<reference|eq:strain-canonical-form>) is a function of the
  original set of macroscopic degrees of freedom
  <math|<wide|\<b-l\>|\<check\>>>, but not of the added
  <math|<wide|\<b-l\>|^>> part, we require that any sub-block in
  <math|\<b-E\><rsub|l><around*|(|\<b-m\>|)>>,
  <math|\<b-E\><rsub|l><rprime|'><around*|(|\<b-m\>|)>> or
  <math|\<b-E\><rsub|l><rprime|''><around*|(|\<b-m\>|)>> corresponding to a
  range of indices <math|<wide|\<b-l\>|^>> vanishes, <em|i.e.>,

  <\equation>
    \<b-E\><rsub|l><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>=\<b-0\>,<separating-space|2em>\<b-E\><rsub|l><rprime|'><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>=\<b-0\><separating-space|2em>\<b-E\><rsub|l><rprime|''><rsup|T<rsub|1\<nocomma\>4\<nocomma\>3\<nocomma\>2>><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>=\<b-0\>,<label|eq:d-transparent-to-l>
  </equation>

  Indeed, equation<nbsp>(<reference|eq:d-transparent-to-l>) warrants that the
  strain in<nbsp>(<reference|eq:strain-canonical-form>) can be rewritten in
  terms of <math|<wide|\<b-l\>|\<check\>>> and its gradients as

  <\equation>
    \<b-E\>=<wide|\<b-E\>|\<check\>><rsub|l><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-l\>|\<check\>>+<wide|\<b-E\>|\<check\>><rsub|l><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\><wide|\<b-l\>|\<check\>>+<wide|\<b-E\>|\<check\>><rsub|l><rprime|''><around*|(|\<b-m\>|)><tc3>\<nabla\><rsup|2><wide|\<b-l\>|\<check\>>+\<ldots\>+\<b-E\><rsub|y><around*|(|\<b-m\>|)>\<cdot\>\<b-y\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<b-y\><rprime|'>+\<b-E\><rsub|y><rprime|''><around*|(|\<b-m\>|)><tc3>\<b-y\><rprime|''>+\<cdots\><label|eq:E-function-of-lcheck>
  </equation>

  where <math|<wide|\<b-E\>|\<check\>><rsub|l><around*|(|\<b-m\>|)>=\<b-E\><rsub|l><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>>,
  <math|<wide|\<b-E\>|\<check\>><rsub|l><rprime|'><around*|(|\<b-m\>|)>=<around*|(|\<b-E\><rsub|l><rprime|'><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>|)><rsup|T<rsub|1\<nocomma\>3\<nocomma\>2>>>
  and <math|<wide|\<b-E\>|\<check\>><rsub|l><rprime|''><around*|(|\<b-m\>|)>=<around*|(|\<b-E\><rsub|l><rprime|''><rsup|T<rsub|1\<nocomma\>4\<nocomma\>3\<nocomma\>2>><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>|)><rsup|T<rsub|1\<nocomma\>4\<nocomma\>3\<nocomma\>2>>>.
  The proof of<nbsp>(<reference|eq:E-function-of-lcheck>) is left to the
  reader.

  In view of Equation<nbsp>(<reference|eq:d-transparent-to-l>) and
  Table<nbsp><reference|tab:input>, <math|<wide|\<b-l\>|^>> does not appear
  anywhere in the specification of the problem: it is a set of <em|free>
  parameters that are reserved for parameterizing the
  solution<nbsp>(<reference|eq:linear-singular-solution>) of the
  rank-deficient linear problem.

  <subsection|Changes to leading-order analysis>

  The leading-order problem<nbsp>(<reference|eq:hom-sol-optimum-pb>) is of
  the form<nbsp>(<reference|eq:linear-singular-problem>) with
  <math|\<b-Y\>=-<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>>>>\<cdot\>\<b-l\><around*|(|\<b-X\>|)>>.
  The solvability condition<nbsp>(<reference|eq:linear-singular-solvability>)
  yields

  <\equation>
    <around*|[|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>|]>\<cdot\>\<b-l\><around*|(|\<b-m\>|)>=\<b-0\>.<label|eq:solvability-y0>
  </equation>

  The vector in square brackets is an output of the homogenization procedure,
  representing <math|<nd>> conditions that are linear in the macroscopic
  strain <math|\<b-l\>>.

  When<nbsp>(<reference|eq:solvability-y0>) is satisfied, the solution is
  given by<nbsp>(<reference|eq:linear-singular-solution>) as

  <\equation*>
    <matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>=-\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>\<cdot\>\<b-l\>+\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-l\>|^>.
  </equation*>

  Thanks to the definition of the extended macroscopic strain vector
  <math|\<b-l\>> in<nbsp>(<reference|eq:extended-l>), this solutions matches
  the form<nbsp>(<reference|eq:y0-lambda0-combined>) used in the
  non-deficient case, provided we replace the inverse by the Moore\UPenrose
  inverse and include a new term in the definition of <math|\<b-cal-R\>>
  in<nbsp>(<reference|eq:R-order0>),

  <\equation>
    \<b-cal-R\><around*|(|\<b-m\>|)>=-\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>+\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><math|<wide|\<b-cal-I\>|^><rsup|T>>.<label|eq:R-singular-tmp>
  </equation>

  It is convenient to rewrite this equation in a slightly different form, for
  a reason that will be discussed later. Using<nbsp>(<reference|eq:d-transparent-to-l>),
  one can show that <math|<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>\<cdot\><wide|\<b-cal-I\>|^>=\<b-0\>>,
  <em|i.e.>, the operators <math|\<b-cal-W\><rsub|y\<nocomma\>l>> and
  <math|\<b-E\><rsub|l>> do not sense the added degrees of freedom
  <math|<wide|\<b-l\>|^>>. Combining with<nbsp>(<reference|eq:l-space-decomposition>),
  this shows that <math|<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>=<matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>>.
  Inserting into the expression of <math|\<b-cal-R\><around*|(|\<b-m\>|)>>,
  we obtain

  <\equation>
    \<b-cal-R\><around*|(|\<b-m\>|)>=-\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-cal-W\><rsub|y\<nocomma\>l><around*|(|\<b-m\>|)>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|l><around*|(|\<b-m\>|)>>>>>>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>+\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><math|<wide|\<b-cal-I\>|^><rsup|T>>.<label|eq:def-modify-R>
  </equation>

  We use this expression of <math|\<b-cal-R\><around*|(|\<b-m\>|)>> in the
  code and not that proposed earlier in<nbsp>(<reference|eq:R-order0>).
  Indeed, the latter can be recovered as a particular case: when the matrix
  <math|\<b-P\>> is invertible, <math|<nd>=0>, implying that
  <math|\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>=\<b-P\><rsup|-1><around*|(|\<b-m\>|)>>
  and that <math|\<b-N\><rsub|\<b-P\>>> and <math|<wide|\<b-cal-I\>|^>> are
  zero-dimension array, and the last term
  in<nbsp>(<reference|eq:def-modify-R>) vanishes.

  The definitions<nbsp>(<reference|eq:localization-tensor-order0>\U<reference|eq:PVW0>)
  of the other leading-order quantities such as <math|\<b-Y\><rsub|0>>,
  <math|\<b-G\><rsub|0>>, etc.<nbsp>are unchanged.

  The following identity can be established
  using<nbsp>(<reference|eq:localization-tensor-order0>),
  (<reference|eq:def-modify-R>), (<reference|eq:NA-def>) and the
  orthogonality of the projectors <math|<wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\><wide|\<b-cal-I\>|^>=\<b-0\>>
  which follows from<nbsp>(<reference|eq:deficient-injection-matrices>),

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<b-Y\><rsub|0>\<cdot\><wide|\<b-cal-I\>|^>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-cal-R\><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>>>|<row|<cell|>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>\<cdot\>\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>>|<row|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>|)><rsup|T>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-N\><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-0\><rsub|<nc>\<times\><ny>>>|<cell|\<b-I\><rsub|<nc>>>>>>>\<cdot\><matrix|<tformat|<table|<row|<cell|\<b-I\><rsub|<ny>>>>|<row|<cell|\<b-0\><rsub|<ny>\<times\><nc>>>>>>>|)><rsup|T>,>>|<row|<cell|>|<cell|=>|<cell|\<b-0\>.>>>>><label|eq:no-y0-lhat-coupling>
  </equation>

  As a result, the microscopic displacement
  <math|\<b-y\><rsub|<around*|[|0|]>>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>>
  can be expressed as <math|\<b-y\><rsub|<around*|[|0|]>>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><around*|(|<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>+<wide|\<b-cal-I\>|^>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>|)>\<cdot\>\<b-l\>=<wide|\<b-Y\>|\<check\>><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-l\>|\<check\>>>
  where <math|<wide|\<b-Y\>|\<check\>><rsub|0><around*|(|\<b-m\>|)>=\<b-Y\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>>:
  it depends on the original set of degrees of freedom
  <math|<wide|\<b-l\>|\<check\>>> only and the matrix <math|\<b-Y\><rsub|0>>
  has a zero block in the range of indices associated with
  <math|<wide|\<b-l\>|^>>, <math|\<b-Y\><rsub|0><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|<wide|\<b-Y\>|\<check\>><rsub|0><around*|(|\<b-m\>|)>>|<cell|\<b-0\><rsub|<ny>\<times\><nd>>>>>>>>.

  The stress <math|\<b-G\><rsub|0>\<cdot\>\<b-l\>>, however, can depend on
  the <math|<wide|\<b-l\>|^>>-block of <math|\<b-l\>> as well: by adapting
  the calculation in<nbsp>(<reference|eq:no-y0-lhat-coupling>), one can show
  that <math|\<b-G\><rsub|0>\<cdot\><wide|\<b-cal-I\>|^>=\<b-N\><rsup|T><around*|(|\<b-m\>|)>>
  is non-zero in the rank-deficient case. The components of
  <math|<wide|\<b-l\>|^>> can therefore be interpreted as the stress
  associated with the macroscopic kinematic
  constraint<nbsp>(<reference|eq:solvability-y0>); this stress is akin to a
  Lagrange multiplier, <em|i.e.>, is not set by any constitutive law.

  Combining<nbsp>(<reference|eq:no-y0-lhat-coupling>)
  with<nbsp>(<reference|eq:E-localization>) and<nbsp>(<reference|eq:K0-def>),
  one can show that the strain localization tensor <math|\<b-F\><rsub|0>> and
  the equivalent stiffness <math|\<b-K\><rsub|0>> are also uncoupled to
  <math|<wide|\<b-l\>|^>>, implying zero <math|<wide|\<b-l\>|^>>-sub-blocks,

  <\equation>
    \<b-F\><rsub|0><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|<wide|\<b-F\>|\<check\>><rsub|0><around*|(|\<b-m\>|)>>|<cell|\<b-0\><rsub|<ny>\<times\><nd>>>>>>>,<separating-space|2em>\<b-K\><rsub|0><around*|(|\<b-m\>|)>=<matrix|<tformat|<table|<row|<cell|<wide|\<b-K\>|\<check\>><rsub|0><around*|(|\<b-m\>|)>>|<cell|\<b-0\><rsub|<nl0>\<times\><nd>>>>|<row|<cell|\<b-0\><rsub|<nd>\<times\><nl0>>>|<cell|\<b-0\><rsub|<nd>\<times\><nd>>>>>>>.<label|eq:deficient-F0-K0>
  </equation>

  In addition, we obtain the following identity by
  combining<nbsp>(<reference|eq:total-stress-S0>),
  (<reference|eq:no-y0-lhat-coupling>) and<nbsp>(<reference|eq:deficient-F0-K0>)<rsub|1>,

  <\equation>
    <tabular|<tformat|<table|<row|<cell|\<b-S\><rsub|0>\<cdot\><math|<wide|\<b-cal-I\>|^>>>|<cell|=>|<cell|\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    <around*|[|\<b-F\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>|]>+\<b-cal-Q\><rsup|T>\<cdot\>\<b-G\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><math|<math|<wide|\<b-cal-I\>|^>>>>>|<row|<cell|>|<cell|=>|<cell|\<b-cal-Q\><rsup|T>\<cdot\>\<b-N\><rsup|T><around*|(|\<b-m\>|)>.>>>>><label|eq:how-S0-talks-to-ld>
  </equation>

  \;

  The reason we prefer the expression of <math|\<b-cal-R\>>
  in<nbsp>(<reference|eq:def-modify-R>) to that derived first
  in<nbsp>(<reference|eq:R-singular-tmp>) is that it makes it evident that
  <math|\<b-Y\><rsub|0>>, <math|\<b-F\><rsub|0>> and <math|\<b-K\><rsub|0>>
  are insensitive to the added degrees of freedom <math|<wide|\<b-l\>|^>>,
  see the identities<nbsp>(<reference|eq:no-y0-lhat-coupling>\U<reference|eq:deficient-F0-K0>).

  <subsection|Changes to the energy expansion>

  With the help of<nbsp>(<reference|eq:l-space-decomposition>),
  (<reference|eq:how-S0-talks-to-ld>), (<reference|eq:Q-yi-is-zero>),
  and<nbsp>(<reference|eq:PVW0>)<rsub|1>, the second term in the integrand
  in<nbsp>(<reference|eq:phi1-phi2-tmp>)<rsub|2> can be written as

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>>|<cell|=>|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>+<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|^>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>+<around*|(|<math|\<b-cal-Q\><rsup|T>\<cdot\>\<b-N\><rsup|T><around*|(|\<b-m\>|)>>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>+<around*|(|<math|\<b-N\><rsup|T><around*|(|\<b-m\>|)>>\<cdot\><wide|\<b-cal-I\>|^><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\><wide*|\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>|\<wide-underbrace\>><rsub|\<b-0\>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>|)>>>|<row|<cell|>|<cell|>|<cell|<separating-space|2em>+<around*|<left|(|-5>|<wide*|\<b-E\><rsub|y><rsup|T><around*|(|\<b-m\>|)>\<cdot\>\<b-S\><rsub|0><around*|(|\<b-m\>|)>|\<wide-underbrace\>><rsub|\<b-0\>>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|<right|)|-5>>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-l\>|)>\<cdot\><around*|(|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>+\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)><tc2>\<nabla\>\<b-y\><rsub|<around*|[|1|]>>|)>>>>>><label|eq:deficient-eliminate-y2>
  </equation>

  In view of this, the definition of the operators
  <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>>>,
  <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>>,
  <math|\<b-cal-C\><rsup|<around*|(|0|)>>>,
  <math|\<b-cal-C\><rsup|<around*|(|1|)>>> can be modified by including the
  projector <math|<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>>
  onto the subspace <math|<wide|\<b-l\>|^>=\<b-0\>>, to the right of
  <math|\<b-S\><rsub|0><around*|(|\<b-m\>|)>>,

  <\equation>
    <tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<cwith|3|3|1|1|cell-halign|r>|<cwith|1|3|1|1|cell-halign|r>|<cwith|1|2|3|3|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<table|<row|<cell|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|<left|(|1>|<around*|(|\<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|><rsup|T<rsub|412\<nocomma\>3>>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|<right|)|1>><rsup|T<rsub|1\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>4\<nocomma\>6>>>|<cell|+>|<cell|2*<around*|(|\<b-cal-J\><rsup|\<nocomma\>1\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|T<rsub|61234\<nocomma\>5>>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-cal-V\><rsup|l>>>|<row|<cell|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|<left|(|2>|<around*|(|\<b-cal-J\><rsup|\<nocomma\>1><around*|(|\<b-m\>|)>|)><rsup|><rsup|T<rsub|412\<nocomma\>3>>\<cdot\>
    \<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>
    \<b-E\><rsub|y><around*|(|\<b-m\>|)>|<right|)|2>><rsup|T<rsub|1\<nocomma\>2\<nocomma\>4\<nocomma\>3>>>|<cell|>|<cell|>>|<row|<cell|\<b-cal-C\><rsup|<around*|(|0|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|>|<cell|>|<cell|<around*|(|\<b-cal-J\><rsup|2><around*|(|\<b-m\>|)>|)><rsup|T<rsub|5123\<nocomma\>4>>\<cdot\>
    \ \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-cal-V\><rsup|l>>>|<row|<cell|\<b-cal-C\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>>|<cell|=>|<cell|>|<cell|>|<cell|<around*|(|\<b-E\><rsub|y><rprime|'><around*|(|\<b-m\>|)>|)><rsup|T<rsub|312>>\<cdot\>
    \<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-cal-V\><rsup|l>.>>>>><label|eq:deficient-Bcirc-C>
  </equation>

  The original and amended definitions in<nbsp>(<reference|eq:Kt2-At1-Ft1-etc-tmp>)
  and<nbsp>(<reference|eq:deficient-Bcirc-C>) are equally valid, but the
  latter has the advantage that it yields final tensors
  <math|\<b-a\><rsub|1>> and <math|\<b-B\><rsub|0>> having zero
  <math|<wide|\<b-l\>|^>>-sub-blocks (the sub-blocks obtained with the former
  set of definitions do evaluate to zero when the solvability constraints are
  considered but this is much less evident, and potentially confusing).

  Our implementation makes use of<nbsp>(<reference|eq:deficient-Bcirc-C>) and
  not<nbsp>(<reference|eq:Kt2-At1-Ft1-etc-tmp>), both definitions being
  identical for non-singular matrices: when <math|<nd>=0>, we have
  <math|<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>=\<b-I\><rsub|<nl>>>
  by<nbsp>(<reference|eq:l-space-decomposition>).

  Similarly, we implement<nbsp>(<reference|eq:implement-B-C>) in the form

  <\equation>
    <tabular|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<b-cal-B\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<tabular|<tformat|<table|<row|<cell|<around*|(|<around*|(|\<b-cal-F\><rsup|1>|)><rsup|T<rsub|4\<nocomma\>1\<nocomma\>2\<nocomma\>3>><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-K\><around*|(|\<b-m\>|)>\<cdot\>\<b-cal-F\><rsup|1><around*|(|\<b-m\>|)>|)><rsup|T<rsub|1\<nocomma\>2\<nocomma\>5\<nocomma\>3\<nocomma\>4\<nocomma\>6>>>>|<row|<cell|<space|2em>+2*<around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-cal-F\><rsup|1\<nocomma\>1>|)><rsup|T<rsub|6\<nocomma\>1\<nocomma\>2\<nocomma\>3\<nocomma\>4\<nocomma\>5>>>>>>>|]><rsup|S<rsub|<around*|{|1\<nocomma\>2|}>\<nocomma\><around*|{|3\<nocomma\>4|}>>\<circ\>S<rsub|5\<nocomma\>6>>>>|<row|<cell|\<b-cal-C\><around*|(|\<b-m\>|)>>|<cell|=>|<cell|<around*|[|<around*|(|<around*|(|\<b-S\><rsub|0><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>\<cdot\>\<b-cal-V\><rsup|l>|)><rsup|T>\<cdot\>\<b-cal-F\><rsup|2>|)><rsup|T<rsub|5\<nocomma\>1\<nocomma\>2\<nocomma\>3\<nocomma\>4>>|]><rsup|S<rsub|4\<nocomma\>5>>,>>>>><label|eq:B-C-rank-deficient-aware>
  </equation>

  where the projector <math|<wide|\<b-cal-I\>|\<check\>>\<cdot\><wide|\<b-cal-I\>|\<check\>><rsup|T>>
  is systematically combined with the leading-order stress relocalization
  tensor <math|\<b-S\><rsub|0><around*|(|\<b-m\>|)>>.

  <subsection|Changes to the corrective displacement>

  The linear problem<nbsp>(<reference|eq:pb-for-optimal-displacement>) makes
  use of the same matrix <math|\<b-P\>> as the leading-order problem. The
  solvability condition for <math|\<b-y\><rsub|<around*|[|1|]>>> is furnished
  by<nbsp>(<reference|eq:linear-singular-solvability>) as

  <\equation>
    <around*|[|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>|)><rsup|T<rsub|231\<nocomma\>4>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>>>>|]><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>=\<b-0\>.<label|eq:solvability-y1>
  </equation>

  The quantity in square brackets is an output of the homogenization
  procedure that encodes <math|<nd>> conditions depending linearly on
  <math|\<b-l\>> and <math|\<nabla\>\<b-l\>>.

  As we did earlier at the leading order, the solution
  <math|<matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>>
  of<nbsp>(<reference|eq:pb-for-optimal-displacement>) is the sum of a
  particular solution furnished by the Moore-Penrose inverse and a linear
  combination of the null vectors of <math|\<b-P\>> with new coefficients
  <math|<wide|<wide|\<b-l\>|^>|^>>,

  <\equation>
    <matrix|<tformat|<table|<row|<cell|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>|<row|<cell|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>>>>>=-\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>|)><rsup|T<rsub|231\<nocomma\>4>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\><around*|(|\<b-X\>|)>|)>>>>>><tc3><around*|(|\<nabla\>\<b-h\><around*|(|\<b-X\>|)>\<otimes\>\<b-h\><around*|(|\<b-X\>|)>|)>+\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|<wide|\<b-l\>|^>|^>.<label|eq:deficient-order-1-sol>
  </equation>

  The first term in the right-hand side is taken care of by replacing the
  inverse of <math|\<b-P\><around*|(|\<b-m\>|)>> by its Moore-Penrose inverse
  in the definition of <math|\<b-cal-R\><rprime|'>>
  in<nbsp>(<reference|eq:RforY0prime>), as we did earlier with
  <math|\<b-cal-R\>>. The second term <math|\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|<wide|\<b-l\>|^>|^>>
  in <math|\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  and <math|\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>>
  adds up in a straightforward way to the term
  <math|\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|\<b-l\>|^>>
  from leading order: the full microscopic displacement
  <math|\<b-y\><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-y\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<b-y\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<cdots\>>
  and Lagrange multipliers <math|\<b-g\><rprime|\<star\>><around*|(|\<b-X\>|)>=\<b-g\><rsub|<around*|[|0|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<b-g\><rsub|<around*|[|1|]>><rprime|\<star\>><around*|(|\<b-X\>|)>+\<cdots\>>
  obtained by summing up the <math|\<eta\><rsup|0>> and
  <math|\<eta\><rsup|1>> contributions are now linear combinations of the
  null vectors in <math|\<b-N\><rsub|\<b-P\>><around*|(|\<b-m\>|)>> with
  coefficients <math|<wide|\<b-l\>|^>+<wide|<wide|\<b-l\>|^>|^>> having both
  a leading order contribution (<math|<wide|\<b-l\>|^>>) and an order
  <math|\<eta\>> contribution (<math|<wide|<wide|\<b-l\>|^>|^>>). A simple
  way to deal with this complication is (<em|i>)<nbsp>to discard the
  <math|\<b-N\><rsub|\<b-P\>><rsup|T><around*|(|\<b-m\>|)>\<cdot\><wide|<wide|\<b-l\>|^>|^>>
  contribution in<nbsp>(<reference|eq:deficient-order-1-sol>),
  and<nbsp>(<em|ii>) agree that <math|<wide|\<b-l\>|^>> is a <em|series> in
  <math|\<eta\>>. This avoids extending the vector <math|\<b-l\>> with
  <math|<nd>> new entries for every homogenization order.

  Concretely, we simply replace the inverse appearing
  in<nbsp>(<reference|eq:RforY0prime>) by the Moore\UPenrose inverse: our
  implementation uses

  <\equation>
    \<b-cal-R\><rprime|'><around*|(|\<b-m\>|)>=-\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>\<cdot\><matrix|<tformat|<table|<row|<cell|<around*|(|\<b-cal-B\><rsup|<around*|(|1|)>><around*|(|\<b-m\>|)>|)><rsup|T<rsub|231\<nocomma\>4>>>>|<row|<cell|\<b-cal-Q\>\<cdot\>\<b-cal-J\><rsup|1><around*|(|\<b-m\>|)>>>>>>.<label|eq:def-Rprime>
  </equation>

  <subsection|Solvability condition for <math|\<b-y\><rsub|<around*|[|2|]>>>>

  The quantity <math|\<b-y\><rsub|<around*|[|2|]>>> entering in
  <math|\<b-E\><rsup|<around*|[|2|]>>=\<b-E\><rsub|y>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>+<around*|[|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-F\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>|]>>,
  see<nbsp>(<reference|eq:higher-order-E-relocalized>)<rsub|2>, has been
  eliminated from<nbsp>(<reference|eq:deficient-eliminate-y2>) using the
  constraint <math|\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>=0>. For
  <math|\<b-y\><rsub|<around*|[|2|]>>> to exist, one must have
  <math|\<b-0\>=\<b-cal-Q\>\<cdot\>\<b-E\><rsup|<around*|[|2|]>>=\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y>\<cdot\>\<b-y\><rsub|<around*|[|2|]>>+\<b-cal-Q\>\<cdot\><around*|[|<around*|(|\<b-cal-F\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-F\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>|]>>.
  This leads to the compatibility condition

  <\equation>
    \<b-cal-Q\>\<cdot\><around*|[|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>|]>\<in\>Im
    <around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y>|)>.<label|eq:solvability-y2>
  </equation>

  In the code, a basis of vectors perpendicular to <math|Im
  <around*|(|\<b-cal-Q\>\<cdot\>\<b-E\><rsub|y>|)>> is produced using a
  row-reduction algorithm, and the conditions that each of these vectors is
  perpendicular to <math|\<b-cal-Q\>\<cdot\><around*|[|<around*|(|\<b-cal-J\><rsup|1\<nocomma\>1>\<cdot\>\<b-h\>|)><tc4><around*|(|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>|)>+<around*|(|\<b-cal-J\><rsup|2>\<cdot\>\<b-h\>|)><tc3>\<nabla\><rsup|2>\<b-h\>|]>>
  are output: this yields conditions depending linearly on
  <math|\<nabla\>\<b-h\>\<otimes\>\<nabla\>\<b-h\>\<otimes\>\<b-h\>> and
  <math|\<nabla\><rsup|2>\<b-h\>\<otimes\>\<b-h\>>.

  <subsection|Summary: extension to rank-deficient problems>

  The following extension of the code enables us to deal with a
  rank-deficient matrix <math|\<b-P\><around*|(|\<b-m\>|)>>:

  <\itemize>
    <item>provide integers <math|<wide|n|\<check\>><rsub|l>> and <math|<nd>>
    and the injection matrix <math|<wide|\<b-cal-I\>|^>> as a optional
    arguments to the homogenization procedure and check the
    condition<nbsp>(<reference|eq:d-transparent-to-l>) on the tensors
    <math|\<b-E\><rsub|l>>, <math|\<b-E\><rsub|l><rprime|'>> and
    <math|\<b-E\><rsub|l><rprime|''>> passed in argument;

    <item>compute a set of null vectors of the symmetric matrix
    <math|\<b-P\><around*|(|\<b-m\>|)>>, check that there are <math|<nd>>
    such vectors and that they are of the
    form<nbsp>(<reference|eq:characterize-A-null-vectors>), compute the
    Moore-Penrose inverse <math|\<b-P\><rprime|\<dag\>><around*|(|\<b-m\>|)>>
    if <math|<nd>\<gtr\>0>;

    <item>return the solvability conditions<nbsp>(<reference|eq:solvability-y0>),
    (<reference|eq:solvability-y1>) and<nbsp>(<reference|eq:solvability-y2>)
    whenever <math|<nd>\<gtr\>0>;

    <item>replace Equations<nbsp>(<reference|eq:R-order0>),
    (<reference|eq:Kt2-At1-Ft1-etc-tmp>) and<nbsp>(<reference|eq:RforY0prime>)
    yielding <math|\<b-cal-R\>>, <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|0|)>>>,
    <math|<wide|\<b-cal-B\>|\<invbreve\>><rsup|<around*|(|1|)>>>,
    <math|\<b-cal-C\><rsup|<around*|(|0|)>>>,
    <math|\<b-cal-C\><rsup|<around*|(|1|)>>> and <math|\<b-cal-R\><rprime|'>>
    with their extensions<nbsp>(<reference|eq:def-modify-R>),
    (<reference|eq:deficient-Bcirc-C>) and<nbsp>(<reference|eq:def-Rprime>)
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|10>
    <associate|info-flag|detailed>
    <associate|math-font|math-schola>
    <associate|page-medium|paper>
  </collection>
</initial>

<\attachments>
  <\collection>
    <\associate|bib-bibliography>
      <\db-entry|+lvObsnz1uchnvL4|incollection|Boutin-Homogenization-Methods-and-Generalized-2019>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678205853>
      <|db-entry>
        <db-field|author|Claude <name|Boutin>>

        <db-field|title|Homogenization methods and generalized continua in
        linear elasticity>

        <db-field|booktitle|Encyclopedia of Continuum Mechanics>

        <db-field|publisher|Springer>

        <db-field|year|2019>

        <db-field|editor|H. <name|Altenbach><name-sep>A. <name|�chsner>>

        <db-field|address|Berlin, Heidelberg>
      </db-entry>

      <\db-entry|+pRKqT4n10OPBD4z|article|sanchez1980non>
        <db-field|newer|+lvObsnz1uchnvL2>

        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678209493>
      <|db-entry>
        <db-field|author|Enrique <name|Sanchez-Palencia>>

        <db-field|title|Non-homogeneous media and vibration theory>

        <db-field|journal|Lecture Note in Physics, Springer-Verlag>

        <db-field|year|1980>

        <db-field|volume|320>

        <db-field|pages|57\U65>
      </db-entry>

      <\db-entry|+2aqPvRKd2MNKKa5S|book|bakhvalov1989homogenisation>
        <db-field|newer|+2aqPvRKd2MNKKa5R>

        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678440970>
      <|db-entry>
        <db-field|author|Nikolaj S <name|Bachvalov><name-sep>Grigori<math|<wide|<text|\Y>|\<breve\>>>
        Petrovich <name|Panasenko>>

        <db-field|title|Homogenisation: averaging processes in periodic
        media: mathematical problems in the mechanics of composite materials>

        <db-field|publisher|Kluwer Academic Publishers>

        <db-field|year|1989>
      </db-entry>

      <\db-entry|+pRKqT4n10OPBD4y|book|cioranescu1999introduction>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678209493>
      <|db-entry>
        <db-field|author|Doina <name|Cioranescu><name-sep>Patrizia
        <name|Donato>>

        <db-field|title|An introduction to homogenization>

        <db-field|publisher|Oxford university press Oxford>

        <db-field|year|1999>

        <db-field|volume|17>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRH|book|cioranescu2012homogenization>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Doina <name|Cioranescu><name-sep>Jeannine Saint Jean
        <name|Paulin>>

        <db-field|title|Homogenization of reticulated structures>

        <db-field|publisher|Springer Science & Business Media>

        <db-field|year|2012>

        <db-field|volume|136>
      </db-entry>

      <\db-entry|+5xSWiId1YPM124T|article|gambin1989higher>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678274520>
      <|db-entry>
        <db-field|author|B <name|Gambin><name-sep>E <name|Kr�ner>>

        <db-field|title|Higher-order terms in the homogenized stress-strain
        relation of periodic elastic media>

        <db-field|journal|physica status solidi (b)>

        <db-field|year|1989>

        <db-field|volume|151>

        <db-field|number|2>

        <db-field|pages|513\U519>

        <db-field|publisher|Wiley Online Library>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRW|article|boutin1996microstructural>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Claude <name|Boutin>>

        <db-field|title|Microstructural effects in elastic composites>

        <db-field|journal|International Journal of Solids and Structures>

        <db-field|year|1996>

        <db-field|volume|33>

        <db-field|number|7>

        <db-field|pages|1023\U1051>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRX|article|smyshlyaev2000rigorous>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Valery P <name|Smyshlyaev><name-sep>Kirill D
        <name|Cherednichenko>>

        <db-field|title|On rigorous derivation of strain gradient effects in
        the overall behaviour of periodic heterogeneous media>

        <db-field|journal|Journal of the Mechanics and Physics of Solids>

        <db-field|year|2000>

        <db-field|volume|48>

        <db-field|number|6-7>

        <db-field|pages|1325\U1357>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRY|article|hans2008dynamics>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Stephane <name|Hans><name-sep>Claude <name|Boutin>>

        <db-field|title|Dynamics of discrete framed structures: a unified
        homogenized description>

        <db-field|journal|Journal of Mechanics of Materials and Structures>

        <db-field|year|2008>

        <db-field|volume|3>

        <db-field|number|9>

        <db-field|pages|1709\U1739>

        <db-field|publisher|Mathematical Sciences Publishers>
      </db-entry>

      <\db-entry|+2NhSPyP12Ja26Moc|article|bacigalupo2014second>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1693297821>
      <|db-entry>
        <db-field|author|Andrea <name|Bacigalupo>>

        <db-field|title|Second-order homogenization of periodic materials
        based on asymptotic approximation of the strain energy: formulation
        and validity limits>

        <db-field|journal|Meccanica>

        <db-field|year|2014>

        <db-field|volume|49>

        <db-field|number|6>

        <db-field|pages|1407\U1425>

        <db-field|publisher|Springer>
      </db-entry>

      <\db-entry|+4SrAw7gj9jhFPf|article|le2018second>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1682423654>
      <|db-entry>
        <db-field|author|Duc Trung <name|Le><name-sep>Jean-Jacques
        <name|Marigo>>

        <db-field|title|Second order homogenization of quasi-periodic
        structures>

        <db-field|journal|Vietnam Journal of Mechanics>

        <db-field|year|2018>

        <db-field|volume|40>

        <db-field|number|4>

        <db-field|pages|325\U348>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRD|article|abali2021additive>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Bilen Emek <name|Abali><name-sep>Emilio
        <name|Barchiesi>>

        <db-field|title|Additive manufacturing introduced substructure and
        computational determination of metamaterials parameters by means of
        the asymptotic homogenization>

        <db-field|journal|Continuum Mechanics and Thermodynamics>

        <db-field|year|2021>

        <db-field|volume|33>

        <db-field|number|4>

        <db-field|pages|993\U1009>

        <db-field|publisher|Springer>
      </db-entry>

      <\db-entry|+5xSWiId1YPM124U|article|boutin2011generalized>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678285444>
      <|db-entry>
        <db-field|author|Claude <name|Boutin><name-sep>Jean <name|Soubestre>>

        <db-field|title|Generalized inner bending continua for linear fiber
        reinforced materials>

        <db-field|journal|International Journal of Solids and Structures>

        <db-field|year|2011>

        <db-field|volume|48>

        <db-field|number|3-4>

        <db-field|pages|517\U534>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRU|article|abdoul2018homogenization>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Houssam <name|Abdoul-Anziz><name-sep>Pierre
        <name|Seppecher>>

        <db-field|title|Homogenization of periodic graph-based elastic
        structures>

        <db-field|journal|Journal de l'�cole polytechnique\VMath�matiques>

        <db-field|year|2018>

        <db-field|volume|5>

        <db-field|pages|259\U288>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRT|article|abdoul2018strain>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Houssam <name|Abdoul-Anziz><name-sep>Pierre
        <name|Seppecher>>

        <db-field|title|Strain gradient and generalized continua obtained by
        homogenizing frame lattices>

        <db-field|journal|Mathematics and mechanics of complex systems>

        <db-field|year|2018>

        <db-field|volume|6>

        <db-field|number|3>

        <db-field|pages|213\U250>

        <db-field|publisher|Mathematical Sciences Publishers>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRV|article|abdoul2019homogenization>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Houssam <name|Abdoul-Anziz><name-sep>Pierre
        <name|Seppecher><name-sep>C�dric <name|Bellis>>

        <db-field|title|Homogenization of frame lattices leading to second
        gradient models coupling classical strain and strain-gradient terms>

        <db-field|journal|Mathematics and Mechanics of Solids>

        <db-field|year|2019>

        <db-field|volume|24>

        <db-field|number|12>

        <db-field|pages|3976\U3999>

        <db-field|publisher|SAGE Publications Sage UK: London, England>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRI|article|durand2022predictive>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Baptiste <name|Durand><name-sep>Arthur
        <name|Leb�e><name-sep>Pierre <name|Seppecher><name-sep>Karam
        <name|Sab>>

        <db-field|title|Predictive strain-gradient homogenization of a
        pantographic material with compliant junctions>

        <db-field|journal|Journal of the Mechanics and Physics of Solids>

        <db-field|year|2022>

        <db-field|pages|104773>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRL|article|bavzant1972analogy>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|ZP <name|Ba�ant><name-sep>M <name|Christensen>>

        <db-field|title|Analogy between micropolar continuum and grid
        frameworks under initial stress>

        <db-field|journal|International Journal of Solids and Structures>

        <db-field|year|1972>

        <db-field|volume|8>

        <db-field|number|3>

        <db-field|pages|327\U346>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRN|article|dos2012construction>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|F <name|Dos Reis><name-sep>JF <name|Ganghoffer>>

        <db-field|title|Construction of micropolar continua from the
        asymptotic homogenization of beam lattices>

        <db-field|journal|Computers & Structures>

        <db-field|year|2012>

        <db-field|volume|112>

        <db-field|pages|354\U363>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRO|article|nassar2020microtwist>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|Hussein <name|Nassar><name-sep>Hui
        <name|Chen><name-sep>Guoliang <name|Huang>>

        <db-field|title|Microtwist elasticity: a continuum approach to zero
        modes and topological polarization in kagome lattices>

        <db-field|journal|Journal of the Mechanics and Physics of Solids>

        <db-field|year|2020>

        <db-field|volume|144>

        <db-field|pages|104107>

        <db-field|publisher|Elsevier>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNRC|article|LESTRINGANT2020103730>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|C. <name|Lestringant><name-sep>B. <name|Audoly>>

        <db-field|title|Asymptotically exact strain-gradient models for
        nonlinear slender elastic structures: a systematic derivation method>

        <db-field|journal|Journal of the Mechanics and Physics of Solids>

        <db-field|year|2020>

        <db-field|volume|136>

        <db-field|pages|103730>
      </db-entry>

      <\db-entry|+2NhSPyP12Ja26MoY|misc|shoalLibPermanent>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1693297821>
      <|db-entry>
        <db-field|title|The shoal library>

        <db-field|howpublished|<slink|https://archive.softwareheritage.org/browse/origin/https://git.renater.fr/anonscm/git/shoal/shoal.git>>

        <db-field|year|2023>
      </db-entry>

      <\db-entry|+4SrAw7gj9jhFPe|article|Audoly-Lestringant-Asymptotic-derivation-of-high-order-2021>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1682423654>
      <|db-entry>
        <db-field|author|B. <name|Audoly><name-sep>C. <name|Lestringant>>

        <db-field|title|Asymptotic derivation of high-order rod models from
        non-linear 3D elasticity>

        <db-field|journal|Journal of the Mechanics and Physics of Solids>

        <db-field|year|2021>

        <db-field|volume|148>

        <db-field|pages|104264>
      </db-entry>

      <\db-entry|+2NhSPyP12Ja26Mob|article|david2012homogenized>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1693297821>
      <|db-entry>
        <db-field|author|Martin <name|David><name-sep>J-J
        <name|Marigo><name-sep>C <name|Pideri>>

        <db-field|title|Homogenized interface model describing
        inhomogeneities located on a surface>

        <db-field|journal|Journal of Elasticity>

        <db-field|year|2012>

        <db-field|volume|109>

        <db-field|pages|153\U187>

        <db-field|publisher|Springer>
      </db-entry>

      <\db-entry|+hDS0FUy1fZTcNR9|misc|Mathematica>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1678115454>
      <|db-entry>
        <db-field|author|<name|Wolfram Research, Inc.>>

        <db-field|title|Mathematica, Version 13.0.0>

        <db-field|year|2021>

        <db-field|note|Champaign, IL, 2021>

        <db-field|url|<slink|https://www.wolfram.com/mathematica>>
      </db-entry>

      <\db-entry|+2NhSPyP12Ja26MoZ|article|Berdichevskii-On-the-energy-of-an-elastic-rod-1981>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1693297821>
      <|db-entry>
        <db-field|author|V. L. <name|Berdichevskii>>

        <db-field|title|On the energy of an elastic rod>

        <db-field|journal|Journal of Applied Mathematics and Mechanics>

        <db-field|year|1981>

        <db-field|volume|45>

        <db-field|number|4>

        <db-field|pages|518\U529>

        <db-field|month|jan>
      </db-entry>

      <\db-entry|+2NhSPyP12Ja26Moa|book|Hodges-Nonlinear-composite-beam-2006>
        <db-field|contributor|claire>

        <db-field|modus|imported>

        <db-field|date|1693297821>
      <|db-entry>
        <db-field|author|D. H. <name|Hodges>>

        <db-field|title|Nonlinear composite beam theory>

        <db-field|publisher|American Institute of Aeronautics and
        Astronautics>

        <db-field|year|2006>

        <db-field|volume|213>

        <db-field|series|Progress in astronautics and aeronautics>
      </db-entry>
    </associate>
  </collection>
</attachments>

<\references>
  <\collection>
    <associate|a:tensors|<tuple|A|10>>
    <associate|app:gradient-effect|<tuple|C|13>>
    <associate|app:homogeneous properties|<tuple|D|21>>
    <associate|app:homogeneous-solution|<tuple|B|11>>
    <associate|app:rank-deficient|<tuple|E|22>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|3.2|5>>
    <associate|auto-11|<tuple|3.3|6>>
    <associate|auto-12|<tuple|4|7>>
    <associate|auto-13|<tuple|5|7>>
    <associate|auto-14|<tuple|5.1|7>>
    <associate|auto-15|<tuple|5.2|8>>
    <associate|auto-16|<tuple|6|9>>
    <associate|auto-17|<tuple|6.1|10>>
    <associate|auto-18|<tuple|6.1|10>>
    <associate|auto-19|<tuple|A|10>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-20|<tuple|B|11>>
    <associate|auto-21|<tuple|C|13>>
    <associate|auto-22|<tuple|C.1|13>>
    <associate|auto-23|<tuple|C.1|13>>
    <associate|auto-24|<tuple|C.2|14>>
    <associate|auto-25|<tuple|C.3|15>>
    <associate|auto-26|<tuple|C.4|16>>
    <associate|auto-27|<tuple|C.5|16>>
    <associate|auto-28|<tuple|C.6|17>>
    <associate|auto-29|<tuple|C.7|17>>
    <associate|auto-3|<tuple|2.1|1>>
    <associate|auto-30|<tuple|C.8|18>>
    <associate|auto-31|<tuple|C.9|19>>
    <associate|auto-32|<tuple|C.10|20>>
    <associate|auto-33|<tuple|C.2|20>>
    <associate|auto-34|<tuple|C.3|20>>
    <associate|auto-35|<tuple|D|21>>
    <associate|auto-36|<tuple|D.1|21>>
    <associate|auto-37|<tuple|E|22>>
    <associate|auto-38|<tuple|E.1|22>>
    <associate|auto-39|<tuple|E.2|22>>
    <associate|auto-4|<tuple|2.1|2>>
    <associate|auto-40|<tuple|E.3|22>>
    <associate|auto-41|<tuple|E.4|23>>
    <associate|auto-42|<tuple|E.5|24>>
    <associate|auto-43|<tuple|E.6|25>>
    <associate|auto-44|<tuple|E.7|25>>
    <associate|auto-45|<tuple|E.8|26>>
    <associate|auto-5|<tuple|2.2|3>>
    <associate|auto-6|<tuple|3|3>>
    <associate|auto-7|<tuple|3.1|3>>
    <associate|auto-8|<tuple|3.2|4>>
    <associate|auto-9|<tuple|3.1|5>>
    <associate|bib-Audoly-Lestringant-An-energy-approach-to-asymptotic-2023|<tuple|AL23|10>>
    <associate|bib-Mathematica|<tuple|21|10>>
    <associate|bib-abdoul2018strain|<tuple|AS18|10>>
    <associate|bib-david2012homogenized|<tuple|DMP12|10>>
    <associate|bib-shoalLibPermanent|<tuple|sho23|10>>
    <associate|e:no-micro-mechanism|<tuple|3.4|4>>
    <associate|energyexpansion|<tuple|C.13|15>>
    <associate|eq-phi-1-2-to-be-relaxed|<tuple|5.10|8>>
    <associate|eq-y-order-by-order|<tuple|3.16|6>>
    <associate|eq:A-unpacked|<tuple|C.21|16>>
    <associate|eq:A0-K1|<tuple|C.22|16>>
    <associate|eq:Att-F-Bparenth|<tuple|C.28|17>>
    <associate|eq:B-C-rank-deficient-aware|<tuple|E.19|25>>
    <associate|eq:BC-extract|<tuple|C.47|20>>
    <associate|eq:C0-ibp|<tuple|C.48|20>>
    <associate|eq:C1-ibp|<tuple|C.25|17>>
    <associate|eq:E-alpha-expansion-nearly-done|<tuple|C.10|15>>
    <associate|eq:E-alpha-expansion-nearly-done-components|<tuple|C.11|15>>
    <associate|eq:E-alpha-using-structure-coefs|<tuple|C.8|14>>
    <associate|eq:E-function-of-lcheck|<tuple|E.10|23>>
    <associate|eq:E-hom-decompose|<tuple|B.10|12>>
    <associate|eq:E-localization|<tuple|B.11|12>>
    <associate|eq:E-optimum-summary|<tuple|3.9|6>>
    <associate|eq:E-phi-0|<tuple|5.1|7>>
    <associate|eq:EofX|<tuple|2.1|1>>
    <associate|eq:F-breakup|<tuple|3.10|6>>
    <associate|eq:K0-def|<tuple|B.12|12>>
    <associate|eq:KABka-expansion|<tuple|3.6|5>>
    <associate|eq:Kt2-At1-Ft1-etc-tmp|<tuple|C.24|16>>
    <associate|eq:L-calligraphic-operator|<tuple|C.5|14>>
    <associate|eq:NA-def|<tuple|E.2|22>>
    <associate|eq:P-matrix|<tuple|B.5|12>>
    <associate|eq:PVW0|<tuple|B.15|13>>
    <associate|eq:Phi-0-form|<tuple|3.11|6>>
    <associate|eq:Phi-1-form|<tuple|3.12|6>>
    <associate|eq:Phi-2-after-ibp|<tuple|C.27|17>>
    <associate|eq:Phi-2-form|<tuple|3.13|6>>
    <associate|eq:Phi1-eliminate-y1|<tuple|C.16|15>>
    <associate|eq:Phi12-yi-eliminated|<tuple|C.18|16>>
    <associate|eq:PhiExpansionAnnounce|<tuple|5.13|8>>
    <associate|eq:Phic-expansion-showing-dep-on-y1|<tuple|5.9|8>>
    <associate|eq:Q-yi-is-zero|<tuple|C.12|15>>
    <associate|eq:QEisZero|<tuple|2.3|2>>
    <associate|eq:R-order0|<tuple|B.7|12>>
    <associate|eq:R-singular-tmp|<tuple|E.12|23>>
    <associate|eq:RforY0prime|<tuple|C.34|18>>
    <associate|eq:Vs|<tuple|C.2|13>>
    <associate|eq:W-0|<tuple|5.2|7>>
    <associate|eq:W0-basic|<tuple|B.1|11>>
    <associate|eq:W0-quadratic|<tuple|B.2|12>>
    <associate|eq:W1-A0-def|<tuple|C.20|16>>
    <associate|eq:Wcal|<tuple|B.3|12>>
    <associate|eq:Y-vs-gradM|<tuple|3.8|6>>
    <associate|eq:Yp-extraction|<tuple|C.37|18>>
    <associate|eq:Yp-unpack|<tuple|C.36|18>>
    <associate|eq:Z-Lambda-Identity|<tuple|B.13|12>>
    <associate|eq:abstract-energy-density|<tuple|2.5|2>>
    <associate|eq:cal-L-operators|<tuple|C.7|14>>
    <associate|eq:calF-def|<tuple|C.40|19>>
    <associate|eq:characterize-A-null-vectors|<tuple|E.1|22>>
    <associate|eq:condition-on-boundary|<tuple|C.29|17>>
    <associate|eq:constrained-variational-pb-yHom|<tuple|5.5|7>>
    <associate|eq:corrector-packed|<tuple|C.32|18>>
    <associate|eq:d-transparent-to-l|<tuple|E.9|23>>
    <associate|eq:def-Rprime|<tuple|E.22|25>>
    <associate|eq:def-h|<tuple|C.1|13>>
    <associate|eq:def-modify-R|<tuple|E.13|23>>
    <associate|eq:deficient-Bcirc-C|<tuple|E.18|24>>
    <associate|eq:deficient-F0-K0|<tuple|E.15|24>>
    <associate|eq:deficient-eliminate-y2|<tuple|E.17|24>>
    <associate|eq:deficient-injection-matrices|<tuple|E.7|23>>
    <associate|eq:deficient-order-1-sol|<tuple|E.21|25>>
    <associate|eq:delta-B|<tuple|C.26|17>>
    <associate|eq:extended-l|<tuple|E.6|22>>
    <associate|eq:extract-F|<tuple|C.42|19>>
    <associate|eq:final-ibp-Phi-star|<tuple|C.52|20>>
    <associate|eq:final-ibp-k-a|<tuple|C.53|20>>
    <associate|eq:final-ibp-tensors|<tuple|C.2|20>>
    <associate|eq:final-ibp-tensors-content|<tuple|C.3|20>>
    <associate|eq:grad-y1-final|<tuple|C.38|18>>
    <associate|eq:h-unpack|<tuple|C.4|14>>
    <associate|eq:higher-order-E-relocalized|<tuple|C.39|19>>
    <associate|eq:hom-sol-optimum-pb|<tuple|B.4|12>>
    <associate|eq:homogenized-model-announce|<tuple|3.5|4>>
    <associate|eq:how-S0-talks-to-ld|<tuple|E.16|24>>
    <associate|eq:implement-B-C|<tuple|C.45|19>>
    <associate|eq:implement-P-R|<tuple|C.33|18>>
    <associate|eq:l-space-decomposition|<tuple|E.8|23>>
    <associate|eq:lambda1sol|<tuple|C.35|18>>
    <associate|eq:linear-singular-problem|<tuple|E.3|22>>
    <associate|eq:linear-singular-solution|<tuple|E.5|22>>
    <associate|eq:linear-singular-solvability|<tuple|E.4|22>>
    <associate|eq:lm-to-h|<tuple|C.3|14>>
    <associate|eq:localization-tensor-Lambda-0|<tuple|B.9|12>>
    <associate|eq:localization-tensor-order0|<tuple|B.8|12>>
    <associate|eq:nabla-order-of-magnitude|<tuple|2.7|3>>
    <associate|eq:new-Phi2-star-packed|<tuple|C.44|19>>
    <associate|eq:no-y0-lhat-coupling|<tuple|E.14|24>>
    <associate|eq:optim-problem-for-y1|<tuple|C.30|17>>
    <associate|eq:order-0-tmpA|<tuple|5.4|7>>
    <associate|eq:pb-for-optimal-displacement|<tuple|C.31|18>>
    <associate|eq:phi-0-in-terms-of-W-hom|<tuple|5.7|8>>
    <associate|eq:phi-1-just-A|<tuple|C.19|16>>
    <associate|eq:phi-2-bt|<tuple|5.12|8>>
    <associate|eq:phi-canonical|<tuple|2.4|2>>
    <associate|eq:phi-continuous-order0|<tuple|5.3|7>>
    <associate|eq:phi-star-new|<tuple|3.2|4>>
    <associate|eq:phi1-phi2-tmp|<tuple|C.15|15>>
    <associate|eq:phi2-tmp2|<tuple|C.23|16>>
    <associate|eq:relaxed-energy-order-by-order|<tuple|3.14|6>>
    <associate|eq:solvability-y0|<tuple|E.11|23>>
    <associate|eq:solvability-y1|<tuple|E.20|25>>
    <associate|eq:solvability-y2|<tuple|E.23|25>>
    <associate|eq:split-phi-2|<tuple|5.11|8>>
    <associate|eq:stationarity-phi2|<tuple|5.14|8>>
    <associate|eq:strain-canonical-form|<tuple|2.2|1>>
    <associate|eq:structure-coefficients|<tuple|C.9|14>>
    <associate|eq:symmetrize-compose|<tuple|A.7|11>>
    <associate|eq:symmetrize-group-indices-demo|<tuple|A.6|11>>
    <associate|eq:symmetrize-with-respect-to-groups-of-indices|<tuple|A.5|11>>
    <associate|eq:tensorContractions|<tuple|A.1|10>>
    <associate|eq:total-stress-S0|<tuple|B.14|13>>
    <associate|eq:upacking-BC|<tuple|C.46|19>>
    <associate|eq:y-expand|<tuple|C.6|14>>
    <associate|eq:y-expansion|<tuple|3.15|6>>
    <associate|eq:y-expansion-leading-order-known|<tuple|5.8|8>>
    <associate|eq:y-hom-from-h|<tuple|5.6|7>>
    <associate|eq:y-in-gradients-of-l|<tuple|3.7|5>>
    <associate|eq:y-star-withLagrange|<tuple|3.1|4>>
    <associate|eq:y0-lambda0-combined|<tuple|B.6|12>>
    <associate|eq:y1-star|<tuple|5.15|9>>
    <associate|eq:ystar-as-stpt|<tuple|3.3|4>>
    <associate|eta-def|<tuple|2.6|3>>
    <associate|rk:scaling-nabla|<tuple|2.2|3>>
    <associate|s:canonical-form|<tuple|2.1|1>>
    <associate|s:examples|<tuple|4|7>>
    <associate|sec:analysis-gradient-effect|<tuple|5.2|8>>
    <associate|sec:app-optimal-corrective-displacement|<tuple|C.7|17>>
    <associate|sec:input|<tuple|2|1>>
    <associate|sec:justification|<tuple|5|7>>
    <associate|sec:numerical-implementation|<tuple|6|9>>
    <associate|ssec:elim-y2|<tuple|C.5|16>>
    <associate|ssec:elim-yi|<tuple|C.3|15>>
    <associate|ssec:extract-A0-K1|<tuple|C.4|16>>
    <associate|ssec:ibp|<tuple|C.6|17>>
    <associate|ssec:order-0-detailed-justification|<tuple|5.1|7>>
    <associate|ssec:packed-h|<tuple|C.1|13>>
    <associate|ssec:slow-variations|<tuple|2.2|3>>
    <associate|ssec:work-out-Phi2|<tuple|C.9|19>>
    <associate|tab:ho-homogenization-uniform-props|<tuple|D.1|21>>
    <associate|tab:input|<tuple|2.1|2>>
    <associate|tab:internal-tensors|<tuple|C.1|13>>
    <associate|tab:leading-order-summary|<tuple|6.1|10>>
    <associate|tab:tensor-symmetries|<tuple|3.1|5>>
    <associate|tab:tensors-delivered|<tuple|3.2|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      Audoly-Lestringant-An-energy-approach-to-asymptotic-2023

      abdoul2018strain

      david2012homogenized

      Mathematica

      shoalLibPermanent
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.1>|>
        List of parameters passed as an input to the homogenization
        procedure. The notation used in the last two columns is defined in
        Appendix <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><reference|a:tensors>.
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.1>|>
        Dimensions and symmetries of the tensors appearing in Equation
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:homogenized-model-announce>),
        (<reference|eq:y-in-gradients-of-l>) and
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:E-optimum-summary>).
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.2>|>
        Tensors delivered by the homogenization procedure, defining the
        homogenized model in Equations <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:homogenized-model-announce>\U<reference|eq:F-breakup>).
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6.1>|>
        Implementation of the leading-order procedure in the non-deficient
        case (<with|mode|<quote|math>|n<rsub|<with|mode|<quote|text>|d>>=0>),
        based on the formulas referenced in the first three items in the
        bullet list from Section <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><reference|sec:numerical-implementation>.
      </surround>|<pageref|auto-17>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|C.1>|>
        Summary of the tensors used internally by the homogenization
        procedure in Appendix <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><reference|app:gradient-effect>.
        All these tensors make use of the compact
        <with|mode|<quote|math>|\<b-h\>> notation, see
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:def-h>).
      </surround>|<pageref|auto-22>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|C.2>|>
        Tensors associated with the boundary term in
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:final-ibp-Phi-star>).
      </surround>|<pageref|auto-33>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|C.3>|>
        Content of the tensors associated with the boundary term, see
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>(<reference|eq:final-ibp-k-a>).
      </surround>|<pageref|auto-34>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|D.1>|>
        Higher-order homogenization in the special case of homogeneous
        properties. A complete implementation of the method in this special
        case is possible based on Table <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><reference|tab:leading-order-summary>
        (leading order, ignoring any dependence on
        <with|mode|<quote|math>|\<b-m\>>) and on the definitions appearing in
        the first column of the table above. The quantities appearing in the
        grey rows are the main results (localization tensors for corrective
        displacement <with|mode|<quote|math>|\<b-Y\><rsub|0><rprime|'>>,
        Lagrange multipliers <with|mode|<quote|math>|\<b-G\><rsub|0><rprime|'>>,
        bulk energy contributions <with|mode|<quote|math>|\<b-A\><rsub|0>>
        and <with|mode|<quote|math>|\<b-B\><rsub|0>> and boundary
        contribution <with|mode|<quote|math>|\<b-A\><rsub|0>>).
      </surround>|<pageref|auto-36>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|1.<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|2.<space|2spc>Input
      to the homogenization procedure> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1.<space|2spc>Energy formulation of the
      input model <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|2.2.<space|2spc>Assumption of slow
      variations <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|3.<space|2spc>Summary
      of the main results > <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-6><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1.<space|2spc>Homogenization as a partial
      energy relaxation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|3.2.<space|2spc>Homogenization results in
      compact form <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|3.3.<space|2spc>Homogenization results in
      the form of a systematic expansion <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|4.<space|2spc>Illustrations>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-12><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|5.<space|2spc>Derivation
      of the homogenized model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-13><vspace|0.5fn>

      <with|par-left|<quote|1tab>|5.1.<space|2spc>Leading order (classical
      homogenization) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>

      <with|par-left|<quote|1tab>|5.2.<space|2spc>Analysis of the gradient
      effect <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|6.<space|2spc>Symbolic
      implementation: The <with|font-family|<quote|tt>|language|<quote|verbatim>|shoal>
      library> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-16><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-18><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Appendix
      A.<space|2spc>Tensor algebra> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-19><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Appendix
      B.<space|2spc>Detailed analysis of leading order (classical
      homogenization)> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-20><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Appendix
      C.<space|2spc>Detailed analysis of the gradient effect>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-21><vspace|0.5fn>

      <with|par-left|<quote|1tab>|C.1.<space|2spc>Packed macroscopic
      variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <with|par-left|<quote|1tab>|C.2.<space|2spc>Structure coefficients
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24>>

      <with|par-left|<quote|1tab>|C.3.<space|2spc>Strain energy expansion in
      terms of corrective displacement <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      <with|par-left|<quote|1tab>|C.4.<space|2spc>Correction at order
      <with|mode|<quote|math>|\<eta\>> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26>>

      <with|par-left|<quote|1tab>|C.5.<space|2spc>Functional governing
      corrector <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-27>>

      <with|par-left|<quote|1tab>|C.6.<space|2spc>Integration by parts
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>>

      <with|par-left|<quote|1tab>|C.7.<space|2spc>Optimal corrective
      displacement <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      <with|par-left|<quote|1tab>|C.8.<space|2spc>Microscopic strain
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-30>>

      <with|par-left|<quote|1tab>|C.9.<space|2spc>Homogenized energy at order
      <with|mode|<quote|math>|\<eta\><rsup|2>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31>>

      <with|par-left|<quote|1tab>|C.10.<space|2spc>Optional integration by
      parts <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Appendix
      D.<space|2spc>Special case of homogeneous properties>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-35><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-shape|<quote|small-caps>|Appendix
      E.<space|2spc>Extension to a rank-deficient matrix>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <pageref|auto-37><vspace|0.5fn>

      <with|par-left|<quote|1tab>|E.1.<space|2spc>Special form of null
      vectors <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-38>>

      <with|par-left|<quote|1tab>|E.2.<space|2spc>Solutions of the linear
      equation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-39>>

      <with|par-left|<quote|1tab>|E.3.<space|2spc>Extended macroscopic strain
      vector <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-40>>

      <with|par-left|<quote|1tab>|E.4.<space|2spc>Changes to leading-order
      analysis <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-41>>

      <with|par-left|<quote|1tab>|E.5.<space|2spc>Changes to the energy
      expansion <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-42>>

      <with|par-left|<quote|1tab>|E.6.<space|2spc>Changes to the corrective
      displacement <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-43>>

      <with|par-left|<quote|1tab>|E.7.<space|2spc>Solvability condition for
      <with|mode|<quote|math>|\<b-y\><rsub|<around*|[|2|]>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-44>>

      <with|par-left|<quote|1tab>|E.8.<space|2spc>Summary: extension to
      rank-deficient problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>>
    </associate>
  </collection>
</auxiliary>